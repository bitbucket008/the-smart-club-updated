//
//  MyCartTableViewController.swift
//  smartclub
//
//  Created by Admin on 03/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit




protocol MyCartTableViewControllerDelegate :NSObjectProtocol {
    
    func  openSmartBagTab()
    
}


class MyCartTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , PopupAddedSmartBagViewControllerDelegate{

    
    weak var delegate:MyCartTableViewControllerDelegate?
    @IBOutlet var tableView: UITableView!
    
    var checkoutBtnActionCounter:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
           tableView.register(UINib(nibName: "MyCartTableViewCell", bundle: nil), forCellReuseIdentifier: "mycarttableviewcell")
        
        checkoutBtnActionCounter = -1;
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 15
    }

    
    
    @IBAction func checkoutBtnAction(_ sender: Any) {
        
        
        checkoutBtnActionCounter = checkoutBtnActionCounter! + 1
        
        let stboard = UIStoryboard.init(name: "mycart", bundle: nil)
        
        
        if checkoutBtnActionCounter! == 0 {
        
        let vc = stboard.instantiateViewController(withIdentifier: "popupbuyspvc") as! PopupBuySPViewController
        
        
        
        self.present(vc, animated: true, completion: nil)
        
        } else {
            
            
            let vc = stboard.instantiateViewController(withIdentifier: "popupaddedspvc") as! PopupAddedSmartBagViewController
            
            
            vc.delegate = self;
            self.present(vc, animated: true, completion: nil)
            
            
            checkoutBtnActionCounter =  -1
            
            
        }
        
        
        
        
    }
    
    
    
    func onClickGotoSmartBag() {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openSmartCartObs"), object: nil)
        
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycarttableviewcell", for: indexPath) as! MyCartTableViewCell
        
       //he cell...
        return cell
    }
    

    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      
        return 175;
        
        
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
