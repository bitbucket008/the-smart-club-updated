//
//  BuySPTableViewCell.swift
//  smartclub
//
//  Created by Admin on 03/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

protocol BuySpDelegate:NSObjectProtocol {
    func onClickBuyNow(position:Int?)
}

class BuySPTableViewCell: UITableViewCell {
    
    //MARK: Properties
    open let CLASS_NAME = "BuySPTableViewCell"
    open var delegate:BuySpDelegate?
    open var spPosition:Int?
    
    //MARK: Outlets
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSP: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var priceContainer: UIView!
    @IBOutlet weak var btnBuyNow: UIBorderButton!
    @IBOutlet weak var rootView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rootView.layoutMargins = UIEdgeInsetsMake(5, 5, 5, 5)
//        containerView.layer.borderWidth = 1
//        containerView.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: 0xF2F2F2, alpha: 1).cgColor
        
        
        let layerTop:UIView = UIView()
        layerTop.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0xF2F2F2, alpha: 1.0)
        layerTop.frame = CGRect(x:50, y:0, width:containerView.frame.width - 100, height:1)
        
        let layerBottom:UIView = UIView()
        layerBottom.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0xF2F2F2, alpha: 1.0)
        layerBottom.frame = CGRect(x:50, y:containerView.frame.height-1, width:containerView.frame.width-100, height:1)
        
        containerView.addSubview(layerTop)
        containerView.addSubview(layerBottom)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: Actions
    @IBAction func onClickBuyNowButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        if let buySpDelegate  = delegate {
            buySpDelegate.onClickBuyNow(position: spPosition)
        }
    }
    
}
