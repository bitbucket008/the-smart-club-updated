//
//  PopupAddedSmartBagViewController.swift
//  smartclub
//
//  Created by Admin on 03/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit


protocol PopupAddedSmartBagViewControllerDelegate :NSObjectProtocol {
    
    func  onClickGotoSmartBag()
    
    
}


class PopupAddedSmartBagViewController: UIViewController {

    
    
    weak var delegate:PopupAddedSmartBagViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func tagGegtureAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    
    
    @IBAction func smartBagBtnAction(_ sender: Any) {
        
//         let stboard = UIStoryboard.init(name: "smartbag", bundle: nil)
//
//        let vc = stboard.instantiateViewController(withIdentifier: "smartbagvc") as! SmartBagViewController
//
//
//
//        self.present(vc, animated: true, completion: nil)
        
        self.dismiss(animated: true, completion: nil)
        delegate?.onClickGotoSmartBag()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
