//
//  RateDealViewController.swift
//  smartclub
//
//  Created by Admin on 04/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class RateDealViewController: UIViewController {

    @IBOutlet weak var backgroundView:UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backgroundView.clipsToBounds = true
        backgroundView.layer.cornerRadius = 10;
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapgestureAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
