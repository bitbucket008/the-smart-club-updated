//
//  PaymentProccessViewController.swift
//  smartclub
//
//  Created by RASHED on 3/1/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class PaymentProccessViewController: UIViewController {

    //MARK: Properties
    fileprivate let CLASS_NAME = "PaymentProccessViewController"
    open var amount:Int!
    fileprivate let localStorage = LocalStorage()
    
    //MARK: Outlets
    @IBOutlet var wvPaymentForm: UIWebView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var lbllScreenTitle: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(CLASS_NAME+" -- viewDidLoad() -- deviceToken -- "+localStorage.getUserDeviceToken()!)
        
        //load payment form on webview
        let url = URL (string: "https://vistamoney.info/paymentgateway-external/pghostedvista.xhtml?terminalId=SmartIT1&amount="+String(amount)+"&currency=QAR&trackId=Jawad123&udfs1="+localStorage.getUser().getId()!+"&udfs2="+localStorage.getUserDeviceToken()!)
        
        print(CLASS_NAME+" -- viewDidLoad() -- url on webview -- \(url)")
        
        let request = URLRequest(url: url!)
        wvPaymentForm.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Mark: Actions
    //when back press
    @IBAction func onBackPress(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    

}
