//
//  PopupBuySPViewController.swift
//  smartclub
//
//  Created by Admin on 03/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import StoreKit

class PopupBuySPViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Properties 
    open var CLASS_NAME = "PopupBuySPViewController"
    fileprivate var buySpList:[BuySpModel] = [BuySpModel]()
    open var headerText:String!
    fileprivate var spProductList = [SKProduct]()
    fileprivate var spProduct = SKProduct()
    fileprivate var spProductIds = Set<String>()
    fileprivate let localStorage = LocalStorage()
    fileprivate var selectedSpPackageId:String!
    fileprivate var selectedSpPackagePrice:Float!
    open var spChangeDelegate:SmartPointChangeDelegate!

    //MARK: Outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var dialogContainer: UICornerView!
    @IBOutlet weak var lblHeaderText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       lblHeaderText.text = headerText
        
        dialogContainer.clipsToBounds = true
        dialogContainer.layer.cornerRadius = 35
        self.definesPresentationContext = true
        
        tableView.register(UINib(nibName: "BuySPTableViewCell", bundle: nil), forCellReuseIdentifier: "buysptableviewcell")
        
        //get by sp list
        SVProgressHUD.show()
        getSpBuyList()
        
    }
    
    //MARK: IAP_Methods
    private func setupIAP() -> Void {
        if(SKPaymentQueue.canMakePayments()) {
            print(CLASS_NAME+" -- setupIAP() -- productIdentifiers -- \(spProductIds)")
            
            let request: SKProductsRequest = SKProductsRequest(productIdentifiers: spProductIds)
            request.delegate = self
            request.start()
        } else {
            print(CLASS_NAME+" -- setupIAP() -- please enable IAPS")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Network call
    //get buy sp list from server
    private func getSpBuyList() -> Void {
        //show progress dialog
//        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_BUY_NOW_SP)!,
            method: .get,
            parameters:nil, encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
//                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                        let swiftyJson =  JSON(response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let id = swiftyJson["data"][i]["id"].string
                                    let isLock = swiftyJson["data"][i]["is_lock"].int
                                    let qarPrice = swiftyJson["data"][i]["qar_price"].floatValue
                                    let spVal = swiftyJson["data"][i]["sp_val"].intValue
                                    let status = swiftyJson["data"][i]["status"].string
                                    
                                    let buySpModel = BuySpModel(id: id, isLock: isLock, qarPrice: Optional(qarPrice), spVal: Optional(spVal), status: status)
                                    self.spProductIds.insert(id!+"sp"+String(spVal))
                                    self.buySpList.append(buySpModel)
                                    print(self.CLASS_NAME+" -- getSpBuyList()  -- qar value -- "+String(self.buySpList[i].id!))
                                }
                                
                                //reload data
                                self.setupIAP()
                                self.tableView!.reloadData()
//                                SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                                print(self.CLASS_NAME+" -- getSpBuyList()  -- response -- SP list loaded")
                            }
                            
                        }else{
//                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getSpBuyList()  -- response -- Request failed")
                        }
                    
                }else{
                    print(self.CLASS_NAME+" -- getSpBuyList()  -- response -- request failed")
//                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getCategories()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    //update customer sp on  server and send notification
    open func updtaeCustomerSp() -> Void {
        
        print(CLASS_NAME+" -- updtaeCustomerSp() -- \(["emailId": localStorage.getUser().getEmail(), "amount": selectedSpPackagePrice, "udfs1":localStorage.getUser().getId(), "udfs2": localStorage.getUserDeviceToken(), "udfs3": selectedSpPackageId])")
        
//        SVProgressHUD.show()
        Alamofire.request(
            URL(string: ApiEndpoints.URI_PAYMENT_RESPONSE)!,
            method: .get,
            parameters: ["email": localStorage.getUser().getEmail()!, "amount": selectedSpPackagePrice, "customer_id":localStorage.getUser().getId()!, "device_token": localStorage.getUserDeviceToken()!, "sp_package_id": selectedSpPackageId, "success": true])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
//                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        //get updated  customer sp
                        self.spChangeDelegate.getSmartPointFromServer()
//                        SnackBarManager.showSnackBar(message: "Buy sp successfully")
                        
                    }else{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- updtaeCustomerSp()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- updtaeCustomerSp()  -- response -- "+(String(describing: response)))
                
        }
    }

    //send failed notification to customer
    open func sendFailedNotificationToCustomer() -> Void {
        
        print(CLASS_NAME+" -- sendFailedNotificationToCustomer() -- \(["emailId": localStorage.getUser().getEmail(), "amount": selectedSpPackagePrice, "udfs1":localStorage.getUser().getId(), "udfs2": localStorage.getUserDeviceToken(), "udfs3": selectedSpPackageId, "success": false])")
        
//        SVProgressHUD.show()
        Alamofire.request(
            URL(string: ApiEndpoints.URI_PAYMENT_RESPONSE)!,
            method: .get,
            parameters: ["email": localStorage.getUser().getEmail()!, "amount": selectedSpPackagePrice, "customer_id":localStorage.getUser().getId()!, "device_token": localStorage.getUserDeviceToken()!, "sp_package_id": selectedSpPackageId, "success": "false"])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                //                SVProgressHUD.dismiss()
                
                //go to  login view controller
//                if response.result.isSuccess{
//                    let swiftyJson = JSON(response.data!)
//                    if  swiftyJson["success"].intValue == 1{
//                        SnackBarManager.showSnackBar(message: "Buy sp successfully")
//
//                    }else{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
//                    }
//
//                }else{
//                    print(self.CLASS_NAME+" -- updtaeCustomerSp()  -- response -- request failed")
//                    //                    SnackBarManager.showSnackBar(message: "Request failed")
//                }
                
                print(self.CLASS_NAME+" -- updtaeCustomerSp()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //vistamoney transaction
    private func makeVistaTransaction(spPosition:Int) -> Void {
        var requestBody = "<?xml version='1.0' encoding='ISO-8859-1' ?>"
        requestBody += "<request>"
        requestBody += "<terminalid>"+"Smartologyst"+"</terminalid>"
        requestBody += "<password>"+"tsc2018"+"</password>"
        requestBody += "<action>1</action>"
        requestBody += "<card>"+"5273843894580010"+"</card>"
        requestBody += "<cvv2>"+"969"+"</cvv2>"
        requestBody += "<expYear>"+"2019"+"</expYear>"
        requestBody += "<expMonth>"+"05"+"</expMonth>"
        requestBody += "<member>"+"Zuhair Mohammad"+"</member>"
        requestBody += "<currencyCode>USD</currencyCode>"
//        requestBody += "<address></address>"
//        requestBody += "<city></city>"
//        requestBody += "<statecode></statecode>"
//        requestBody += "<zip></zip>"
        requestBody += "<CountryCode>QA</CountryCode>"
        requestBody += "<email>test@test.com</email>"
        requestBody += "<amount>1</amount>"
        requestBody += "<trackid>TEST</trackid>"
//        requestBody += "<udf1>1234</udf1>"
//        requestBody += "<udf2>1234</udf2>"
//        requestBody += "<udf3>1234</udf3>"
//        requestBody += "<udf4>1234</udf4>"
        requestBody += "</request>"
        
        print(CLASS_NAME+" -- makeVistaTransaction() -- "+requestBody)
        
        let url = URL(string:ApiEndpoints.URI_VISTA_MONEY_PERFORM_TRANSACTION)
        var xmlRequest = URLRequest(url: url!)
        xmlRequest.httpBody = requestBody.data(using: String.Encoding.utf8, allowLossyConversion: true)
        xmlRequest.httpMethod = "POST"
        xmlRequest.addValue("application/xml", forHTTPHeaderField: "Content-Type")

        //show progressbar
        SVProgressHUD.show()
        
        Alamofire.request(xmlRequest)
            .responseData { (response) in
                SVProgressHUD.dismiss()
                let stringResponse: String = String(data: response.data!, encoding: String.Encoding.utf8) as String!
                print(self.CLASS_NAME+" -- makeVistaTransaction() - reponse - "+stringResponse)
        }
    }
    
    
    //MARK: Actions
    @IBAction func tagGegtureAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return buySpList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "buysptableviewcell", for: indexPath) as! BuySPTableViewCell
        
        cell.spPosition = Optional(indexPath.row)
        cell.delegate = self
        
        // Configure the cell...
        if let spVal = buySpList[indexPath.row].spVal {
            cell.lblSP!.text = String(spVal)
        }
        
        if let qarPrice = buySpList[indexPath.row].qarPrice {
            cell.lblPrice!.text = "For "+String(qarPrice)+" \n QAR" 
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}


//MARK: Extension
extension PopupBuySPViewController : BuySpDelegate{
    func onClickBuyNow(position: Int?) {
        if let itemPosition = position {
            print(CLASS_NAME+" -- onClickBuyNow -- position -- "+String(itemPosition))
//            makeVistaTransaction(spPosition: itemPosition)
//            let strBoard = UIStoryboard.init(name: "mycart", bundle: nil)
//            let vc = strBoard.instantiateViewController(withIdentifier: "PaymentProccessViewController") as! PaymentProccessViewController
//            vc.amount = buySpList[itemPosition].qarPrice
//            self.navigationController?.pushViewController(vc, animated: true)
//            self.present(vc, animated: true, completion: nil)
            
            //make iap payment
            if itemPosition < spProductList.count{
                spProduct = spProductList[itemPosition]
                selectedSpPackageId = buySpList[itemPosition].id!
                selectedSpPackagePrice = buySpList[itemPosition].qarPrice!
                
                let pay = SKPayment(product: spProductList[itemPosition])
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(pay as SKPayment)
            }else{
                SnackBarManager.showSnackBar(message: "Please wait...")
            }
            
        }else{
            print(CLASS_NAME+" -- onClickBuyNow -- position is empty")
        }
    }
}


//storekit extension
extension PopupBuySPViewController : SKProductsRequestDelegate, SKPaymentTransactionObserver {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(CLASS_NAME+" -- productsRequest() - \(response.products)")
        spProductList.removeAll()
        
        let myProducts = response.products
        for product in myProducts {
            print(CLASS_NAME+" -- productsRequest() -- \(product.productIdentifier)")
            print(CLASS_NAME+" -- productsRequest() -- \(product.localizedTitle)")
            print(CLASS_NAME+" -- productsRequest() -- \(product.localizedDescription)")
            print(CLASS_NAME+" -- productsRequest() -- \(product.price)")
            
            spProductList.append(product)
        }
        
        //dismiss progress bar
        SVProgressHUD.dismiss()
        
    }
    
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print(CLASS_NAME+" -- paymentQueue()")
        
        for transaction: AnyObject in transactions {
            let trans = transaction as! SKPaymentTransaction
            print(trans.error)
            
            switch trans.transactionState {
            case .purchased:
                print(CLASS_NAME+" -- paymentQueue() -- \(spProduct.productIdentifier) -- purchased successfully...")
                
                let prodID = spProduct.productIdentifier
                queue.finishTransaction(trans)
                
                //update customer sp on server and send notification
                updtaeCustomerSp()
            case .failed:
                 print(CLASS_NAME+" -- paymentQueue() -- \(spProduct.productIdentifier) -- purchased failed...")
                queue.finishTransaction(trans)
                 
                 //send failed notification
                 sendFailedNotificationToCustomer()
//                 SnackBarManager.showSnackBar(message: "Failed to purchase sp")
                break
            default:
                print("Default")
                break
            }
        }
        
    }
    
    
}


