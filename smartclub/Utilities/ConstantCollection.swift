//
//  ConstantCollection.swift
//  smartclub
//
//  Created by RASHED on 1/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

open class ConstantCollection: NSObject {
    
    open static let PREFERENCE_USER = "user"
    open static let PREFERENCE_USER_LOGGEDIN = "user_loggedin"
    open static let PREFERENCE_USER_ID = "user_id"
    open static let PREFERENCE_USER_EMAIL = "user_email"
    open static let PREFERENCE_USER_NAME = "user_name"
    open static let PREFERENCE_USER_PROFILE_PICTURE = "profile_picture"
    open static let PREFERENCE_USER_EARNED_SP = "sp_earned"
    open static let PREFERENCE_USER_REFERRAL_CODE = "referral_code"
    open static let PREFERENCE_USER_TYPE = "user_type"
    open static let PREFERENCE_CUSTOMER_TYPE = "customer_type"
    open static let PREFERENCE_USER_ACCOUNT_STATUS = "account_status"
    open static let PREFERENCE_USER_DEVICE_TOKEN = "device_token"
    
}
