//
//  RoundNumberFormatter.swift
//  smartclub
//
//  Created by Mac-Admin on 2/12/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class RoundNumberFormatter: NSObject {
//    static func formatPoints(num: Double) ->String{
//        let thousandNum = num/1000
//        let millionNum = num/1000000
//        if num >= 1000 && num < 1000000{
//            if(floor(thousandNum) == thousandNum){
//                return("\(Int(thousandNum))k")
//            }
//            return("\(thousandNum.rounded())k")
//        }
//        if num > 1000000{
//            if(floor(millionNum) == millionNum){
//                return("\(Int(thousandNum))k")
//            }
//            return ("\(millionNum.rounded())M")
//        }
//        else{
//            if(floor(num) == num){
//                return ("\(Int(num))")
//            }
//            return ("\(num)")
//        }

    static func roundedWithAbbreviations(num: Double) -> String {
        //let num = Double.self
        let thousand = num/1000
        let million = num/1000000
        if million >= 1.0 {
            return "\(round(million*10)/10)M"
        }
        else if thousand >= 1.0 {
            return "\(round(thousand*10)/10)K"
        }
        else {
            return "\(Int(num))"
        }
    }
    
    }
    
    
    //

    
    
    
    
    


