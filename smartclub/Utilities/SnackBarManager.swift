//
//  SnackBarManager.swift
//  smartclub
//
//  Created by Mac-Admin on 1/1/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TTGSnackbar

open class SnackBarManager: NSObject {

    //show snackbar
    open static func showSnackBar(message:String) {
        //setup snackbar
        let snackbar = TTGSnackbar.init(message: message, duration: .long)
        snackbar.backgroundColor = UIColor(red:0.15, green:0.23, blue:0.56, alpha:1.0)
        snackbar.show()
    }
    
    //show snackbar for internet setting
    open static func showSnackBarForInternetSetting() {
        //setup snackbar
        let snackbar = TTGSnackbar.init(message: "No internet connection", duration: .long, actionText: "Turn On")
        { (snackbar) -> Void in
            let url = URL(string: "App-Prefs:root=WIFI") //for WIFI setting app
            let app = UIApplication.shared
            app.openURL(url!)
            snackbar.dismiss()
        }
        snackbar.show()
    }
}
