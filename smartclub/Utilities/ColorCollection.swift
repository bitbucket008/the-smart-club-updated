//
//  ColorCollection.swift
//  smartclub
//
//  Created by Mac-Admin on 1/1/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ColorCollection: NSObject {
    open static let COLOR_DEEP_BLUE:UInt32 = 0x273b90
    open static let COLOR_LIGHT_GREEN:UInt32 = 0x9ad2d2
    open static let COLOR_DEEP_GRAY:UInt32 = 0xBABABA
    open static let COLOR_LIGHT_GRAY:UInt32 = 0xF2F2F2
}
