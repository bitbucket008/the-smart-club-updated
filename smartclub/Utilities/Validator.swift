//
//  Validator.swift
//  smartclub
//
//  Created by Mac-Admin on 1/1/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class Validator: NSObject {
    /// Check input UserName
    ///
    /// - Parameter username: username
    /// - Returns: true or false
    static func isValidUsername(username:String) -> Bool {
        if username.isEmpty{
            return false
        }
        
        return true
    }
    
    /// Check input company id
    ///
    /// - Parameter username: username
    /// - Returns: true or false
    static func isValidCompanyName(companyId:String?) -> Bool {
        if let id = companyId{
            return true
        }
        
        return false
    }
    
    
    
    /// Ceck input Password
    ///
    /// - Parameter password: password
    /// - Returns: true or false
    static func isValidPassword(password:String) -> Bool {
        if password.isEmpty {
            return false
        }
        
        return true
    }
    
    /// Ceck input confirm Password
    ///
    /// - Parameter password: password
    /// - Returns: true or false
    static func isValidConfirmPassword(password:String, confirmPassword:String) -> Bool {
        if password != confirmPassword {
            return false
        }
        
        return true
    }
    
    
    /// ceck input Email valid formate
    ///
    /// - Parameter email: email
    /// - Returns: true or false
    static func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let formatter = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return formatter.evaluate(with: email)
    }
    
    /// Check input string type
    ///
    /// - Parameter value: String
    /// - Returns: true or false
    static func isValidString(value:String) -> Bool {
        if value.isEmpty{
            return false
        }
        
        return true
    }
}
