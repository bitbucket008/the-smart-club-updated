//
//  ApiManager.swift
//  smartclub
//
//  Created by Mac-Admin on 1/2/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

open class ApiManager: NSObject {
    open static let SERVER_BASE_URL:String = "http://thesmartclub.online" //"http://app.thesmartclubapp.com"
    open static let API_VERSION:String = "v1"
}
//  http://app.thesmartclubapp.com
