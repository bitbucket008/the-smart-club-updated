//
//  ApiEndpoints.swift
//  smartclub
//
//  Created by Mac-Admin on 1/2/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

open class ApiEndpoints: NSObject {
    //server base url
    private static let BASE_URL:String = ApiManager.SERVER_BASE_URL
    
    //api endpoints
    //Registration and Login
    open static let URI_REGISTRATION_OTHER:String = BASE_URL+"/api/register/other"
    open static let URI_REGISTRATION_STUDENT:String = BASE_URL+"/api/register/student"
    open static let URI_LOGIN:String = BASE_URL+"/api/login"
    open static let URI_REGISTRATION_COMPANY_LIST:String = BASE_URL+"/api/companies"
    open static let URI_REGISTRATION_ADD_COMPANY:String = BASE_URL+"/api/companies"
    open static let URI_REGISTRATION_ADD_INSTITUTE:String = BASE_URL+"/api/companies"
    open static let URI_FORGOT_PASSWORD_VERIFICATION_CODE:String = BASE_URL+"/api/password-reset-request"
    open static let URI_CHANGE_PASSWORD_REQUEST:String = BASE_URL+"/api/password-reset"
    
    //Dashboard and details 
    open static let URI_BUSINESS_CATEGORY:String = BASE_URL+"/api/businesses"
    open static let URI_FLASH_DEAL:String = BASE_URL+"/api/deals?flash=true&platform=mobile"
    open static let URI_VENDOR_LIST:String = BASE_URL+"/api/vendors?platform=mobile"
    open static let URI_FLASH_DEAL_DETAILS:String = BASE_URL+"/api/deals"
    open static let URI_FLASH_DEAL_REDEEM:String = BASE_URL+"/api/redeemed-deals"
    open static let URI_VENDOR_DETAILS:String = BASE_URL+"/api/vendors"
    open static let URI_VENDOR_DEAL_REDEEM:String = BASE_URL+"/api/redeemed-deals"
    open static let URI_VENDORS_BY_CATEGORY:String = BASE_URL+"/api/businesses"
    
    //Vendor   
    open static let URI_VENDORS:String = "/vendors"
    open static let URI_VENDOR_PROFILE:String = BASE_URL+"/api/vendors"
    
    //Smart shop
    open static let URI_DEALS:String = "/deals"
    open static let URI_SMART_SHOP_OFFERS:String = BASE_URL+"/api/customers"
    open static let URI_OFFERS:String = "/offers"
    open static let URI_SMART_SHOP_OWNED_OFFERS:String = BASE_URL+"/api/customers"
    open static let URI_REEDEMED_OFFERS:String = "/redeemed-offers"
    open static let URI_SMART_SHOP_BUY_NOW_OFFERS:String = BASE_URL+"/api/redeemed-offers"
    open static let URI_SMART_SHOP_REDEEM_NOW_OFFERS:String = BASE_URL+"/api/redeemed-offers"
    
    //Buy now SP
    open static let URI_BUY_NOW_SP:String = BASE_URL+"/api/smart-packages?platform=mobile&col=sp_val&sort=asc"
    
    //Account
    open static let URI_EDIT_EMAIL:String = BASE_URL+"/api/users"
    open static let URI_EDIT_PASSWORD:String = BASE_URL+"/api/users"
    open static let URI_PROFILE_INFORMATION:String = BASE_URL+"/api/customers"
    open static let URI_UPDATE_PROFILE_INFORMATION:String = BASE_URL+"/api/customers"
    
    //Payment
    open static let URI_ADD_CREDIT_CARD:String = BASE_URL+"/api/customers"
    open static let URI_CREDIT_CARD:String = "/credit-cards"
    open static let URI_PAYMENT_RESPONSE:String = BASE_URL+"/api/ios/payment-responses"
    
    //Survey
    open static let URI_SURVEY_LIST:String = BASE_URL+"/api/surveys"
    open static let URI_STORE_SURVEY_RESULTS:String = BASE_URL+"/api/survey-results"
    
    //Payment Transaction
    open static let URI_VISTA_MONEY_PERFORM_TRANSACTION:String = "https://vistamoney.info/paymentgateway/payments/performXmlTransaction"
    
}
