//
//  LocalStorage.swift
//  smartclub
//
//  Created by RASHED on 1/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class LocalStorage{
    private var preference:UserDefaults!
    private var user:User?
    
    init() {
        preference = UserDefaults.standard
        user = User()
    }
    
    //store user data
    func storeUser(user:User) {
        preference.set(user.username!, forKey: ConstantCollection.PREFERENCE_USER_NAME)
        preference.set(user.email!, forKey: ConstantCollection.PREFERENCE_USER_EMAIL)
        preference.set(user.id!, forKey: ConstantCollection.PREFERENCE_USER_ID)
        preference.set(user.customerType, forKey: ConstantCollection.PREFERENCE_CUSTOMER_TYPE)
        preference.set(user.userType!, forKey: ConstantCollection.PREFERENCE_USER_TYPE)
        preference.set(user.earnedSp!, forKey: ConstantCollection.PREFERENCE_USER_EARNED_SP)
        preference.set(user.referralCode!, forKey: ConstantCollection.PREFERENCE_USER_REFERRAL_CODE)
//        preference.set(user.deviceToken!, forKey: ConstantCollection.PREFERENCE_USER_DEVICE_TOKEN)
//        preference.set(user, forKey: ConstantCollection.PREFERENCE_USER)
        preference.synchronize()
    }
    
    //retrive user data
    func getUser() -> User {
        user?.username = preference.string(forKey: ConstantCollection.PREFERENCE_USER_NAME)
        user?.email =  preference.string(forKey: ConstantCollection.PREFERENCE_USER_EMAIL)
        user?.id =  preference.string(forKey: ConstantCollection.PREFERENCE_USER_ID)
        user?.userType =  preference.string(forKey: ConstantCollection.PREFERENCE_USER_TYPE)
        user?.customerType =  preference.string(forKey: ConstantCollection.PREFERENCE_CUSTOMER_TYPE)
        user?.earnedSp =  preference.integer(forKey: ConstantCollection.PREFERENCE_USER_EARNED_SP)
        user?.referralCode =  preference.string(forKey: ConstantCollection.PREFERENCE_USER_REFERRAL_CODE)
//        user?.deviceToken =  preference.string(forKey: ConstantCollection.PREFERENCE_USER_DEVICE_TOKEN)
//        user =  (preference.object(forKey: ConstantCollection.PREFERENCE_USER) as! User)
        return user!
    }
    
    //set user device token
    func setUserDeviceToken(deviceToken:String) {
        preference.set(deviceToken, forKey: ConstantCollection.PREFERENCE_USER_DEVICE_TOKEN)
        preference.synchronize()
    }

    //set user device token
    func getUserDeviceToken() -> String?{
        return preference.string(forKey: ConstantCollection.PREFERENCE_USER_DEVICE_TOKEN)
    }
    
    //set user logged in
    func setUserLoggedIn(loggedIn:Bool) {
        preference.set(loggedIn, forKey: ConstantCollection.PREFERENCE_USER_LOGGEDIN)
        preference.synchronize()
    }
    
    //check user is loggedin or not
    func isLoggedInUser() -> Bool {
        return preference.bool(forKey: ConstantCollection.PREFERENCE_USER_LOGGEDIN)
    }
    
    //logout user
    func logOut()  {
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_NAME)
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_EMAIL)
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_TYPE)
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_CUSTOMER_TYPE)
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_REFERRAL_CODE)
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_DEVICE_TOKEN)
        preference.set(false, forKey: ConstantCollection.PREFERENCE_USER_ID)
//        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_EARNED_SP)
        preference.set(false, forKey: ConstantCollection.PREFERENCE_USER)
        preference.synchronize()
    }
}
