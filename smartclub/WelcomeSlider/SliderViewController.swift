//
//  AboutPageViewController.swift
//  Temp
//
//  Created by Admin on 20/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CHIPageControl


class SliderViewController : UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    // The custom UIPageControl
     @IBOutlet weak var pageControl: CHIPageControlAji!
    
    @IBOutlet weak var closeBtn:UIButton!
    
    // The UIPageViewController
    var pageContainer: UIPageViewController!
    
    // The pages it contains
    var pages = [UIViewController]()
    
    // Track the current index
    var currentIndex: Int?
    private var pendingIndex: Int?
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the pages
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let page1: UIViewController! = storyboard.instantiateViewController(withIdentifier: "sp1")
        let page2: UIViewController! = storyboard.instantiateViewController(withIdentifier: "sp2")
        let page3: UIViewController! = storyboard.instantiateViewController(withIdentifier: "sp3")
        let page4: UIViewController! = storyboard.instantiateViewController(withIdentifier: "sp4")
        pages.append(page1)
        pages.append(page2)
        pages.append(page3)
        pages.append(page4)
        
        // Create the page container
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([page1], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        // Add it to the view
        view.addSubview(pageContainer.view)
        
        // Configure our custom pageControl
        view.bringSubview(toFront: pageControl)
       // view.bringSubview(toFront:closeBtn)
        pageControl.numberOfPages = pages.count
        
    }
    
    // MARK: - UIPageViewController delegates
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        //let currentIndex = pages.index(of: viewController)!
        //            if currentIndex == 0 {
        //                return nil
        //            }
        //            let previousIndex = abs((currentIndex - 1) % pages.count)
        //            return pages[previousIndex]
        
        
        
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == pages.count-1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % pages.count)
        return pages[nextIndex]
    }
    
    
    
    
    @IBAction func closebtnAction(sender:Any?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        //            let currentIndex = pages.index(of: viewController)!
        //            if currentIndex == pages.count-1 {
        //                return nil
        //            }
        //            let nextIndex = abs((currentIndex + 1) % pages.count)
        //            return pages[nextIndex]
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == 0 {
            return nil
        }
        let previousIndex = abs((currentIndex - 1) % pages.count)
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.index(of: pendingViewControllers.first!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentIndex = pendingIndex
            if let index = currentIndex {
                 pageControl.set(progress: index, animated: true)
            }
        }
    }
}

