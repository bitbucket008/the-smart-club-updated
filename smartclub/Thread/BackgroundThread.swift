//
//  BackgroundThread.swift
//  smartclub
//
//  Created by Mac-Admin on 1/2/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class BackgroundThread: NSObject {
    public static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
}
