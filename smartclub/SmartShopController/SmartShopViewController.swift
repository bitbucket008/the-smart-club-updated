//
//  SmartBagViewController.swift
//  smartclub
//
//  Created by Admin on 03/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SVProgressHUD
import QuiltView

class SmartShopViewController: UIViewController {
    
    //MARK: Properties
    fileprivate let CLASS_NAME:String = "SmartShopViewController"
    fileprivate let localStorage = LocalStorage()
    fileprivate var offerList:[Offer] = [Offer]()
    fileprivate var ownedOfferList:[OwnedOffer] = [OwnedOffer]()
    open var spChangeDelegate:SmartPointChangeDelegate?
//    fileprivate var userId:String = "1cac87bdaa504a003a920c4e9fa027ae"
    

    //MARK: Outlets
//    @IBOutlet weak var parchasedTableView: UITableView!
//    @IBOutlet weak var redeemdeTableView: UITableView!
    @IBOutlet weak var btnOffers: UIButton!
    @IBOutlet weak var btnOwned: UIButton!
    @IBOutlet weak var cvOffer: UICollectionView!
    @IBOutlet weak var cvOwnedOffer: UICollectionView!
//    @IBOutlet weak var lblEarnedSp: UILabel!
    
    @IBOutlet weak var uivTavIndicatorContainer: UIView!
    
    @IBOutlet weak var uivTabIndicator: UIView!
    
    
    //MARK: UI Elements
    fileprivate var rcOffers:UIRefreshControl!
    fileprivate var rcOwnedOffers:UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       //initialize ui
        initUI()
        
        //get offer list from server
        getOfferList()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UI Method
    //initialize ui
    private func initUI() -> Void {
        cvOwnedOffer.isHidden = true
        cvOffer.tag = 203;
        cvOwnedOffer.tag = 204
        cvOwnedOffer.clearsContextBeforeDrawing = true
        
        cvOffer.delegate = self;
        cvOffer.dataSource = self
        cvOffer.clearsContextBeforeDrawing = true
        
//        let alignedFlowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .justified, verticalAlignment: .top)
//        cvOffer.collectionViewLayout = alignedFlowLayout
//        let alignedFlowLayout2 = AlignedCollectionViewFlowLayout(horizontalAlignment: .justified, verticalAlignment: .top)
//        cvOwnedOffer.collectionViewLayout = alignedFlowLayout2
        
        //set layout of cvOffer
        if let layoutForCvOffer = cvOffer.collectionViewLayout as? OptimizedCollectionViewLayout{
            layoutForCvOffer.delegate = self
        }
        
        
        //set layout of cvOwnenOffer
        if let layoutForCvOwnedOffer = cvOwnedOffer.collectionViewLayout as? OptimizedCollectionViewLayout{
            layoutForCvOwnedOffer.delegate = self
        }
        
        self.cvOffer.collectionViewLayout.invalidateLayout()
        
        cvOffer.register(UINib(nibName: "SmartShopOfferCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SmartShopOfferCollectionViewCell")
        
        cvOwnedOffer.register(UINib(nibName: "SmartShopOwnedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SmartShopOwnedCollectionViewCell")
        
        //change select button
        chnageSelectedButtonStyle(btn:btnOffers)
        
        //set eanred sp
//        lblEarnedSp!.text = String(describing: RoundNumberFormatter.formatPoints(num: Double(localStorage.getUser().earnedSp!)))
        
        // Setup Refresh Controller
        setupRefreshController()
        
//        cvOffer.alignmentRect(forFrame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    // Setup Refresh Controller
    private func setupRefreshController() -> Void {
        //refresh controller for offers
        rcOffers = UIRefreshControl()
        rcOffers.tintColor = UIColor.clear
//        rcOffers.attributedTitle = NSAttributedString(string: "Reloading...")
        rcOffers.addTarget(self, action: #selector(reloadOfferData), for: UIControlEvents.valueChanged)
        cvOffer.addSubview(rcOffers)
        
        //refresh controller for owned offers
        rcOwnedOffers = UIRefreshControl()
        rcOwnedOffers.tintColor = UIColor.clear
//        rcOwnedOffers.attributedTitle = NSAttributedString(string: "Reloading...")
        rcOwnedOffers.addTarget(self, action: #selector(reloadOfferData), for: UIControlEvents.valueChanged)
        cvOwnedOffer.addSubview(rcOwnedOffers)
    }
    
    @objc private func reloadOfferData() -> Void {
        print(CLASS_NAME+" -- reloadOfferData()")
        //get offer list from server
        getOfferList()
    }
    
    //MARK: Network call
    //get offer list from server
    private func getOfferList() -> Void {
        //show progress dialog
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_SMART_SHOP_OFFERS+"/"+localStorage.getUser().getId()!+ApiEndpoints.URI_OFFERS)!,
            method: .get,
            parameters:[ "status":"create", "platform":"mobile"])
            .responseJSON { (response) -> Void in
                
                //end refreshing
                self.rcOffers.endRefreshing()
                
                //get owned offer list
                self.getOwnedOfferList()
                
                //parse json
                if response.result.isSuccess{
                        let swiftyJson = JSON(response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            
                            //remove all offer first
                            self.offerList.removeAll()
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let id = swiftyJson["data"][i]["id"].stringValue
                                    let name = swiftyJson["data"][i]["name"].stringValue
                                    let spNeed = swiftyJson["data"][i]["sp_need"].intValue
                                    let offerDescription = swiftyJson["data"][i]["description"].string
                                    let orgName = swiftyJson["data"][i]["org_name"].string
                                    let identityCode = swiftyJson["data"][i]["identity_code"].string
                                    let vendorId = swiftyJson["data"][i]["vendor_id"].string
                                    let image = swiftyJson["data"][i]["image"].string
                                    self.offerList.append(Offer(id:id, name:name, spNeed:spNeed, offerDescription:offerDescription, orgName:orgName, identityCode:identityCode, vendorId:vendorId,  image:image))
                                    print(self.CLASS_NAME+" -- getOfferList()  -- offer  description -- "+(self.offerList[i].offerDescription!))
                                }
                                
                                //reload tableview data
                                self.cvOffer.reloadData()
//                                self.cvOffer.collectionViewLayout.invalidateLayout()
                            }
                            
//                            SnackBarManager.showSnackBar(message: "Offer list loaded")
                            print(self.CLASS_NAME+" -- getOfferList()  -- response -- category loaded")
                            
                        }else{
//                            SnackBarManager.showSnackBar(message: "Request failed")
                            print(self.CLASS_NAME+" -- getOfferList()  -- response -- Request failed")
                        }
                }else{
                    print(self.CLASS_NAME+" -- getOfferList()  -- response -- request failed")
//                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getOfferList()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //get owned offer list from server
    private func getOwnedOfferList() -> Void {
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_SMART_SHOP_OWNED_OFFERS+"/"+localStorage.getUser().getId()!+ApiEndpoints.URI_REEDEMED_OFFERS)!,
            method: .get,
            parameters:["platform":"mobile", "status":"create","redeem":"false"])
            .responseJSON { (response) -> Void in
                
                //end refreshing
                self.rcOwnedOffers.endRefreshing()
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        
                        //remove all owned offer first
                        self.ownedOfferList.removeAll()
                        
                        if(swiftyJson["data"].arrayValue.count > 0){
                            for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                let id = swiftyJson["data"][i]["id"].stringValue
                                let name = swiftyJson["data"][i]["name"].stringValue
                                let spNeed = swiftyJson["data"][i]["sp_need"].intValue
                                let offerDescription = swiftyJson["data"][i]["description"].string
                                let orgName = swiftyJson["data"][i]["org_name"].string
                                let identityCode = swiftyJson["data"][i]["identity_code"].string
                                let vendorId = swiftyJson["data"][i]["vendor_id"].string
                                let image = swiftyJson["data"][i]["image"].string
                                self.ownedOfferList.append(OwnedOffer(id:id, name:name, spNeed:spNeed, offerDescription:offerDescription, orgName:orgName, identityCode:identityCode, vendorId:vendorId,  image:image))
                                print(self.CLASS_NAME+" -- getOwnedOfferList()  -- owned offer  identity code -- "+(self.ownedOfferList[i].identityCode!))
                            }
                            
                            //reload tableview data
                            self.cvOwnedOffer.reloadData()
//                            self.cvOwnedOffer.collectionViewLayout.invalidateLayout()
                        }
                        
//                        SnackBarManager.showSnackBar(message: "Owned Offer list loaded")
                        print(self.CLASS_NAME+" -- getOwnedOfferList()  -- response -- owned offer  loaded")
                        
                    }else{
//                        SnackBarManager.showSnackBar(message: "Request failed")
                        print(self.CLASS_NAME+" -- getOwnedOfferList()  -- response -- Request failed")
                    }
                }else{
                    print(self.CLASS_NAME+" -- getOwnedOfferList()  -- response -- request failed")
//                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getOwnedOfferList()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    //post buy now offer to server
    private func buyNowOffer(offerPosition:Int) -> Void {
        //show progress dialog
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_SMART_SHOP_BUY_NOW_OFFERS)!,
            method: .post,
            parameters:["customer_id": localStorage.getUser().getId()!, "offer_id":offerList[offerPosition].id, "vendor_id":offerList[offerPosition].vendorId!, "redeem":"false", "platform":"mobile"], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
//                        let user = self.localStorage.getUser()
//                        user.earnedSp = Optional(self.localStorage.getUser().earnedSp! - self.offerList[offerPosition].spNeed)
//                        self.localStorage.storeUser(user: user) 
                        
                        //show sp to view
//                        self.spChangeDelegate?.onSmartPointChanged(newSP: (self.localStorage.getUser().earnedSp))
                        self.spChangeDelegate?.getSmartPointFromServer()
//                      self.lblEarnedSp.text = String((self.localStorage.getUser().earnedSp)!)
                        
                        //remove current offer
//                        self.getOfferList()
                        
                        //reload owned offer list
                        self.ownedOfferList.append(OwnedOffer(id:self.offerList[offerPosition].id, name:self.offerList[offerPosition].name, spNeed:self.offerList[offerPosition].spNeed, offerDescription:self.offerList[offerPosition].offerDescription!, orgName:self.offerList[offerPosition].orgName!, identityCode:self.offerList[offerPosition].identityCode!, vendorId:self.offerList[offerPosition].vendorId!,  image:self.offerList[offerPosition].image!))
    
                            self.cvOwnedOffer.reloadData()
                        
                        //reload offer list
//                        self.offerList.remove(at: offerPosition)
//
//                        print(self.CLASS_NAME+" -- buyNowOffer() -- arraySize --  \(self.offerList)")
//
//                        if self.offerList.count > 0 {
//                            self.offerList = self.offerList.rearrange(array: self.offerList, fromIndex: offerPosition+1, toIndex: self.offerList.count)
//                        }
//
//                        self.cvOffer.collectionViewLayout.invalidateLayout()
//                        self.cvOffer.reloadData()
                        
                        }else{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- buyNowOffer()  -- response -- Already bought this offer")
                        }
                    }else{
                    print(self.CLASS_NAME+" -- buyNowOffer()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- buyNowOffer()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    //post redeem now offer to server
    private func redeemNowOffer(offerPosition:Int) -> Void {
        //show progress dialog
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_SMART_SHOP_REDEEM_NOW_OFFERS)!,
            method: .post,
            parameters:["customer_id": localStorage.getUser().getId()!, "offer_id":ownedOfferList[offerPosition].id, "vendor_id":ownedOfferList[offerPosition].vendorId!, "redeem":true], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        //removed the offer from list
                        self.ownedOfferList.remove(at: offerPosition)
                        
                        //reload tableview data
                        self.cvOwnedOffer.reloadData()
                        self.cvOwnedOffer.collectionViewLayout.invalidateLayout()
                        
                        print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- You redeemed an offer")
                        
                        //show vendor code
//                        self.showVendorCodeDialog(offerPosition: offerPosition)
                        self.showRedemptionCodeDialog(redemptionCode: swiftyJson["data"][0]["redemption_code"].stringValue)
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- Error from server")
                    }
                }else{
                    print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    //show vendor code dialog
    func showVendorCodeDialog(offerPosition: Int?) {
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "RedeemedSuccessViewController") as! RedeemedSuccessViewController
        vc.vendorCode = ownedOfferList[offerPosition!].identityCode!
        self.present(vc, animated: true, completion: nil)
    }
    
    //show reddemedtion code
    func showRedemptionCodeDialog(redemptionCode: String) {
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "RedeemedSuccessViewController") as! RedeemedSuccessViewController
        vc.vendorCode = redemptionCode
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Actions
    //when click on tab button
    @IBAction func sectionButtonAction(_ sender: UIButton) {
        if sender.tag == 201 {
            changeUnSelectedBtnStyle(btn: btnOwned)
            chnageSelectedButtonStyle(btn: sender)
        
            cvOffer.isHidden = false
            cvOffer.delegate = self
            cvOffer.dataSource = self
            cvOffer.collectionViewLayout.invalidateLayout()
            
            cvOwnedOffer.isHidden = true
            cvOwnedOffer.delegate = nil
            cvOwnedOffer.dataSource = nil
            
            cvOffer.reloadData()
        } else {
            changeUnSelectedBtnStyle(btn: btnOffers)
            chnageSelectedButtonStyle(btn: sender)
            
            cvOffer.isHidden = true
            cvOffer.delegate = nil
            cvOffer.dataSource = nil
            
            cvOwnedOffer.isHidden = false
            cvOwnedOffer.delegate = self
            cvOwnedOffer.dataSource = self
            cvOwnedOffer.collectionViewLayout.invalidateLayout()
            
            cvOwnedOffer.reloadData()
            
        }
        
    }
    
    
    
    //MARK: Tab Buttons
    func chnageSelectedButtonStyle(btn:UIButton) {
//        btn.backgroundColor = UIColor.white
//        btn.setTitleColor(UIColor(red:0.26, green:0.18, blue:0.49, alpha:1.0), for: .normal)
//        btn.layer.opacity = 1;
        
        UIView.animate(withDuration: 0.2, animations: {
            if btn.tag == 201 {
                self.uivTabIndicator.frame = CGRect(x:0, y: 0, width: self.uivTabIndicator.frame.width, height: self.uivTabIndicator.frame.height)
            }else{
                self.uivTabIndicator.frame = CGRect(x: self.uivTabIndicator.frame.width, y: 0, width: self.uivTabIndicator.frame.width, height: self.uivTabIndicator.frame.height)
            }
        })
    }
    
    func changeUnSelectedBtnStyle(btn:UIButton) {
//        btn.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x9A9A9A, alpha: 1.0)
//        btn.setTitleColor(UIColor.white, for: .normal)
//        btn.layer.opacity = 0.5
    }

}


extension SmartShopViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, OptimizedCollectionViewLayoutDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 203 {
            return offerList.count
        }else{
            return ownedOfferList.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 203 {
            
            let  cell: SmartShopOfferCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmartShopOfferCollectionViewCell", for: indexPath) as! SmartShopOfferCollectionViewCell
            cell.delegate = self
            cell.offerPosition = Optional(indexPath.row)
            cell.layer.cornerRadius = 20
            cell.clearsContextBeforeDrawing = true
            
            //size to fit to views
//            cell.lblOfferTitle!.numberOfLines = 0
//            cell.lblOfferTitle!.sizeToFit()
//            cell.lblOfferDescription!.numberOfLines = 0
//            cell.lblOfferDescription!.sizeToFit()
            
            cell.lblOfferTitle!.text = offerList[indexPath.row].name
            cell.lblSP!.text = String(offerList[indexPath.row].spNeed)
            
            if let offerDescription = offerList[indexPath.row].offerDescription {
                cell.lblOfferDescription!.text = offerDescription
            }
            
            if let orgName = offerList[indexPath.row].orgName {
                cell.lblOrgName!.text = offerList[indexPath.row].orgName
            }
            
            
            if let image = offerList[indexPath.row].image {
                cell.ivOfferImage!.sd_setImage(with: URL(string: ApiManager.SERVER_BASE_URL+image)!, placeholderImage: UIImage(named: "not_found"))
//                cell.ivOfferImage!.sd_addActivityIndicator()
//                cell.ivOfferImage!.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
                //        cell.ivOfferImage!.sd_showActivityIndicatorView()
            }
            
//            cell.layoutIfNeeded()
            return cell
            
        } else {
            let  cell: SmartShopOwnedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmartShopOwnedCollectionViewCell", for: indexPath) as! SmartShopOwnedCollectionViewCell
            
            cell.delegate = self
            cell.offerPosition = Optional(indexPath.row)
            cell.layer.cornerRadius = 20
            cell.clearsContextBeforeDrawing = true
            
            //size to fit to views
//            cell.lblOfferTitle!.numberOfLines = 0
//            cell.lblOfferTitle!.sizeToFit()
//            cell.lblOfferDescription!.numberOfLines = 0
//            cell.lblOfferDescription!.sizeToFit()
            
            cell.lblOfferTitle.text = ownedOfferList[indexPath.row].name
            cell.lblSP.text = String(ownedOfferList[indexPath.row].spNeed)
            
            if let offerDescription = ownedOfferList[indexPath.row].offerDescription {
                cell.lblOfferDescription.text = offerDescription
            }
            
            if let orgName = ownedOfferList[indexPath.row].orgName {
                cell.lblOrgName.text = orgName
            }
            
            if let image = ownedOfferList[indexPath.row].image {
                cell.ivOfferImage!.sd_setImage(with: URL(string: ApiManager.SERVER_BASE_URL+image)!, placeholderImage: UIImage(named: "not_found"))
//                cell.ivOfferImage!.sd_addActivityIndicator()
//                cell.ivOfferImage!.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
                //        cell.ivOfferImage!.sd_showActivityIndicatorView()
            }
            
            return cell
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 203 {
            let estimatedFrameForTitle =  NSString(string:offerList[indexPath.row].name).boundingRect(with: CGSize(width:(cvOffer.frame.size.width/2), height: 1500), options: .usesFontLeading, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 14.0)!], context: nil)
            
            //            let estimatedFrameForTitle =  CGSizeFromString(offerList[indexPath.row].name)
            
            let estimatedFrameForOrgName =  NSString(string:offerList[indexPath.row].orgName!).boundingRect(with: CGSize(width:(cvOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 12.0)!], context: nil)
            
            //            let estimatedFrameForOrgName =  CGSizeFromString(offerList[indexPath.row].orgName!)
            
            let estimatedFrameForDescription =  NSString(string:offerList[indexPath.row].offerDescription!).boundingRect(with: CGSize(width:(cvOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.5)], context: nil)
            
            //            let estimatedFrameForDescription =  CGSizeFromString(offerList[indexPath.row].offerDescription!)
            
            let height = estimatedFrameForTitle.height + estimatedFrameForOrgName.height + estimatedFrameForDescription.height
            
            print(CLASS_NAME+" -- sizeForItemAt -- \(height)")
        
            return CGSize(width: (cvOffer.frame.size.width/2)-15 , height: 180+50+height);
        }else{
            let estimatedFrameForTitle =  NSString(string: ownedOfferList[indexPath.row].name).boundingRect(with: CGSize(width:(cvOwnedOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 14.0)!], context: nil)
            
            //            let estimatedFrameForTitle =  CGSizeFromString(offerList[indexPath.row].name)
            
            let estimatedFrameForOrgName =  NSString(string: ownedOfferList[indexPath.row].orgName!).boundingRect(with: CGSize(width:(cvOwnedOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 12.0)!], context: nil)
            
            //            let estimatedFrameForOrgName =  CGSizeFromString(offerList[indexPath.row].orgName!)
            
            let estimatedFrameForDescription =  NSString(string: ownedOfferList[indexPath.row].offerDescription!).boundingRect(with: CGSize(width:(cvOwnedOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)], context: nil)
            
            //            let estimatedFrameForDescription =  CGSizeFromString(ownedOfferList[indexPath.row].offerDescription!)
            
            let height = estimatedFrameForTitle.height + estimatedFrameForOrgName.height + estimatedFrameForDescription.height
            
            print(CLASS_NAME+" -- sizeForItemAt -- \(height)")
            
            return CGSize(width: (cvOwnedOffer.frame.size.width/2)-15 , height: 180+50+height);
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsetsMake(10, 10, 10, 10)

    }
    
    //layout for OptimizedCollectionLayout
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        if collectionView.tag == 203 {
            let estimatedFrameForTitle =  NSString(string:offerList[indexPath.row].name).boundingRect(with: CGSize(width:(cvOffer.frame.size.width/2), height: 1500), options: .usesFontLeading, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 14.0)!], context: nil)
            
//            let estimatedFrameForTitle =  CGSizeFromString(offerList[indexPath.row].name)
            
            let estimatedFrameForOrgName =  NSString(string:offerList[indexPath.row].orgName!).boundingRect(with: CGSize(width:(cvOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 12.0)!], context: nil)
            
//            let estimatedFrameForOrgName =  CGSizeFromString(offerList[indexPath.row].orgName!)
            
            let estimatedFrameForDescription =  NSString(string:offerList[indexPath.row].offerDescription!).boundingRect(with: CGSize(width:(cvOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
            
//            let estimatedFrameForDescription =  CGSizeFromString(offerList[indexPath.row].offerDescription!)
            
            let height = estimatedFrameForTitle.height + estimatedFrameForOrgName.height + estimatedFrameForDescription.height
            
            print(CLASS_NAME+" -- heightForPhotoAtIndexPath -- \(height)")
            
            return 180+70+height
        }else{
            let estimatedFrameForTitle =  NSString(string: ownedOfferList[indexPath.row].name).boundingRect(with: CGSize(width:(cvOwnedOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 14.0)!], context: nil)
            
//            let estimatedFrameForTitle =  CGSizeFromString(offerList[indexPath.row].name)
            
            let estimatedFrameForOrgName =  NSString(string: ownedOfferList[indexPath.row].orgName!).boundingRect(with: CGSize(width:(cvOwnedOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 12.0)!], context: nil)
            
//            let estimatedFrameForOrgName =  CGSizeFromString(offerList[indexPath.row].orgName!)
            
            let estimatedFrameForDescription =  NSString(string: ownedOfferList[indexPath.row].offerDescription!).boundingRect(with: CGSize(width:(cvOwnedOffer.frame.size.width/2), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
            
//            let estimatedFrameForDescription =  CGSizeFromString(ownedOfferList[indexPath.row].offerDescription!)
            
            let height = estimatedFrameForTitle.height + estimatedFrameForOrgName.height + estimatedFrameForDescription.height
            
            print(CLASS_NAME+" -- heightForPhotoAtIndexPath -- \(height)")
            
            return 180+70+height
        }
    }
    
}


//MARK: SmartShop Delegates
extension SmartShopViewController : SmartShopOfferCollectionCellDelegate, SmartShopOwnedCollectionViewCellDelegate, BuyConfirmDialogDelegate, VendorCodeDelegate {
    
    //when click on reededm now button
    func onReedemNowClicked(offerPosition:Int?) {
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "vendorcodevc") as! VendorCodeViewController
        vc.delegate = self
        vc.offerPosition = offerPosition
        self.present(vc, animated: true, completion: nil)
    }
    
    //when click on buy now button
    func onBuyNowClicked(offerPosition:Int?) {
        //show confirm dialog
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "BuyConfirmDialogViewController") as! BuyConfirmDialogViewController
        vc.delegate = self
        vc.offerPosition = offerPosition
        vc.spNeed = Optional(offerList[offerPosition!].spNeed)
        self.present(vc, animated: true, completion: nil)
    }
    
    // BuyConfirmDialogDelegate -> when click on yes button of buy noew confirm dialog
    func onYesButtonCLick(offerPosition: Int?) {
        //buy now this offer
        if let position = offerPosition {
            //check sp point
            if localStorage.getUser().earnedSp! >= offerList[offerPosition!].spNeed{
                self.buyNowOffer(offerPosition: position)
            }else{
                BackgroundThread.background(delay: 0.5, background: nil, completion: {
                    let stboard = UIStoryboard.init(name: "mycart", bundle: nil)
                    let vc = stboard.instantiateViewController(withIdentifier: "popupbuyspvc") as! PopupBuySPViewController
                    vc.headerText = "SORRY YOU DON'T HAVE ENOUGH POINTS."
                    vc.spChangeDelegate = self.spChangeDelegate
                    self.present(vc, animated: true, completion: nil)
                })
            }
        }
    }
    
    //VendorcodeDelegate
    func onPressVendorCode(newCode: String, offerPosition:Int?) {
        print("\n VendorCodePress - "+newCode)
        
        if let identityCode = ownedOfferList[offerPosition!].identityCode{
            if newCode == identityCode{
                self.redeemNowOffer(offerPosition: offerPosition!)
            }else{
                SnackBarManager.showSnackBar(message: "Invalid vendor code")
            }
        }
    }
    
}


extension Array {
    func rearrange<T>(array: Array<T>, fromIndex: Int, toIndex: Int) -> Array<T>{
        var arr = array
        
        for i in fromIndex..<toIndex {
            let element = arr.remove(at: i)
            arr.insert(element, at: i-1)
        }
        
        return arr
    }
}
