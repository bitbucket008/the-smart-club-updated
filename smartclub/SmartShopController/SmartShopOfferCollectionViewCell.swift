//
//  SmartBagParchasedTableViewCell.swift
//  smartclub
//
//  Created by Admin on 04/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit



protocol SmartShopOfferCollectionCellDelegate : NSObjectProtocol {
    func onBuyNowClicked(offerPosition:Int?)
}

class SmartShopOfferCollectionViewCell: UICollectionViewCell {

    //MARK: Porperties
    fileprivate let CLASS_NAME:String = "SmartShopOfferCollectionViewCell"
    open var offerPosition:Int?
    weak var delegate: SmartShopOfferCollectionCellDelegate?
    
    
    //MARK: Outlets
    @IBOutlet weak var lblOfferTitle: UILabel!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var lblOfferDescription: UILabel!
    @IBOutlet weak var lblSP: UILabel!
    @IBOutlet weak var btnReedemNow: UIButton!
    @IBOutlet weak var ivOfferImage: UIImageView!
    @IBOutlet weak var spContainer: UIView!
    @IBOutlet weak var containerView: UIView!

    
    
    @IBAction func redeemBtnAction(_ sender: Any) {
        
        guard let delegate = delegate else {
            return
        }
      
        delegate.onBuyNowClicked(offerPosition: offerPosition)
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initUI()
        
    }
    
    //MARK: UI Method
    private func initUI() -> Void{
        lblSP!.numberOfLines = 0
        lblSP!.sizeToFit()
//        lblOfferTitle.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
//        lblOfferTitle.contentMode = .topLeft
//        lblOfferTitle!.numberOfLines = 0
//        lblOfferTitle!.sizeToFit()
//        lblOrgName!.numberOfLines = 0
//        lblOrgName!.sizeToFit()
//        lblOfferDescription!.numberOfLines = 0
//        lblOfferDescription!.sizeToFit()
        
        //Container view 
        spContainer.layer.cornerRadius = 15
        containerView.layer.cornerRadius = 15
    }

}
