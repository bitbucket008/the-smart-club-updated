//
//  VendorCodeViewController.swift
//  smartclub
//
//  Created by Admin on 04/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

protocol VendorCodeDelegate:NSObjectProtocol {
    func onPressVendorCode(newCode:String, offerPosition:Int?)
}

class VendorCodeViewController: UIViewController {
    //MARK: Properties
    fileprivate let CLASS_NAME = "VendorCodeViewController"
    fileprivate var counter = 0
    open var delegate:VendorCodeDelegate?
    private var vendorCode:String = ""
    private var vendorCodes:[String] = [String]()
    open var offerPosition:Int?
    
    //MARK: Outlets
    @IBOutlet weak var lblDisplay: UILabel!
    @IBOutlet weak var btnCodeOne: UIButton!
    @IBOutlet weak var btnCodeTwo: UIButton!
    @IBOutlet weak var btnCodeThree: UIButton!
    @IBOutlet weak var btnCodeFour: UIButton!
    @IBOutlet weak var btnCodeFive: UIButton!
    @IBOutlet weak var btnCodeSix: UIButton!
    @IBOutlet weak var uivVendorCodeContainer: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //init ui
       initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:: UI Method
    private func initUI() -> Void {
        uivVendorCodeContainer.layer.cornerRadius = 15
        
        var layerOne:CALayer = CALayer()
        layerOne.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x422D7D, alpha: 1.0).cgColor
        layerOne.frame = CGRect(x:0, y:btnCodeOne.frame.height-1, width:btnCodeOne.frame.width, height:2)
        btnCodeOne.layer.addSublayer(layerOne)
        
        var layerTwo:CALayer = CALayer()
        layerTwo.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x422D7D, alpha: 1.0).cgColor
        layerTwo.frame = CGRect(x:0, y:btnCodeTwo.frame.height-1, width:btnCodeTwo.frame.width, height:2)
        btnCodeTwo.layer.addSublayer(layerTwo)
        
        var layerThree:CALayer = CALayer()
        layerThree.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x422D7D, alpha: 1.0).cgColor
        layerThree.frame = CGRect(x:0, y:btnCodeThree.frame.height-1, width:btnCodeThree.frame.width, height:2)
        btnCodeThree.layer.addSublayer(layerThree)
        
        var layerFour:CALayer = CALayer()
        layerFour.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x422D7D, alpha: 1.0).cgColor
        layerFour.frame = CGRect(x:0, y:btnCodeFour.frame.height-1, width:btnCodeFour.frame.width, height:2)
        btnCodeFour.layer.addSublayer(layerFour)
        
        var layerFive:CALayer = CALayer()
        layerFive.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x422D7D, alpha: 1.0).cgColor
        layerFive.frame = CGRect(x:0, y:btnCodeFive.frame.height-1, width:btnCodeFive.frame.width, height:2)
        btnCodeFive.layer.addSublayer(layerFive)
        
        var layerSix:CALayer = CALayer()
        layerSix.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x422D7D, alpha: 1.0).cgColor
        layerSix.frame = CGRect(x:0, y:btnCodeSix.frame.height-1, width:btnCodeSix.frame.width, height:2)
        btnCodeSix.layer.addSublayer(layerSix)
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        
        
        self.dismiss(animated: true, completion: nil)
    }

    
    //MARK: Actions
    //when click on vendor code button
    @IBAction func onClickVendorCode(_ sender: Any, forEvent event: UIEvent) {
        
        counter = counter + 1
        
        let button = sender as! UIButton
        let vendorCodeChar = button.title(for: .normal)!
        vendorCode = vendorCode+vendorCodeChar
        vendorCodes.append(vendorCodeChar)
        
        if counter == 1 {
            btnCodeOne.setTitle((vendorCode.last?.description), for: .normal)
        }else if counter == 2 {
            btnCodeTwo.setTitle((vendorCode.last?.description), for: .normal)
        }else if counter == 3 {
            btnCodeThree.setTitle((vendorCode.last?.description), for: .normal)
        }else if counter == 4 {
            btnCodeFour.setTitle((vendorCode.last?.description), for: .normal)
        }else if counter == 5 {
            btnCodeFive.setTitle((vendorCode.last?.description), for: .normal)
        }else if counter == 6 {
            btnCodeSix.setTitle((vendorCode.last?.description), for: .normal)
        }
        
        if counter > 6 {
            counter = 6
        }
        
//        lblDisplay.text = vendorCode
        
//        if vendorCode.characters.count == 6{
//            delegate?.onPressVendorCode(newCode: vendorCode, offerPosition: offerPosition)
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    
    //when click on delete button
    @IBAction func onCLickDeleteButton(_ sender: UIButton, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onCLickDeleteButton()")
        
        if counter == 6 {
            btnCodeSix.setTitle("", for: .normal)
        }else if counter == 5 {
            btnCodeFive.setTitle("", for: .normal)
        }else if counter == 4 {
            btnCodeFour.setTitle("", for: .normal)
        }else if counter == 3 {
            btnCodeThree.setTitle("", for: .normal)
        }else if counter == 2 {
            btnCodeTwo.setTitle("", for: .normal)
        }else if counter == 1 {
            btnCodeOne.setTitle("", for: .normal)
        }
        
        counter = counter - 1
        
        if counter < 0 {
            counter = 0
        }
        
//        if vendorCode.count > 0 {
//            vendorCode = vendorCode.substring(to: vendorCode.index(before: vendorCode.endIndex))
//        }
        
//        lblDisplay.text = vendorCode
    }
    
    
    //When click on ok button
    @IBAction func onClickOkButton(_ sender: UIButton, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onClickOkButton()")
        
        
        guard let codeOne = btnCodeOne.title(for: .normal), let codeTwo = btnCodeOne.title(for: .normal), let codeThree = btnCodeThree.title(for: .normal), let codeFour = btnCodeFour.title(for: .normal), let codeFive = btnCodeFive.title(for: .normal), let codeSix = btnCodeSix.title(for: .normal)
            else{
                return
        }
        
        
        let finalVendorCode = btnCodeOne.title(for: .normal)! + btnCodeTwo.title(for: .normal)! + btnCodeThree.title(for: .normal)! + btnCodeFour.title(for: .normal)! + btnCodeFive.title(for: .normal)! + btnCodeSix.title(for: .normal)!
        
//        if vendorCode.characters.count == 6{
            delegate?.onPressVendorCode(newCode: finalVendorCode, offerPosition: offerPosition)
            self.dismiss(animated: true, completion: nil)
//        }else{
//            print(CLASS_NAME+" -- onClickOkButton() -- minimum length...")
//        }
    }
    
    
}
