//
//  BuyConfirmDialogViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 1/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

//MARK: Protocols
protocol BuyConfirmDialogDelegate : NSObjectProtocol{
    func onYesButtonCLick(offerPosition:Int?)
}

class BuyConfirmDialogViewController: UIViewController {
    //MARK:Properties
    open var delegate:BuyConfirmDialogDelegate?
    open var offerPosition:Int?
    open var spNeed:Int?
    
    
    //MARK: Outlets
    @IBOutlet weak var lblDialogContent: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet weak var dialogView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // init ui
        initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UI Method
    //init ui
    private func initUI() -> Void {
//        dialogView.layer.cornerRadius = 35
        dialogView.layer.cornerRadius = dialogView.frame.height/10
        lblDialogContent.text = "ARE YOU SURE TO BUY THIS OFFER FOR "+String(spNeed!)+" SP?"
    }


    //MARK: Action
    //when click on cancel
    @IBAction func onCLickCancelButton(_ sender: UIButton, forEvent event: UIEvent) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //when click on yes button
    @IBAction func onCLickYesButton(_ sender: UIButton, forEvent event: UIEvent) {
        delegate?.onYesButtonCLick(offerPosition: offerPosition)
        self.dismiss(animated: true, completion: nil)
    }
    
    //when click on gestiure recognizer
    @IBAction func onCLickGestureRecognizer(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
