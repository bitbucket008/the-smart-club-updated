//
//  SmartBagRedeemedTableViewCell.swift
//  smartclub
//
//  Created by Admin on 04/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit


protocol SmartShopOwnedCollectionViewCellDelegate : NSObjectProtocol {
    func onReedemNowClicked(offerPosition:Int?)
}

class SmartShopOwnedCollectionViewCell: UICollectionViewCell {
    
    //MARK: Porperties
    fileprivate let CLASS_NAME:String = "SmartShopOwnedCollectionViewCell"
    open var offerPosition:Int?
    weak var delegate:SmartShopOwnedCollectionViewCellDelegate?
    
    
    //MARK: Outlets
    @IBOutlet weak var lblOfferTitle: UILabel!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var lblOfferDescription: UILabel!
    @IBOutlet weak var lblSP: UILabel!
    @IBOutlet weak var btnReedemNow: UIButton!
    @IBOutlet weak var ivOfferImage: UIImageView!
    @IBOutlet weak var spContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initUI()
    }


    //MARK: Init method
    //init ui
    private func initUI() -> Void {
        spContainer.layer.cornerRadius = 15
//        self.contentView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    
    //MARK: Delgate methods
    @IBAction func redeemNowBtnAction(_ sender: Any) {
        
        guard let delegate = delegate else {
            return
        }
        
        delegate.onReedemNowClicked(offerPosition: offerPosition)
        
    }

}


