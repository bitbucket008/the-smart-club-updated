//
//  UICornerView.swift
//  smartclub
//
//  Created by Admin on 10/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class UICornerView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override func layoutSubviews() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = 5;
        
    }
    
    

}
