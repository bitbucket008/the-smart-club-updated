//
//  CardViewWithRadious.swift
//  smartclub
//
//  Created by Admin on 30/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CardViewWithRadious: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0
    
    @IBInspectable var shadowOffsetWidth: Int = 4
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.gray
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
       //layer.cornerRadius = cornerRadius
       // let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 0)
//          let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight,.bottomRight], cornerRadii: CGSize(width: 100, height: 100))
//        let layer1 = CAShapeLayer()
//        layer1.masksToBounds = false
//        layer1.shadowColor = shadowColor?.cgColor
//        layer1.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
//        layer1.shadowOpacity = shadowOpacity
//        layer1.shadowPath = path.cgPath
//        layer1.strokeColor = UIColor.white.cgColor
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight,.bottomRight], cornerRadii: CGSize(width: 100, height: 100))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//
//        mask.masksToBounds = false
//        mask.shadowColor = shadowColor?.cgColor
//        mask.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
//        mask.shadowOpacity = shadowOpacity
//        mask.shadowPath = path.cgPath
//
//        self.layer.mask = mask
       //self.layer.addSublayer(layer1)
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale =  1
        
        
    }

}
