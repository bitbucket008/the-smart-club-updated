//
//  RightCornerView.swift
//  smartclub
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class RightCornerView: UIView {
    
    override func layoutSubviews() {
        roundCorners([.topRight, .bottomRight], radius: self.bounds.size.height/2)
    }
    
}



extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.borderColor = UIColor.gray.cgColor
        mask.borderWidth = 2;
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}

