//
//  RightCornerWithBorder.swift
//  smartclub
//
//  Created by Admin on 07/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class RightCornerWithBorder: UIView {
    
    


        override func layoutSubviews() {
            roundCornersWithStock([.topRight, .bottomRight], radius: self.bounds.size.height/2)
        }
    
    
    func roundCornersWithStock(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.strokeColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0).cgColor
        mask.fillColor = UIColor.clear.cgColor
        mask.borderWidth = 3;
        mask.path = path.cgPath
        self.layer.addSublayer(mask)
    }
    

}


class BothCornerWithBorder: UIView {
    
    
    override func layoutSubviews() {
        roundCornersWithStock([.topLeft, .bottomLeft,.topRight, .bottomRight], radius: self.bounds.size.height/2)
    }
    
    
    func roundCornersWithStock(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.strokeColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0).cgColor
        mask.fillColor = UIColor.clear.cgColor
        mask.borderWidth = 3;
        mask.path = path.cgPath
        self.layer.addSublayer(mask)
    }
    
    
}


class VendorDetailTableviewTopBorder: UIView {
    
    
    override func layoutSubviews() {
        //self.layer.masksToBounds = true
        roundCornersWithStock([ .bottomLeft, .bottomRight], radius:10 )
    }
    
    
    func roundCornersWithStock(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.strokeColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0).cgColor
        mask.fillColor = UIColor.clear.cgColor
        mask.borderWidth = 3;
        mask.path = path.cgPath
       // mask.masksToBounds = true
        self.layer.addSublayer(mask)
    }
    
    
}

