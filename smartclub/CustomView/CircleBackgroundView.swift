//
//  CircleBackgroundView.swift
//  smartclub
//
//  Created by Admin on 29/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CircleBackgroundView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2;
        layer.masksToBounds = true;
    }

}
