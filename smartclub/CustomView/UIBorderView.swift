//
//  UIBorderView.swift
//  smartclub
//
//  Created by Admin on 23/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit


class UIBorderView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1.0
        layer.cornerRadius = self.frame.size.height/2
        layer.borderColor = self.backgroundColor?.cgColor
        
    }
    
}


class UIBorderViewWithBorderColor: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 3.0
        layer.cornerRadius = self.frame.size.height/2
        layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: 0x9ad2d2, alpha: 1.0).cgColor

    }
    
}


class UIBorderViewWithLeftCorner: UIView {
   
    
    override func layoutSubviews() {
      
            roundCorners([.topLeft, .bottomLeft], radius: self.bounds.size.height/2)
        
    }
    
    
}

