//
//  StickyHeadersCollectionViewFlowLayout.swift
//  smartclub
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class StickyHeadersCollectionViewFlowLayout: UICollectionViewFlowLayout {
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let layoutAttributes = super.layoutAttributesForElements(in: rect) else { return nil }
        
        // Helpers
        let sectionsToAdd = NSMutableIndexSet()
        var newLayoutAttributes = [UICollectionViewLayoutAttributes]()
        let offset = collectionView!.contentOffset
        for layoutAttributesSet in layoutAttributes {
            if layoutAttributesSet.representedElementCategory == .cell {
                // Add Layout Attributes
                newLayoutAttributes.append(layoutAttributesSet)
                
                // Update Sections to Add
                sectionsToAdd.add(layoutAttributesSet.indexPath.section)
                
            } else if layoutAttributesSet.representedElementCategory == .supplementaryView {
                // Update Sections to Add
                sectionsToAdd.add(layoutAttributesSet.indexPath.section)
            }
        }
        
        for section in sectionsToAdd {
            let indexPath = IndexPath(item: 0, section: section)
            
            if let sectionAttributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath) {
                
                //for header origin
                if offset.y < 0{
                    let deltaY = fabs(offset.y)
                    var frame = sectionAttributes.frame
                    frame.origin.y = frame.minY - deltaY
                    sectionAttributes.frame = frame
                }
                // end of header origin
                
                newLayoutAttributes.append(sectionAttributes)
            }
        }
        
        
        ///////////////////
//        let answer = super.layoutAttributesForElements(in: rect)
        for i in 0..<(layoutAttributes.count) {
            let currentLayoutAttributes: UICollectionViewLayoutAttributes? = layoutAttributes[i]
//            let prevLayoutAttributes: UICollectionViewLayoutAttributes? = layoutAttributes[i - 1] 
            let maximumSpacingX: Int = 10
            let maximumSpacingY: Int = 10
            let originX: Int = Int((currentLayoutAttributes?.frame.minX)!)
            let originY: Int = Int((currentLayoutAttributes?.frame.minY)!)
           
//            if CGFloat(originX + maximumSpacingX) + (currentLayoutAttributes?.frame.size.width)! < collectionViewContentSize.width {
//                var frame: CGRect? = currentLayoutAttributes?.frame
//                frame?.origin.x = CGFloat(originX + maximumSpacingX)
//                currentLayoutAttributes?.frame = frame!
//            }
            
//            if CGFloat(originY + maximumSpacingY) + (currentLayoutAttributes?.frame.size.height)! < collectionViewContentSize.height {
//                var frame: CGRect? = currentLayoutAttributes?.frame
//                frame?.origin.y = CGFloat(originY + maximumSpacingY)
//                currentLayoutAttributes?.frame = frame!
//            }
        }
        //////////////////
        /////////////////
        // Loop through the cache and look for items in the rect
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                newLayoutAttributes.append(attributes)
            }
        }
        //////////////////
        return newLayoutAttributes
    }
    
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        ////////////////////
        supplementaryKindView = elementKind
        ///////////////////
        
        guard let layoutAttributes = super.layoutAttributesForSupplementaryView(ofKind: elementKind, at: indexPath) else { return nil }
        
        guard let boundaries = boundaries(forSection: indexPath.section) else { return layoutAttributes }
        guard let collectionView = collectionView else { return layoutAttributes }
        
        // Helpers
        let contentOffsetY = collectionView.contentOffset.y - 140;
        var frameForSupplementaryView = layoutAttributes.frame
        
        let minimum = boundaries.minimum - frameForSupplementaryView.height
        let maximum = boundaries.maximum - frameForSupplementaryView.height
        
        if contentOffsetY < minimum {
            frameForSupplementaryView.origin.y = minimum
        } else if contentOffsetY > maximum {
            frameForSupplementaryView.origin.y = maximum
        } else {
            frameForSupplementaryView.origin.y = contentOffsetY
        }
        
        layoutAttributes.frame = frameForSupplementaryView
        
        ////
//        cache.append(layoutAttributes)
        ////
        
        return layoutAttributes
    }
    
   
    
    func boundaries(forSection section: Int) -> (minimum: CGFloat, maximum: CGFloat)? {
        // Helpers
        var result = (minimum: CGFloat(0.0), maximum: CGFloat(0.0))
        
        // Exit Early
        guard let collectionView = collectionView else { return result }
        
        // Fetch Number of Items for Section
        let numberOfItems = collectionView.numberOfItems(inSection: section)
        
        // Exit Early
        guard numberOfItems > 0 else { return result }
        
        if let firstItem = layoutAttributesForItem(at: IndexPath(item: 0, section: section)),
            let lastItem = layoutAttributesForItem(at: IndexPath(item: (numberOfItems - 1), section: section)) {
            result.minimum = firstItem.frame.minY
            result.maximum = lastItem.frame.maxY
            
            // Take Header Size Into Account
            result.minimum -= headerReferenceSize.height
            result.maximum -= headerReferenceSize.height
            
            // Take Section Inset Into Account
            result.minimum -= sectionInset.top
            result.maximum += (sectionInset.top + sectionInset.bottom)
        }
        
        return result
    }
    
    
    
    
    ////////////////////////////
    ///////////////////////////
    //1. Pinterest Layout Delegate
    weak var delegate: OptimizedCollectionViewLayoutDelegate!
    
    //2. Configurable properties
    fileprivate var numberOfColumns = 2
    fileprivate var cellPadding: CGFloat = 6
    
    //3. Array to keep a cache of attributes.
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    
    //4. Content height and size
    fileprivate var contentHeight: CGFloat = 0
    
    fileprivate var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    //////////////
    fileprivate var supplementaryKindView:String!
    /////////////
    
    override func prepare() {
        print("StickyHeadersCollectionViewFlowLayout -- prepare()")
        var heightFlashDealsection:CGFloat!
//        self.cache.removeAll()
        
        // 1. Only calculate once
        guard cache.isEmpty == true, let collectionView = collectionView else {
            return
        }
        // 2. Pre-Calculates the X Offset for every column and adds an array to increment the currently max Y Offset for each column
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffset = [CGFloat]()
        for column in 0 ..< numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
        
        
        
        
        // 3. Iterates through the list of items in the first section
        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
//            let indexPath = IndexPath(item: item, section: 0)
//
//            // 4. Asks the delegate for the height of the picture and the annotation and calculates the cell frame.
//            let photoHeight = delegate.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath)
//            let height = cellPadding * 2 + photoHeight
//            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
//            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
//
//            /////
//            let frameForFlasDeals = CGRect(x: 0, y: 0, width: contentWidth, height: photoHeight)
//            let insetFrameForFlasDeals = frameForFlasDeals.insetBy(dx: 0, dy: 0)
//            /////
//
//            // 5. Creates an UICollectionViewLayoutItem with the frame and add it to the cache
//            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            if item == 0{
                print("StickyHeadersCollectionViewFlowLayout -- prepare() -- flashdeal")
                /////// for flash deals
                let indexPathForFlashDeal = IndexPath(item: 0, section: 0)
                let photoHeightForFlashDeal = delegate.collectionView(collectionView, heightForPhotoAtIndexPath: indexPathForFlashDeal)
                let frameForFlasDeals = CGRect(x: 0, y: 0, width: contentWidth, height: photoHeightForFlashDeal)
                let insetFrameForFlasDeals = frameForFlasDeals.insetBy(dx: 0, dy: 0)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPathForFlashDeal)
//                cache.append(attributes)
//                contentHeight = max(contentHeight, insetFrameForFlasDeals.maxY)
                heightFlashDealsection =  insetFrameForFlasDeals.maxY
                yOffset[0] = heightFlashDealsection + 70
                yOffset[1] = heightFlashDealsection + 70
//                //yOffset[column] = yOffset[column] + photoHeightForFlashDeal
//                yOffset[column] = 0 + photoHeightForFlashDeal
//
//                //column = column < (numberOfColumns - 1) ? (column + 1) : 0
//                column = 0
//                ///////end of flash deals
//                attributes.frame = frameForFlasDeals
            }else{
                print("StickyHeadersCollectionViewFlowLayout -- prepare() -- vendor_list - \(yOffset[column])")
                
                let indexPath = IndexPath(item: item, section: 0)
                
                // 4. Asks the delegate for the height of the picture and the annotation and calculates the cell frame.
                let photoHeight = delegate.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath)
                let height = cellPadding * 2 + photoHeight
                let frame = CGRect(x: xOffset[column], y: yOffset[column]+heightFlashDealsection!, width: columnWidth, height: height)
                let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
                
                /////
//                let frameForFlasDeals = CGRect(x: 0, y: 0, width: contentWidth, height: photoHeight)
//                let insetFrameForFlasDeals = frameForFlasDeals.insetBy(dx: 0, dy: 0)
                /////
                
                // 5. Creates an UICollectionViewLayoutItem with the frame and add it to the cache
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = insetFrame
                cache.append(attributes)
                
                // 6. Updates the collection view content height
                contentHeight = max(contentHeight, frame.maxY)
                yOffset[column] = yOffset[column] + height
                
                column = column < (numberOfColumns - 1) ? (column + 1) : 0
            }
            
//            cache.append(attributes)
//
//            // 6. Updates the collection view content height
//            contentHeight = max(contentHeight, frame.maxY)
//            yOffset[column] = yOffset[column] + height
//
//            column = column < (numberOfColumns - 1) ? (column + 1) : 0
        }
    }

//        override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
//
//            if let layoutAttributes = super.layoutAttributesForSupplementaryView(ofKind: supplementaryKindView, at: indexPath){
//                return layoutAttributes
//            } else {
//                return cache[indexPath.item]
//
//            }
//
////            return cache[indexPath.item]
//        }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        self.cache.removeAll()
        contentHeight = 350
    }
    
}
