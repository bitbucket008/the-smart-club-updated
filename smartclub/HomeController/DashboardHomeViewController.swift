//
//  DashboardHomeViewController.swift
//  smartclub
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import CoreLocation
import SDWebImage

import Crashlytics

class DashboardHomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, OptimizedCollectionViewLayoutDelegate,  UITextFieldDelegate {
    
    //MARK: Instance variable
    fileprivate let CLASS_NAME:String = "DashboardHomeViewController"
    open var spChangedDelegate:SmartPointChangeDelegate?
    private var categoryList:[BusinessCategory] = [BusinessCategory]()
    private var flashDealList:[FlashDeal] = [FlashDeal]()
    private var vendorList:[Vendor] = [Vendor]()
    fileprivate var locationManager:CLLocationManager!
    fileprivate var latitude:Double!
    fileprivate var longitude:Double!
    fileprivate var currentLocation:String?
    fileprivate var isCategorySearched = false
    fileprivate var searchText: String!
    fileprivate let localStorage = LocalStorage()
    open var isloginFirstTime:Bool!
    fileprivate var isLoadedFirstTime = true
    fileprivate var currentIndexPath:IndexPath!
    
    //Hide Tabbar
    open var isTabBarHidden = false
    
    //MARK:Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: StickyHeadersCollectionViewFlowLayout!
    
    //MARK: UI Elements
    fileprivate var refreshController:UIRefreshControl!
    fileprivate var headerView:HeaderViewCollectionReusableView!  //for keeping border on category image
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //initialization
        initialization()  
        
        // Setup Refresh Controller
        setupRefreshController()
        
        //get category list
        getCategories()
        dissmisKeyboard()

    }

    //Handle tab bar
//    internal func handleTabBar() -> Void {
//        if isTabBarHidden{
//            hideTabBar()
//        }else{
//            showTabBar()
//        }
//    }
//
//    //show tab bar
//    internal func showTabBar() -> Void {
//        tabBarView.isHidden = false
//    }
//
//    //hide tab bar
//    internal func hideTabBar() -> Void {
//        tabBarView.isHidden = true
//    }
    
    
    
    //MARK: Initial Method
    //initialization
    private func initialization() -> Void {
        print(CLASS_NAME+" -- initialization()")
        
        print(CLASS_NAME+" -- initialization() -- userId : "+localStorage.getUser().getId()!)
        print(CLASS_NAME+" -- initialization() -- deviceToken : \(localStorage.getUserDeviceToken())")
        
        definesPresentationContext = true
        
        //disable image cahing on sdwebimage
//        SDImageCache().config.shouldCacheImagesInMemory = false
        
        //init location manager
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        //get customer update sp
        spChangedDelegate?.getSmartPointFromServer()
        

        //init collectionview
        //set layout of cvOffer
        if let layoutForCollectionView = collectionView.collectionViewLayout as? StickyHeadersCollectionViewFlowLayout{
            layoutForCollectionView.delegate = self
        }
        
        collectionView.register(UINib.init(nibName: "CustomHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
        
        let nib = UINib(nibName: "VendorView", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "vendorcell")
        
        let nib1 = UINib(nibName: "FlashDealView", bundle: nil)
        collectionView?.register(nib1, forCellWithReuseIdentifier: "flashDeal")
        
        let nib2 = UINib(nibName: "AppBarView", bundle: nil)
        collectionView?.register(nib2, forCellWithReuseIdentifier: "appbar")
        
    }
    
    // Setup Refresh Controller
    func setupRefreshController() -> Void {
        refreshController = UIRefreshControl()
        refreshController.tintColor = UIColor.clear
        //            refreshController.attributedTitle = NSAttributedString(string: "Reloading...")
        refreshController.addTarget(self, action: #selector(reloadDashboardData), for: UIControlEvents.valueChanged)
        collectionView.addSubview(refreshController)
    }
    
    @objc private func reloadDashboardData() -> Void {
        //get category list
        initialization()
        getCategories()
    }
    
    //MARK: Network call
    //get category list from server
    private func getCategories() -> Void {
        print(self.CLASS_NAME+" -- getCategories() -- api call ")
        
        //check connectivity status
        if !ConnectivityDetector.isInternetAvailable() {
            SnackBarManager.showSnackBarForInternetSetting()
            return
        }
        
        //show progress dialog
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_BUSINESS_CATEGORY)!,
            method: .get,
            parameters:["status":"active"])
            .responseJSON { (response) -> Void in
                
                //get flash deals
                self.getFlashDeals()
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            
                            //remove previous data
                            self.categoryList.removeAll()
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let id = swiftyJson["data"][i]["id"].intValue
                                    let name = swiftyJson["data"][i]["name"].stringValue
                                    let image = swiftyJson["data"][i]["image"].string
                                    let status = swiftyJson["data"][i]["status"].intValue
                                    self.categoryList.append(BusinessCategory(id: id, name: name, image: image, status: status))
                                    print(self.CLASS_NAME+" -- getCategories()  -- category name -- "+(self.categoryList[i].name))
                                    print(self.CLASS_NAME+" -- getCategories()  -- category image -- "+(self.categoryList[i].image!))
                                }
                                
                                //stop temporarily
                                self.collectionView.reloadData()
//                                self.collectionView!.collectionViewLayout.invalidateLayout()
                            }
                            
                            //                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getCategories()  -- response -- category loaded")
                            
                        }else{
                            //                            SnackBarManager.showSnackBar(message: "Request failed")
                            print(self.CLASS_NAME+" -- getCategories()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getCategories() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- getCategories()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getCategories()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //get flash deal list from server
    private func getFlashDeals() -> Void {
        
        //check connectivity status
        if !ConnectivityDetector.isInternetAvailable() {
            SnackBarManager.showSnackBarForInternetSetting()
            return
        }
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_FLASH_DEAL)!,
            method: .get,
            parameters:["latitude":latitude, "longitude":longitude, "customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!])
            .responseJSON { (response) -> Void in
                
                //get vendor list
                self.getVendorlist()
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            //remove previuos data
                            self.flashDealList.removeAll()
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let id = swiftyJson["data"][i]["id"].stringValue
                                    let orgName = swiftyJson["data"][i]["org_name"].stringValue
                                    let dealImage = swiftyJson["data"][i]["deal_image"].string
                                    let gainSp = swiftyJson["data"][i]["gain_sp"].string
                                    
                                    self.flashDealList.append(FlashDeal(id: id, orgName: orgName, dealImage: dealImage, gainSp: gainSp))
                                    print(self.CLASS_NAME+" -- getFlashDeals()  -- flash deal name -- "+(self.flashDealList[i].orgName))
                                }
                                
                                //reload data
                                //stop temporarily
                                self.collectionView.reloadData()
//                                self.collectionView!.collectionViewLayout.invalidateLayout()
                            }
                            
                            //                            SnackBarManager.showSnackBar(message: "Flash Deals loaded")
                            print(self.CLASS_NAME+" -- getFlashDeals()  -- response -- Flash Deals loaded")
                            
                        }else{
                            //                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getFlashDeals()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getFlashDeals() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- getFlashDeals()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getFlashDeals()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //get vebdor list from server
    private func getVendorlist() -> Void {
        
        //check connectivity status
        if !ConnectivityDetector.isInternetAvailable() {
            SnackBarManager.showSnackBarForInternetSetting()
            return
        }
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_VENDOR_LIST)!,
            method: .get,
            parameters:["status":"active", "latitude":latitude, "longitude":longitude,"customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!])
            .responseJSON { (response) -> Void in
                
                //remove refresh controller
                self.refreshController.endRefreshing()
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            
                            //remove all vendor list first
                            self.vendorList.removeAll()
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let vendorId = swiftyJson["data"][i]["vendor_id"].string
                                    let orgName = swiftyJson["data"][i]["org_name"].string
                                    let profileImage = swiftyJson["data"][i]["profile_img"].string
                                    let spLeast = swiftyJson["data"][i]["sp_least"].string
                                    let distance = swiftyJson["data"][i]["distance"].string
                                    
                                    self.vendorList.append(Vendor(vendorId: vendorId, orgName: orgName, profileImage: profileImage, spLeast: spLeast, distance:distance))
                                    
                                    self.collectionView!.reloadData()
//                                    self.collectionView!.collectionViewLayout.invalidateLayout()
                                    
//                                    if let name = self.vendorList[i].orgName{
                                        print(self.CLASS_NAME+" -- getVendorlist()  -- vendor-image -- \(profileImage)")
//                                    }
                                }
                            }else{
                                //reload collection view
                                self.collectionView!.reloadData()
//                                self.collectionView!.collectionViewLayout.invalidateLayout()
                            }
                            
                            //SnackBarManager.showSnackBar(message: "Vendor list loaded")
                            print(self.CLASS_NAME+" -- getVendorlist()  -- response -- Vendor list loaded")
                            
                        }else{
                            //                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getVendorlist()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getVendorlist() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- getVendorlist()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getVendorlist()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //get flash deal list from server when searching
    private func getFlashDealsBySearch(text:String) -> Void {
        
        //check connectivity status
        if !ConnectivityDetector.isInternetAvailable() {
            SnackBarManager.showSnackBarForInternetSetting()
            return
        }
        
        //show progress dialog
        SVProgressHUD.show()    
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_FLASH_DEAL)!,
            method: .get,
            parameters:["latitude":latitude, "longitude":longitude, "search":text,"customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!])
            .responseJSON { (response) -> Void in
                
                //get vendor list
                self.getVendorlistBySearch(text: text)
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            //remove previuos data
                            self.flashDealList.removeAll()
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let id = swiftyJson["data"][i]["id"].stringValue
                                    let orgName = swiftyJson["data"][i]["org_name"].stringValue
                                    let dealImage = swiftyJson["data"][i]["deal_image"].string
                                    let gainSp = swiftyJson["data"][i]["gain_sp"].string
                                    
                                    self.flashDealList.append(FlashDeal(id: id, orgName: orgName, dealImage: dealImage, gainSp: gainSp))
                                    print(self.CLASS_NAME+" -- getFlashDealsBySearch()  -- flash deal name -- "+(self.flashDealList[i].orgName))
                                }
                                
                                //reload data
                                self.collectionView.reloadData()
//                                self.collectionView!.collectionViewLayout.invalidateLayout()
                            }
                            
                            //                            SnackBarManager.showSnackBar(message: "Flash Deals loaded")
                            print(self.CLASS_NAME+" -- getFlashDealsBySearch()  -- response -- Flash Deals loaded")
                            
                        }else{
                            //                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getFlashDealsBySearch()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getFlashDealsBySearch() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- getFlashDealsBySearch()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getFlashDealsBySearch()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //get vebdor list from server when searching
    private func getVendorlistBySearch(text:String) -> Void {
        //check connectivity status
        if !ConnectivityDetector.isInternetAvailable() {
            SnackBarManager.showSnackBarForInternetSetting()
            return
        }
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_VENDOR_LIST)!,
            method: .get,
            parameters:["status":"active", "latitude":latitude, "longitude":longitude, "search":text,"customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!])
            .responseJSON { (response) -> Void in
                
                //remove refresh controller
                self.refreshController.endRefreshing()
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            
                            //remove all vendor list first
                            self.vendorList.removeAll()
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let vendorId = swiftyJson["data"][i]["vendor_id"].string
                                    let orgName = swiftyJson["data"][i]["org_name"].string
                                    let profileImage = swiftyJson["data"][i]["profile_img"].string
                                    let spLeast = swiftyJson["data"][i]["sp_least"].string
                                    let distance = swiftyJson["data"][i]["distance"].string
                                    
                                    self.vendorList.append(Vendor(vendorId: vendorId, orgName: orgName, profileImage: profileImage, spLeast: spLeast, distance:distance))
                                    
                                    self.collectionView!.reloadData()
//                                    self.collectionView!.collectionViewLayout.invalidateLayout()
                                    
                                    if let name = self.vendorList[i].orgName{
                                        print(self.CLASS_NAME+" -- getVendorlistBySearch()  -- vendor name -- "+(name))
                                    }
                                }
                            }else{
                                //reload collection view
                                self.collectionView!.reloadData()
//                                self.collectionView!.collectionViewLayout.invalidateLayout()
                            }
                            
                            //                            SnackBarManager.showSnackBar(message: "Vendor list loaded")
                            print(self.CLASS_NAME+" -- getVendorlistBySearch()  -- response -- Vendor list loaded")
                            
                        }else{
                            //                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getVendorlistBySearch()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getVendorlistBySearch() -- swiftyJSON parsing error")
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getVendorlistBySearch()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getVendorlistBySearch()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //get vendor by category
    private func getVendorsByCategory(categoryId:Int) -> Void {
        //check connectivity status
        if !ConnectivityDetector.isInternetAvailable() {
            SnackBarManager.showSnackBarForInternetSetting()
            return
        }
        //show progress dialog
        SVProgressHUD.show()
        
        print(CLASS_NAME+" -- getVendorsByCategory() -- url - "+ApiEndpoints.URI_VENDORS_BY_CATEGORY+"/"+String(categoryId)+ApiEndpoints.URI_VENDORS)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_VENDORS_BY_CATEGORY+"/"+String(categoryId)+ApiEndpoints.URI_VENDORS)!,
            method: .get,
            parameters:["platform":"mobile", "status":"active", "latitude":self.latitude, "longitude":self.longitude,"customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!])
            .responseJSON { (response) -> Void in
                
                //hide progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            //remove all vendor list first
                            self.vendorList.removeAll()
                            
                            print(self.CLASS_NAME+" -- getVendorsByCategory()  -- response -- "+(String(describing: response)))
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let vendorId = swiftyJson["data"][i]["vendor_id"].string
                                    let orgName = swiftyJson["data"][i]["org_name"].string
                                    let profileImage = swiftyJson["data"][i]["profile_img"].string
                                    let spLeast = swiftyJson["data"][i]["sp_least"].string
                                    let distance = swiftyJson["data"][i]["distance"].string
                                    
                                    self.vendorList.append(Vendor(vendorId: vendorId, orgName: orgName, profileImage: profileImage, spLeast: spLeast, distance:distance))
                                    
                                    self.collectionView!.reloadData()
                                    //                                    self.collectionView!.collectionViewLayout.invalidateLayout()
                                    
                                    //                                    if let name = self.vendorList[i].orgName{
                                    print(self.CLASS_NAME+" -- getVendorlist()  -- vendor-image -- \(profileImage)")
                                    //                                    }
                                }
                            }else{
                                //reload collection view
                                self.collectionView!.reloadData()
//                                self.collectionView!.collectionViewLayout.invalidateLayout()
                            }
                            
                            //                            SnackBarManager.showSnackBar(message: "Vendor list loaded")
                            print(self.CLASS_NAME+" -- getVendorsByCategory()  -- response -- "+swiftyJson["message"].stringValue)
                            
                        }else{
                            //                            SnackBarManager.showSnackBar(message: "Request failed")
                            print(self.CLASS_NAME+" -- getVendorsByCategory()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getVendorsByCategory() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- getVendorsByCategory()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getVendorsByCategory()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        print("Called from return headersize")
        
        return CGSize(width:collectionView.frame.size.width, height:260.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    } 
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return vendorList.count + 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print(CLASS_NAME+" -- collectionView() -- cellForItemAt")
        
        if indexPath.row == 0 {
            let t: FlashDealCollectionViewCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "flashDeal", for: indexPath) as! FlashDealCollectionViewCell
//            t.prepareForReuse()
            t.delegate = self;
            t.flashDealList = flashDealList
            t.lblFlashDealItemCount!.text = String(flashDealList.count)
//            t.cvFlashDeal.reloadData()
            
            return t;
            
        }else{
            let cell: VendorCollectionViewCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "vendorcell", for: indexPath) as! VendorCollectionViewCell
            
            print(CLASS_NAME+" -- collectionView() -- cellForItemAt -- image-\(indexPath.row-1): \(cell)")
            
//            cell.bigImage!.image = nil
            cell.prepareForReuse()
//            cell.tag = indexPath.row-1
            cell.layer.cornerRadius = 10
            cell.layer.borderWidth = 2
            cell.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: 0xF2F2F2, alpha: 1).cgColor
//            cell.layer.shadowColor = UIColor.gray.cgColor
//            cell.layer.shadowOpacity = 1
//            cell.layer.shadowOffset = CGSize(width:0.0, height: 0.0)
            
            cell.bigImage.frame = CGRect(x: 0, y: 0, width: (collectionView.frame.size.width/2)-15, height: (collectionView.frame.size.width/2)-15)
            
            //            cell.title.text = String.excerpt(value: vendorList[indexPath.row-1].orgName!, length: 15)
            cell.title.text = vendorList[indexPath.row-1].orgName!
            
            //cell.lblSP!.text = vendorList[indexPath.row-1].spLeast!
            
            //show least sp
            if let spLeast = vendorList[indexPath.row-1].spLeast {
                cell.lblSP.text = spLeast 
            }
            
            //
            if let distance = vendorList[indexPath.row-1].distance {
                cell.subTitle.text =  distance
            }else{
                cell.subTitle.text =  "Not found"
            }
            
            print(CLASS_NAME+" -- vendorImage-test -- image-\(indexPath.row-1): \(vendorList[indexPath.row-1].profileImage)")
            
            if let image = vendorList[indexPath.row-1].profileImage {
//                DispatchQueue.main.async {
//                if cell.tag == indexPath.row-1 {
//                print(CLASS_NAME+" -- collectionView() -- cellForItemAt -- image-\(indexPath.row-1): \(image)")
                print(CLASS_NAME+" -- vendorImage-pre -- image-\(indexPath.row-1): \(image)")
            
//                SDImageCache.shared().clearMemory()
//                SDImageCache.shared().clearDisk(onCompletion: nil)
                cell.bigImage!.plainView.reloadInputViews()
//                cell.bigImage!.image = imagelist[indexPath.row-1]
            print(CLASS_NAME+" -- vendorImage-post -- image-\(indexPath.row-1): \(image)")
//                cell.bigImage!.sd_cancelCurrentAnimationImagesLoad()
            
                cell.bigImage!.sd_setImage(with: URL(string: ApiManager.SERVER_BASE_URL+image)!, placeholderImage: UIImage(named: "default-vendor-profile"))
            
//                cell.bigImage!.kf.setImage(with: URL(string: ApiManager.SERVER_BASE_URL+image)!)
//                    cell.bigImage!.image = UIImage(data: NSData(contentsOf: (NSURL(string: ApiManager.SERVER_BASE_URL+image) as? URL)!) as! Data)
//                }
                
//                }
            
            }
            
            
            return cell;
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(CLASS_NAME+" -- collectionView() -- didSelectItemAt")
        
        if indexPath.row > 0 {
            let vendorDeatilStoryboard = UIStoryboard.init(name: "vendordetails", bundle: nil)
            let vc = vendorDeatilStoryboard.instantiateViewController(withIdentifier: "vendordetailvc") as! VendorDetailTableViewController
            vc.vendorId = vendorList[indexPath.row-1].vendorId
            vc.spChangedDelegate = self.spChangedDelegate
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //set current indexpath
        currentIndexPath = indexPath
        
        if (indexPath.row == 0) {
//            if flashDealList.count == 0{
//                return CGSize(width: collectionView.frame.size.width, height: 0)
//            }
            return CGSize(width: collectionView.frame.size.width, height: 190);
        }
        
        if indexPath.row > 0 {
            if vendorList.count > 0 {
                let estimatedFrame = NSString(string:vendorList[indexPath.row-1].orgName!).boundingRect(with: CGSize(width:(collectionView.frame.size.width/2)-15, height: 1000), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 20.0)!], context: nil)
                
                return CGSize(width: (collectionView.frame.size.width/2)-15 , height: estimatedFrame.height + 205)
            }
        }
        
        return CGSize(width: (collectionView.frame.size.width/2)-15 , height: 265);
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//
//        if currentIndexPath.row > 0{
//            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//        }else{
//            return UIEdgeInsetsMake(0, 0, 0, 0)
//        }
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        //set current indexpath
        currentIndexPath = indexPath
        
        if (indexPath.row == 0) {
//            if flashDealList.count == 0{
//                return 0
//            }
            return 190
        }
        
        if indexPath.row > 0 {
            if vendorList.count > 0 {
                let estimatedFrame = NSString(string:vendorList[indexPath.row-1].orgName!).boundingRect(with: CGSize(width:(collectionView.frame.size.width/2)-15, height: 1000), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 17.0)!], context: nil)
                
                return estimatedFrame.height + 205
            }
        }
        
        return 265
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        //for keeping border on category image
        if !isCategorySearched {
            headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath as IndexPath) as! HeaderViewCollectionReusableView
            
//            headerView.frame = CGRect(x: 0, y: 0, width: headerView.frame.width, height: headerView.frame.height)
            
            headerView.isLoadedFirstTime = isLoadedFirstTime
            isLoadedFirstTime = false
            
            headerView.delegate = self
            headerView.searchdelegate = self
            headerView.tfDelegate = self
            
//            headerView.frame.size.height = 260
            
            //set location on topbar
            if let location = currentLocation {
                headerView.lblCurrentLocation.text = location
            }
            
            headerView.categoryList = categoryList
            headerView.cvBusinessCategory.reloadData()
        }
        return headerView
    }
    
    
    //get address from lat long
    // Using closure
    func getAddress() -> Void
    {
        //        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: self.latitude, longitude: self.longitude)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    
                    var addressString : String = ""
                    
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    
                    if pm.country != nil {
                        addressString = addressString + pm.country! + " "
                    }
//                    if pm.postalCode != nil {
//                        addressString = addressString + pm.postalCode! + " "
//                    }
                    
                    
                    print(addressString)
                    print(self.CLASS_NAME+" -- getAddress() -- sublocality-- \(pm.subLocality)")
                    
                    if addressString != "" {
                        self.currentLocation = addressString
                    }else{
                        self.currentLocation = "Doha, Qatar"
                    }
                    self.collectionView.reloadData()
                }
        })
    }
    
    //for safe area
    override var additionalSafeAreaInsets: UIEdgeInsets {
        set {
            super.additionalSafeAreaInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        }
        
        get {
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
}


extension DashboardHomeViewController : FlashDealCollectionViewCellDelegate, CategoryItemDelegate, SearchDelegate, CLLocationManagerDelegate {
    
    // FlashDealCollectionViewCellDelegate
    func onFlashDealItemClicked(position:Int) {
        let srt = UIStoryboard.init(name: "vendordetails", bundle: nil)
        let vc = srt.instantiateViewController(withIdentifier: "flashdealdetailvc") as! FlashDealDeatilViewController
        vc.flashDealId = flashDealList[position].id
        vc.spChangedDelegate = self.spChangedDelegate
        vc.flashDealDetailImageUrl = flashDealList[position].dealImage
       
//        self.navigationController?.pushViewController(vc, animated: true)

      self.present(vc, animated: true, completion: nil)

    }
    
    // CategoryItemDelegate
    func onCategoryItemSelected(itemPosition: Int?){
        print(self.CLASS_NAME+"-- CategoryItemDelegate -- onCategoryItemSelected() -- category selected -- \(itemPosition)")
        
        //reset collection view content offset
        collectionView.contentOffset = CGPoint(x: 0, y: 0)
        
        //for keeping border on category image
        isCategorySearched = true
        
        if let position = itemPosition{
            if position < 1 {
                //get category list
                getCategories()
            }else{
                getVendorsByCategory(categoryId: self.categoryList[position-1].id)
//                getVendorlist()
            }
        }else{
            print(self.CLASS_NAME+"-- CategoryItemDelegate -- onCategoryItemSelected() -- posiiton is nil")
        }
    }
    
    // SearchDelegate -> onClickSearchButton()
    func onClickSearchButton(text: String) {
        print(CLASS_NAME+" -- onClickSearchButton() -- \(text)")
        
        getFlashDealsBySearch(text: text) 
        
//        if text != "" {
//            getFlashDealsBySearch(text: text)
//        }else{
//            print(CLASS_NAME+" -- onClickSearchButton() -- no text entered....")
//        }
    }
    
    func searchFieldShouldReturn(searchField: UITextField, serachQuery:String) {
        print(CLASS_NAME+" -- searchFieldShouldReturn() -- \(serachQuery)")
        if serachQuery.characters.count > 0 {
            getFlashDealsBySearch(text: serachQuery)
        }else{
            print(CLASS_NAME+" -- searchFieldShouldReturn() -- no text entered....")
        }
        
    }
    
    //CLLocationManagerDelegate -> OnLocationChange
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(CLASS_NAME+" -- locationManager() -- didChangeAuthorization -- \(status)")
    }
    
    // 2
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(CLASS_NAME+" -- locationManager() -- didFailWithError -- \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations.last!
        print(CLASS_NAME+" -- locationManager() -- didUpdateLocations -- \(currentLocation.coordinate.latitude) ")
        self.latitude = currentLocation.coordinate.latitude
        self.longitude = currentLocation.coordinate.longitude
        
        //get categories
        self.getCategories()
        
        if ConnectivityDetector.isInternetAvailable() {
            self.getAddress()
        }else{
            self.currentLocation = "Doha, Qatar"
            self.collectionView.reloadData()
        }
        
        //stop updating location
        self.locationManager.stopUpdatingLocation()
    }
    
    
    //MARK: Keyboard
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }
    
    // Start Editing The Text Field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -250, up: true)
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -250, up: false)
    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        self.view.endEditing(true)
        return true
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
}


