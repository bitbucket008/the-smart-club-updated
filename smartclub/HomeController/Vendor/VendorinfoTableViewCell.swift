//
//  VendorinfoTableViewCell.swift
//  smartclub
//
//  Created by Admin on 30/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class VendorinfoTableViewCell: UITableViewCell {
    //MARK: properties
    open let CLASS_NAME = "VendorinfoTableViewCell"
    open var vendorProfile:VendorProfile!
    
    //MARK: Outlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblAdditionalPhoneNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAddressOne: UILabel!
    @IBOutlet weak var lblAddressTwo: UILabel!
    @IBOutlet weak var lblWebUrl: UILabel!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var ivCoverImage: UIImageView!
    @IBOutlet weak var lblWorkingHour: UIButton!
    @IBOutlet weak var btnLinkWeb: UIImageView!
    @IBOutlet weak var btnLinkSnapChat: UIButton!
    @IBOutlet weak var btnLinkInstagram: UIButton!
    @IBOutlet weak var btnLinkTwitter: UIButton!
    @IBOutlet weak var btnLinkFacebook: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //initialization
        initialization()
        
    }
    
    //MARK: UI_Method
    //initialization
    private func initialization() -> Void {
        //background view
        bgView.clipsToBounds = true
        bgView.layer.cornerRadius = 20
        bgView.layer.shadowColor = UIColor.gray.cgColor
        bgView.layer.shadowOpacity = 1
        bgView.layer.shadowOffset = CGSize(width:0.0, height: 0.0)
        
        //when click on address one
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onCLickAddress))
        lblAddressOne.addGestureRecognizer(tapGesture)
        
        //when click on the phone number
        let tapGestureForPhoneNumber = UITapGestureRecognizer(target: self, action: #selector(onClickPhoneNumber))
        lblPhoneNumber.addGestureRecognizer(tapGestureForPhoneNumber)
        
        //when click on social link - facebbok
        let tapGestureForSocialLinkFacebook = UITapGestureRecognizer(target: self, action: #selector(onClickSocialLink))
        btnLinkFacebook.addGestureRecognizer(tapGestureForSocialLinkFacebook)
        
        //when click on social link - Twitter
        let tapGestureForSocialLinkTwitter = UITapGestureRecognizer(target: self, action: #selector(onClickSocialLink))
        btnLinkTwitter.addGestureRecognizer(tapGestureForSocialLinkTwitter)
        
        //when click on social link - Instagram
        let tapGestureForSocialLinkInstagram = UITapGestureRecognizer(target: self, action: #selector(onClickSocialLink))
        btnLinkInstagram.addGestureRecognizer(tapGestureForSocialLinkInstagram)
        
        //when click on social link - Snapchat
        let tapGestureForSocialLinkSnapchat = UITapGestureRecognizer(target: self, action: #selector(onClickSocialLink))
        btnLinkSnapChat.addGestureRecognizer(tapGestureForSocialLinkSnapchat)
        
        //when click on social link - Website
        let tapGestureForSocialLinkWeb = UITapGestureRecognizer(target: self, action: #selector(onClickSocialLink))
        btnLinkWeb.addGestureRecognizer(tapGestureForSocialLinkWeb)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //when click on the phone number
     @objc func onClickPhoneNumber(sender:UITapGestureRecognizer) -> Void  {
        
        print(CLASS_NAME+" -- onClickCallUsBitton()")

        let number = vendorProfile.phone!
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //when click on social links
    @objc func onClickSocialLink(sender:UITapGestureRecognizer) -> Void  {
        print(CLASS_NAME+" -- onClickSocialLink() -- view-tag : \(sender.view?.tag)")
        
        var url:String?
        
        if sender.view?.tag == 101 {
            url = vendorProfile.linkFaceebok
        }else if sender.view?.tag == 102 {
            url = vendorProfile.linkTwitter
        }else if sender.view?.tag == 103 {
            url = vendorProfile.linkInstagram
        }else if sender.view?.tag == 104 {
            url = vendorProfile.linkSnapchat
        }else if sender.view?.tag == 105 {
            url = vendorProfile.linkWeb
        }
        
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: url!)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: url!)!)
        }
    }
    
    
    //when click on address one
    @objc func onCLickAddress(sender:UITapGestureRecognizer) -> Void {
        print(CLASS_NAME+" -- onCLickAddress() -- longitude : \(vendorProfile.longitude!)")
        
        guard let latitude = vendorProfile.latitude , let longitude = vendorProfile.longitude else{
            print(CLASS_NAME+" -- onCLickAddress() -- No latitude and longitude found !!  !!");
            return
        }
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            if let mapLink = vendorProfile.mapLink{
                if !mapLink.elementsEqual(""){
                    UIApplication.shared.openURL(NSURL(string:mapLink)! as URL)
                }else{
                    print(CLASS_NAME+" -- onCLickAddress() -- map link is empty !!");
                }
            }else{
                print(CLASS_NAME+" -- onCLickAddress() --  map link not found !!");
            }
//            UIApplication.shared.openURL(NSURL(string:
//                "comgooglemaps://?saddr=&daddr=\(vendorProfile.latitude!),\(vendorProfile.longitude!)&directionsmode=walking")! as URL)
            
        } else {
            print(CLASS_NAME+" -- onCLickAddress() -- Google map not found !!");
            SnackBarManager.showSnackBar(message: "Google map not found")
        }
    }

}
