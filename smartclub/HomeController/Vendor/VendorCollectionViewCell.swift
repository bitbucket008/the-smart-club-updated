//
//  VendorCollectionViewCell.swift
//  smartclub
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class VendorCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bigImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var lblSP: UILabel!
    
    override func awakeFromNib() {
        title.numberOfLines = 0
        title.sizeToFit()
        
        subTitle.numberOfLines = 0
        subTitle.sizeToFit()
        
//        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(0, 10, 0, 10))
//        self.contentView.translatesAutoresizingMaskIntoConstraints = true
        
    }
    
    override func prepareForReuse() {
        // still visible on screen (window's view hierarchy)
//        if self.window != nil { return }
        
//        bigImage!.image = nil
    }
    
}
