//
//  RedeemedSuccessViewController.swift
//  smartclub
//
//  Created by Admin on 05/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

 class RedeemedSuccessViewController: UIViewController {
    //MARK: Properties
    open var vendorCode:String = ""
    
    //MARK: Outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnVendorCode: UIBorderButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // init ui
        initUI()
        
    }

    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       // let degrees:Float = 20; //the value in degrees
//        topView.transform = CGAffineTransform(rotationAngle: CGFloat(20 * M_PI/180));
        
        //go home screen after few seconds
//        BackgroundThread.background(delay: 3, background: nil, completion: {
//            let storyBoard = UIStoryboard.init(name: "dashboard", bundle: nil)
//            let vc = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//            vc.isLoginFirstTime = false
//            self.present(vc, animated: true, completion: nil)
//        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: UI Method
    //init ui
    private func initUI() -> Void{
        btnVendorCode!.setTitle(vendorCode, for: .normal)
    }
    
    //MARK: Action
    @IBAction func onClickGestureRecognizer(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
