//
//  vendordealTableViewCell.swift
//  smartclub
//
//  Created by Admin on 30/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

//MARK: Delegates 
protocol VendordealTableViewCellDelegate :NSObjectProtocol {
    func  onClickRedeemButton(dealPosition:Int?)
    func onClickDownArrow(indexPath:IndexPath, cell:VendordealTableViewCell,  isExanded:Bool)
}


class VendordealTableViewCell: UITableViewCell {

    //MARK: Porperties
    fileprivate let CLASS_NAME:String = "VendordealTableViewCell"
    open var dealPosition:Int?
    weak var delegate:VendordealTableViewCellDelegate!
    open var vendorDescription:String!
    open var indexPath:IndexPath!
    open var cell:VendordealTableViewCell!
    open var isExpanded:Bool = false
    
    //MARK: Outlets
    @IBOutlet weak var bdView: UIView!
    @IBOutlet weak var lblVendorDealTitle: UILabel!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblVendorDealDescription:UILabel!
    @IBOutlet weak var lblSP: UILabel!
    @IBOutlet weak var spContainer: UIBorderView!
    @IBOutlet weak var ivDealType: UIImageView!
    
    @IBOutlet weak var uivArrowContainer: UIView!
    
    @IBOutlet weak var ivDownArrow: UIButton!
    @IBOutlet weak var uivDealTypeImageContainer: UIView!
    @IBOutlet weak var descriptionHeightContsaint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ivDealType.layer.cornerRadius = ivDealType.frame.height/2
        uivDealTypeImageContainer.layer.cornerRadius = uivDealTypeImageContainer.frame.height/2
        uivDealTypeImageContainer.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1).cgColor
        uivDealTypeImageContainer.layer.borderWidth = 2
        
        spContainer.layer.cornerRadius = 15
        bdView.layer.cornerRadius = 20
         bdView.layer.shadowColor = UIColor.gray.cgColor
        bdView.layer.shadowOpacity = 0.5
        bdView.layer.shadowOffset = CGSize(width:0.0, height: 0.0)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickDownArrow))
        uivArrowContainer.addGestureRecognizer(tapGesture)
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = true 
        
    }
    
    
//    @IBAction func AddtoCartBtnAction(_ sender: Any) {
//
//
//        let  dashboard = UIStoryboard.init(name: "dashboard", bundle: nil)
//        let vc = dashboard.instantiateViewController(withIdentifier: "congrasspop") as! CongrassViewController
//
//        self.present(vc, animated: false, completion: nil)
//
//    }
    

    //MARK: Actions
    @IBAction func addToCartBtnAction(_ sender: Any) {
        
        guard let delegate = delegate else {
            return
        }
        delegate.onClickRedeemButton(dealPosition:dealPosition)
    }
    
    //MARK: Actions
    //when click down arrow
//    @IBAction func onClickDownArrowq(_ sender: UITapGestureRecognizer) {
//       print(CLASS_NAME+" -- onClickDownArrow() ")
//        delegate.onClickDownArrow(indexPath: indexPath,  cell:cell, isExanded: isExpanded)
//    }
    
    @objc func onClickDownArrow(_ sender: UITapGestureRecognizer) {
        print(CLASS_NAME+" -- onClickDownArrow() ")
        delegate.onClickDownArrow(indexPath: indexPath,  cell:cell, isExanded: isExpanded)
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
