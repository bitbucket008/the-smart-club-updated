//
//  VendorDetailTableViewController.swift
//  smartclub
//
//  Created by Admin on 30/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SVProgressHUD

class VendorDetailTableViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, GotoCartPopupViewControllerDelegate{

    //MARK: Property
    fileprivate let CLASS_NAME:String = "VendorDetailTableViewController"
    fileprivate let localStorage = LocalStorage()
//    fileprivate var userId:String = "1cac87bdaa504a003a920c4e9fa027ae"
    open var vendorId:String?
   
    //open var customerType:String?
    fileprivate var vendorDealList:[VendorDeal] = [VendorDeal]()
    fileprivate var vendorProfile:VendorProfile!
    open var spChangedDelegate:SmartPointChangeDelegate?
//    fileprivate var isArrowPressed = false
    
    //MARK: Outlets
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var lblVendorName: UILabel!
    
    //MARK: UI Elements
    fileprivate var refreshController:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //init ui
        initUI()
        
        //get vendor deal list from server
        getVendorDealList()
       print("The value is -" , localStorage.getUser().customerType!)
    }
    
    //MARK: UI Method
    //initialize ui
    private func initUI() -> Void{
        self.tableView!.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.register(UINib(nibName: "VendorDetailCell", bundle: nil), forCellReuseIdentifier: "vendordealcell")
        tableView.register(UINib(nibName: "VendorDetailInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "vendorinfocell")

        //        tableView.estimatedRowHeight = 200
//        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Setup Refresh Controller
        setupRefreshController()
    }
    
    // Setup Refresh Controller
    private func setupRefreshController() -> Void { 
        refreshController = UIRefreshControl()
        refreshController.tintColor = UIColor.clear
//        refreshController.attributedTitle = NSAttributedString(string: "Reloading...")
        refreshController.addTarget(self, action: #selector(reloadVendorData), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshController)
    }
    
    @objc private func reloadVendorData() -> Void {
        print(CLASS_NAME+" -- reloadVendorData()")
        //get vendor deal list from server
        getVendorDealList()
    }
    
    
    func onClickGotoCart() {
        let stboard = UIStoryboard.init(name: "mycart", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "mycartvc") as! MyCartTableViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK: Network calls
    //get vebdor deal list from server
    private func getVendorDealList() -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_VENDOR_DETAILS+"/"+vendorId!+ApiEndpoints.URI_DEALS)!,
            method: .get,
            parameters:["platform":"mobile", "status":"create", "approve":"true", "flash":"false","customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!]) //"customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!
            .responseJSON { (response) -> Void in
                
                //get profile information
                self.getVendorProfileInformation()
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            
                            if(swiftyJson["data"].arrayValue.count > 0){
                                for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                    let id = swiftyJson["data"][i]["id"].stringValue
                                    let name = swiftyJson["data"][i]["name"].stringValue
                                    let orgName = swiftyJson["data"][i]["org_name"].string
                                    let vendorDealDescription = Optional( swiftyJson["data"][i]["description"].stringValue);                                    let gainSP = swiftyJson["data"][i]["gain_sp"].intValue
                                    let identityCode = swiftyJson["data"][i]["identity_code"].stringValue
                                    let dealType = swiftyJson["data"][i]["deal_type"].string
                                    
                                    
                                    //append to the list
                                    let vendorDeal = VendorDeal(id:id, name:name, vendorDealDescription:vendorDealDescription, orgName:orgName, gainSP:gainSP, identityCode:identityCode, dealType:dealType)
                                    vendorDeal.isExpanded = Optional(false)
                                    vendorDeal.isDownArrowPressed = Optional(false)
                                    self.vendorDealList.append(vendorDeal)
                                }
                                
                                //reload tableview data
                                self.tableView!.reloadData()
                            }
                            
//                            SnackBarManager.showSnackBar(message: "Vendor list loaded")
                            print(self.CLASS_NAME+" -- getVendorDealList()  -- response -- Vendor deal list loaded")
                            
                        }else{
//                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getVendorDealList()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getVendorDealList() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- getVendorDealList()  -- response -- request failed")
//                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getVendorDealList()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //get vebdor deal list from server
    private func getVendorProfileInformation() -> Void {
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_VENDOR_PROFILE+"/"+vendorId!)!,
            method: .get,
            parameters:["platform":"mobile","customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!])
            .responseJSON { (response) -> Void in
                
                //end refreshing
                self.refreshController.endRefreshing()
                
                //hide progressview 
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            
                            let additionalPhone = swiftyJson["data"]["additional_phone"].string
                            let email = swiftyJson["data"]["email"].string
                            let orgName = swiftyJson["data"]["org_name"].string
                            let phone = swiftyJson["data"]["phone"].string
                            let websiteUrl = swiftyJson["data"]["website_url"].string
                            let coverImage = swiftyJson["data"]["cover_image"].string
                            let businessHour = swiftyJson["data"]["business_hour"].string
//                            let address = swiftyJson["data"]["address"]["street"].stringValue + "," + swiftyJson["data"]["address"]["suburb"].stringValue + "," + swiftyJson["data"]["address"]["state"].stringValue + "," + swiftyJson["data"]["address"]["country"].stringValue
                            
//                            let address = swiftyJson["data"]["address"]["formattedAddress"].stringValue
                            let address = swiftyJson["data"]["address"]["area"].stringValue
                            let mapLink = swiftyJson["data"]["address"]["link"].string
                            
                            let latitude = swiftyJson["data"]["address"]["latitude"].stringValue
                            let longitude = swiftyJson["data"]["address"]["longitude"].stringValue
                            
                            let facebookLink = swiftyJson["data"]["social_url"]["facebook"].string
                            let instagramLink = swiftyJson["data"]["social_url"]["instagram"].string
                            let snapchatLink = swiftyJson["data"]["social_url"]["snapchat"].string
                            let twitterLink = swiftyJson["data"]["social_url"]["twitter"].string
                            
                            //populate ui
                            self.vendorProfile = VendorProfile(additionalPhone:additionalPhone, email:email, orgName:orgName, phone:phone, websiteUrl:websiteUrl, coverImage:coverImage, businessHour:businessHour)
                            self.vendorProfile.address = Optional(address)
                            self.vendorProfile.latitude = Optional(latitude)
                            self.vendorProfile.longitude = Optional(longitude)
                            self.vendorProfile.linkFaceebok = facebookLink
                            self.vendorProfile.linkInstagram = instagramLink
                            self.vendorProfile.linkSnapchat = snapchatLink
                            self.vendorProfile.linkTwitter = twitterLink
                            self.vendorProfile.mapLink = mapLink
                            self.vendorProfile.linkWeb = websiteUrl
                            
                            self.tableView!.reloadData()
                            //                            SnackBarManager.showSnackBar(message: "Vendor list loaded")
                            print(self.CLASS_NAME+" -- getVendorProfileInformation()  -- response -- Vendor deal list loaded")
                            
                        }else{
                            //                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            print(self.CLASS_NAME+" -- getVendorProfileInformation()  -- response -- "+swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- getVendorProfileInformation() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- getVendorProfileInformation()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getVendorProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //post redeem now offer to server
    private func redeemNowDeal(dealPosition:Int) -> Void {
        //show progress dialog
        SVProgressHUD.show()

        Alamofire.request(
            URL(string: ApiEndpoints.URI_VENDOR_DEAL_REDEEM)!,
            method: .post,
            parameters:["customer_id": localStorage.getUser().id!, "vendor_id": vendorId!, "deal_id": vendorDealList[dealPosition].id, "vendor_code": vendorDealList[dealPosition].identityCode, "redeem":true], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in

                //dismiss progress dialog
                SVProgressHUD.dismiss()

                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        //                        self.offerList.remove(at: offerPosition)
                        //reload tableview data
                        //                        self.cvOwnedOffer.reloadData()
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        let redeemtionCode = swiftyJson["data"]["redemption_code"].stringValue
                        
                        //change user sp
//                        let user = self.localStorage.getUser()
//                        user.earnedSp = Optional(self.localStorage.getUser().earnedSp! + self.vendorDealList[dealPosition].gainSP)
//                        self.localStorage.storeUser(user: user)
//                        self.spChangedDelegate?.onSmartPointChanged(newSP: self.localStorage.getUser().earnedSp)
                        
                        self.spChangedDelegate?.getSmartPointFromServer()
                        
                        print(self.CLASS_NAME+" -- redeemNowDeal()  -- response -- You redeemed a deal")
                        
                        //show vendor code
                        self.showRedeemSuccessDialog(redeemtionCode: redeemtionCode)
                        
                    }else{
                        SnackBarManager.showSnackBar(message: "Error from server")
                        print(self.CLASS_NAME+" -- redeemNowDeal()  -- response -- Error from server")
                    }
                }else{
                    print(self.CLASS_NAME+" -- redeemNowDeal()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }

                print(self.CLASS_NAME+" -- redeemNowDeal()  -- response -- "+(String(describing: response)))

        }
    }
    
    
    //show vendor code dialog
    func showRedeemSuccessDialog(redeemtionCode: String) {
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "RedeemedSuccessViewController") as! RedeemedSuccessViewController
        vc.vendorCode = redeemtionCode
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return vendorDealList.count+1;
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell:VendorinfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "vendorinfocell", for: indexPath) as! VendorinfoTableViewCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.vendorProfile = vendorProfile
            
            //binding data
            lblVendorName.text = self.vendorProfile?.orgName
            cell.lblWorkingHour.setTitle(vendorProfile?.businessHour, for: .normal)
            
            if let image = self.vendorProfile?.coverImage {
                cell.ivCoverImage!.sd_setImage(with: URL(string: ApiManager.SERVER_BASE_URL+image)!, placeholderImage: UIImage(named: "default-vendor-cover"))
            }
            
            if let value = self.vendorProfile?.orgName{
                if value != "" || !value.isEmpty {
                    cell.lblOrgName.text = value
                }else{
                    cell.lblOrgName.text = "No organization name"
                }
            }else{
                cell.lblOrgName.text = "No organization name"
            }
            
            if let value = self.vendorProfile?.email{
                if value != "" || !value.isEmpty {
                    cell.lblEmail.text = value
                }else{
                    cell.lblEmail.text = "No email address"
                }
            }else{
                cell.lblEmail.text = "No email address"
            }
            
            //for phone
            if let value = self.vendorProfile?.phone{
                if value != "" || !value.isEmpty {
                    cell.lblPhoneNumber.text = value
                }else{
                    cell.lblPhoneNumber.text = "No phone number available"
                }
            }else{
                cell.lblPhoneNumber.text = "No phone number available"
            }
            
            //for additional phone
            if let value = self.vendorProfile?.additionalPhone{
                if value != "" || !value.isEmpty {
                    cell.lblAdditionalPhoneNumber.text = value
                }else{
                    cell.lblAdditionalPhoneNumber.text = "No additional phone"
                }
            }else{
                cell.lblAdditionalPhoneNumber.text = "No additional phone"
            }
            
            if let value = self.vendorProfile?.address{
                if value != "" || !value.isEmpty {
                    cell.lblAddressOne.text = value
                }else{
                    cell.lblAddressOne.text = "No address provided"
                }
            }else{
                cell.lblAddressOne.text = "No address provided"
            }
            
            if let value =  self.vendorProfile?.websiteUrl{
                if value != "" || !value.isEmpty {
                    cell.btnLinkWeb.isHidden = false
                }else{
                    cell.btnLinkWeb.isHidden = true
                }
            }else{
                cell.btnLinkWeb.isHidden = true
            }
            
            if let value =  self.vendorProfile?.linkFaceebok{
                if value != "" || !value.isEmpty {
                    cell.btnLinkFacebook.isHidden = false
                }else{
                    cell.btnLinkFacebook.isHidden = true
                }
            }else{
                cell.btnLinkFacebook.isHidden = true
            }
            
            if let value =  self.vendorProfile?.linkInstagram{
                if value != "" || !value.isEmpty {
                    cell.btnLinkInstagram.isHidden = false
                }else{
                    cell.btnLinkInstagram.isHidden = true
                }
            }else{
                cell.btnLinkInstagram.isHidden = true
            }
            
            if let value =  self.vendorProfile?.linkSnapchat{
                if value != "" || !value.isEmpty {
                    cell.btnLinkSnapChat.isHidden = false
                }else{
                    cell.btnLinkSnapChat.isHidden = true
                }
            }else{
                cell.btnLinkSnapChat.isHidden = true
            }
            
            if let value =  self.vendorProfile?.linkTwitter{
                if value != "" || !value.isEmpty {
                    cell.btnLinkTwitter.isHidden = false
                }else{
                    cell.btnLinkTwitter.isHidden = true
                }
            }else{
                cell.btnLinkTwitter.isHidden = true
            }
            
             return cell
        } else {
            let cell:VendordealTableViewCell = tableView.dequeueReusableCell(withIdentifier: "vendordealcell", for: indexPath) as! VendordealTableViewCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.delegate = self
            cell.dealPosition = Optional(indexPath.row-1)
            cell.indexPath = indexPath
            cell.cell = cell
            
            //bind data
            cell.lblVendorName.text = vendorDealList[indexPath.row-1].orgName
            cell.lblVendorDealTitle.text = vendorDealList[indexPath.row-1].name
            cell.lblVendorDealDescription.text = vendorDealList[indexPath.row-1].vendorDealDescription
            cell.lblSP.text = String(vendorDealList[indexPath.row-1].gainSP)
            
            if let dealType = vendorDealList[indexPath.row-1].dealType{
                if dealType.elementsEqual("prepaid"){
                    cell.ivDealType.image = UIImage(named: "prepaid")
                }else if dealType.elementsEqual("bogo"){
                    cell.ivDealType.image = UIImage(named: "bogo")
                }else if dealType.elementsEqual("discount"){
                    cell.ivDealType.image = UIImage(named: "discount")
                }
            }
            
             return cell
       }
        // Configure the cell...

       
    }
    
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }

    //for row height
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // if the cell is profile ifo cell
        if indexPath.row == 0 {
            return 350
        }
        
        let estimatedFrameForTitle = NSString(string:vendorDealList[indexPath.row-1].name).boundingRect(with: CGSize(width:(tableView.frame.size.width), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 15.0)!], context: nil)
        
        let estimatedFrameForOrg = NSString(string:vendorDealList[indexPath.row-1].orgName!).boundingRect(with: CGSize(width:(tableView.frame.size.width), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 13.0)!], context: nil)
        
        let estimatedFrameForDescription =  NSString(string:vendorDealList[indexPath.row-1].vendorDealDescription!).boundingRect(with: CGSize(width:(tableView.frame.size.width), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 15.0)!], context: nil)
        
        //if the cell is deal list cell
        if indexPath.row > 0 {
            if vendorDealList[indexPath.row - 1].isExpanded! {
//                return estimatedFrameForTitle.height + estimatedFrameForOrg.height + estimatedFrameForDescription.height + 150 + 20
                return estimatedFrameForTitle.height + estimatedFrameForOrg.height + estimatedFrameForDescription.height + 100 + 30
            }else{
//                return estimatedFrameForTitle.height + estimatedFrameForOrg.height + 150 + 20
                return estimatedFrameForTitle.height + estimatedFrameForOrg.height + estimatedFrameForDescription.height + 100 + 30
            }
//            return estimatedFrameForTitle.height + 170 + 15
        }
    return 200;
        
    }
    
    //for selecting row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(CLASS_NAME+" -- tableView() -- didSelectRowAt")
    }
    
    
    //MARK: Actions
    @IBAction func backBtnAction(sebder:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


//MARK: Delegates implementation
extension VendorDetailTableViewController : VendordealTableViewCellDelegate, VendorCodeDelegate {
    
    //when click on reededm button
    func onClickRedeemButton(dealPosition: Int?) {
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "vendorcodevc") as! VendorCodeViewController
        vc.delegate = self
        vc.offerPosition = dealPosition
        self.present(vc, animated: true, completion: nil)
    }
    
    //VendorcodeDelegate
    func onPressVendorCode(newCode: String, offerPosition:Int?) {
        print("\n VendorCodePress - "+newCode)
        
        if newCode == self.vendorDealList[offerPosition!].identityCode{
            self.redeemNowDeal(dealPosition: offerPosition!)
        }else{
            SnackBarManager.showSnackBar(message: "Invalid vendor code")
        }
    }
    
    //when click on down arrow
    func onClickDownArrow(indexPath: IndexPath, cell:VendordealTableViewCell,  isExanded:Bool) {
        print(CLASS_NAME+" -- onClickDownArrow() -- \(indexPath) -- \(cell)")
        
//        let vendordealTableViewCell:VendordealTableViewCell = tableView.cellForRow(at: indexPath) as! VendordealTableViewCell
//        vendorDealList[indexPath.row - 1].isExpanded = Optional(true)
        
        let estimatedFrameForTitle = NSString(string:vendorDealList[indexPath.row-1].name).boundingRect(with: CGSize(width:(tableView.frame.size.width), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 15.0)!], context: nil)
        
        let estimatedFrameForOrg = NSString(string:vendorDealList[indexPath.row-1].orgName!).boundingRect(with: CGSize(width:(tableView.frame.size.width), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 13.0)!], context: nil)
        
        let estimatedFrameForDescription = NSString(string:vendorDealList[indexPath.row-1].vendorDealDescription!).boundingRect(with: CGSize(width:(tableView.frame.size.width), height: 1500), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 15.0)!], context: nil)
        
        
        if vendorDealList[indexPath.row - 1].isExpanded!{
            print(CLASS_NAME+" -- onClickDownArrow() -- isExpanded : \(vendorDealList[indexPath.row - 1].isExpanded!)")
            
            cell.ivDownArrow.setImage(UIImage(named:"arrow_down"), for: .normal)
            
            UIView.animate(withDuration: 0.3, animations:{
                cell.descriptionHeightContsaint.constant = 35
//                cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height - estimatedFrameForDescription.height)

                cell.layoutIfNeeded()
//                cell.layoutSubviews()
//                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            })
            vendorDealList[indexPath.row - 1].isExpanded = Optional(false)

        }else{
            print(CLASS_NAME+" -- onClickDownArrow() -- isExpanded : \(vendorDealList[indexPath.row - 1].isExpanded!)")
            
            cell.ivDownArrow.setImage(UIImage(named:"arrow_up"), for: .normal)
            
            UIView.animate(withDuration: 0.3, animations:{
                cell.descriptionHeightContsaint.constant = 40 + estimatedFrameForDescription.height
//                cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height + estimatedFrameForTitle.height + estimatedFrameForDescription.height)
                
                cell.layoutIfNeeded()
//                cell.layoutSubviews()
                
            })
            
            vendorDealList[indexPath.row - 1].isExpanded = Optional(true)
            
        }
        
        tableView.beginUpdates()
//        self.tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
    
}

