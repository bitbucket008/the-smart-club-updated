//
//  CategoryFirstItemTableViewCell.swift
//  smartclub
//
//  Created by MacBook on 12/28/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CategoryFirstItemCell: UICollectionViewCell {

    //MARK: Properties
    fileprivate let CLASS_NAME = "CategoryFirstItemCell"
    open var isLoadedFirstTime:Bool!
  
    
    
    //MARK: Outlets
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var lblCategoryName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print(CLASS_NAME+" -- awakeFromNib()")

//        isLoadedFirstTime = true
//        self.isSelected = false
//
        self.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
        self.lblCategoryName.textColor = UIColor.white
//        self.isSelected = true
        
        // Initialization code
        parentView.layer.cornerRadius = parentView.frame.width/2
    }
    

    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        print(CLASS_NAME+" -- layoutSubviews()")
//
//        isLoadedFirstTime = true
//        self.isSelected = true
//
//        // Initialization code
//        parentView.layer.cornerRadius = parentView.frame.width/2
//    }
    

    //check selection
//    override var isSelected: Bool{
//        didSet{
//            print(CLASS_NAME+" -- isSelected -- didSet")
//            if self.isSelected {
//                print(CLASS_NAME+" -- isSelected -- true")
//                self.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
//                self.lblCategoryName.textColor = UIColor.white
//            }
//            else{
//                print(CLASS_NAME+" -- isSelected -- false")
////                if isLoadedFirstTime {
////                    print(CLASS_NAME+" -- isSelected -- isLoadedFirstTime-- true")
////                    self.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
////                    self.lblCategoryName.textColor = UIColor.white
////                    isLoadedFirstTime = false
////                }else{
//                    print(CLASS_NAME+" -- isSelected -- isLoadedFirstTime-- false")
//                    self.parentView.backgroundColor = UIColor.white
//                    self.lblCategoryName.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
////                }
//            }
//
//        }
//    }

}
