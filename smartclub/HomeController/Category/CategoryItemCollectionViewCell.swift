//
//  CategoryItemCollectionViewCell.swift
//  smartclub
//
//  Created by Admin on 29/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class CategoryItemCollectionViewCell: UICollectionViewCell {
    //MARK:Properties
    fileprivate let CLASS_NAME = "CategoryItemCollectionViewCell"
    
    //MARK: Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblCategoryItemTitle: UILabel!
    @IBOutlet weak var categoryImageContainer: CircleBackgroundView!
    
    
    override func awakeFromNib() {
        //setup initial ui
        print(CLASS_NAME+" -- awakeFromNib()")
        setupInitialUI()
        self.categoryImageContainer.isUserInteractionEnabled = true
    }
    
    //MARK: UI Method
    //setup initial ui
    internal func setupInitialUI() -> Void {
        lblCategoryItemTitle!.sizeToFit()
    }
    
    
    //check selection
    override var isSelected: Bool{
        didSet{
            if self.isSelected {
              // self.categoryImageContainer.isUserInteractionEnabled = true
                self.categoryImageContainer.layer.borderWidth = isSelected ? 3.0 : 0
                self.categoryImageContainer.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0).cgColor
                
            }
            else{
              //self.categoryImageContainer.layer.borderWidth = 0
              self.categoryImageContainer.layer.borderColor = UIColor.clear.cgColor
                
            }
        
        }
    }
    
}
