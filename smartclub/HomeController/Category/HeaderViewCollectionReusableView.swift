//
//  HeaderViewCollectionReusableView.swift
//  smartclub
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import SDWebImage

//Delegate
protocol CategoryItemDelegate : NSObjectProtocol {
    func onCategoryItemSelected(itemPosition:Int?)
}

protocol SearchDelegate : NSObjectProtocol {
    func onClickSearchButton(text:String)
    func searchFieldShouldReturn(searchField: UITextField, serachQuery:String)
}

class HeaderViewCollectionReusableView: UICollectionReusableView , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UITextFieldDelegate, UISearchBarDelegate{
    
    //MARK: Instance property
    fileprivate let CLASS_NAME = "HeaderViewCollectionReusableView"
    internal var categoryList:[BusinessCategory] = [BusinessCategory]()
    var catPhotos = ["All","h_icon","product","tecket","desh"];
    open var delegate:CategoryItemDelegate?
    open var tfDelegate:UITextFieldDelegate?
    open var searchdelegate:SearchDelegate?
    open var searchQuery:String!
    open var isLoadedFirstTime:Bool = true
    open var firstCell:Bool = true
    fileprivate var selectedIndexPath = 0
    open var swaponVai = 0
    
    
    //MARK: Outles
    @IBOutlet weak var cvBusinessCategory: UICollectionView!
    @IBOutlet weak var btnSearch: UIBorderButton!
    @IBOutlet weak var tfSearchBox: UITextField!
    @IBOutlet weak var lblCurrentLocation: UILabel!
    @IBOutlet weak var searchBarContainer: UIView!
    
    
    //MARK: UI Elements
    fileprivate let searchBarController:UISearchController = UISearchController(searchResultsController: nil)
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tfSearchBox.delegate = self
        serInnerCollectionView()
        
        configureSearchController()
    }
    
    //configure search controller
    func configureSearchController() -> Void {
        print(CLASS_NAME+" -- configureSearchController() --  searchController - \(searchBarController.description)")
        
//        searchBarController.searchResultsUpdater = self
//        searchBarController.searchBar.searchBarStyle = .default
        searchBarController.searchBar.backgroundImage = UIImage()
        searchBarController.searchBar.isTranslucent = true
        searchBarController.searchBar.backgroundColor = UIColor.clear
        searchBarController.searchBar.barTintColor = UIColor.clear
        searchBarController.searchBar.tintColor =  ColorGenerator.UIColorFromHex(rgbValue: 0x273B90, alpha: 1.0)
        
        let searchField =  searchBarController.searchBar.value(forKey: "searchField") as! UITextField
        searchField.backgroundColor = UIColor.white
//        searchField.contentHorizontalAlignment = .center
//        searchField.contentVerticalAlignment = .center
//        searchField.contentMode = .center
//        searchField.textAlignment = .center
//        searchField.rightView = searchField.leftView as! UIImageView
        searchField.bounds = CGRect(x: searchField.frame.origin.x, y: searchField.frame.origin.y, width: searchField.bounds.size.width, height: searchField.bounds.size.height+40)
        
//        searchField.textColor = UIColor.white
        
        let placeholder =  searchField.value(forKey: "placeholderLabel") as! UILabel
//        placeholder.backgroundColor = UIColor.red
//        placeholder.textColor = UIColor.white
//        placeholder.textAlignment = .center
//        placeholder.center = searchField.center
        
        let searchIcon = searchField.leftView as! UIImageView
//        searchIcon.image?.withRenderingMode(.alwaysOriginal)
        searchIcon.frame = CGRect(x: searchIcon.frame.origin.x, y: searchIcon.frame.origin.y, width: searchIcon.frame.width+10, height: searchIcon.frame.height+10)
        searchIcon.image = UIImage(named: "search_searchBar")
        searchIcon.tintColor = UIColor.white
//        searchIcon.center = searchField.center
        
        let clearButton =  searchField.value(forKey: "clearButton") as! UIButton
        clearButton.tintColor = UIColor.white
        
        searchBarController.searchBar.delegate = self
        searchBarController.dimsBackgroundDuringPresentation = true
        searchBarController.hidesNavigationBarDuringPresentation = false
        searchBarController.searchBar.sizeToFit()
        searchBarContainer.addSubview(searchBarController.searchBar)
    }
    
    //MARK: SearchController
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
         print(CLASS_NAME+" -- searchBarTextDidBeginEditing() -- text : \(searchBar.text!)")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(CLASS_NAME+" -- searchBar() -- textDidChange : \(searchText)")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print(CLASS_NAME+" -- searchBarSearchButtonClicked() -- text : \(searchBar.text!)")
        searchdelegate?.onClickSearchButton(text: searchBar.text!)
//        searchBarController.dimsBackgroundDuringPresentation = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print(CLASS_NAME+" -- searchBarCancelButtonClicked() -- text : \(searchBar.text!)")
        searchdelegate?.onClickSearchButton(text: "")
//        searchBarController.dimsBackgroundDuringPresentation = false
    }
    
    
    //MARK: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("HeaderViewCollectionReusableView -- textFieldShouldReturn()")
        searchdelegate?.searchFieldShouldReturn(searchField: textField, serachQuery:textField.text!)
        return true
    }
    
    //MARK: Actions
    //when click on search button
    @IBAction func onClickSearchButton(_ sender: UIBorderButton) {
        searchdelegate?.onClickSearchButton(text: tfSearchBox.text!)
    }
    
    
    func serInnerCollectionView() {
        
        //
        //        let layout = UICollectionViewFlowLayout()
        //
        //            layout.scrollDirection = .horizontal
        //        self.flashDealCollectionView.collectionViewLayout = layout
        //
        //flashDealCollectionView.scro
        //catCollectionView.contentSize = CGSize.init(width: 70*6, height: 85)
       
        catCollectionView.delegate = self
        catCollectionView.dataSource = self
        catCollectionView.register(UINib(nibName: "CategoryItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "categoryItem")
        catCollectionView.register(UINib(nibName: "CategoryFirstItemCell", bundle: nil), forCellWithReuseIdentifier: "CategoryFirstItemCell")
    
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        catCollectionView.collectionViewLayout = flowLayout;
        //self.myCollectionView.collectionViewLayout = collectionViewFlowLayout;
        
        self.catCollectionView.isPagingEnabled = false;
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//      return 5
        return categoryList.count + 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var  m:CategoryItemCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryItem", for: indexPath) as! CategoryItemCollectionViewCell

      
        
        if indexPath.row == 0 {
            
            print(CLASS_NAME+" -- collectionView() -- cellForItemAt")
            var firstCell:CategoryFirstItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryFirstItemCell", for: indexPath) as! CategoryFirstItemCell
            
//            firstCell.isLoadedFirstTime = isLoadedFirstTime
//            isLoadedFirstTime = false
            
//            if indexPath.row == 0 {
//               firstCell.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
//               firstCell.lblCategoryName.textColor = UIColor.white
//                print("i am calling")
//            } else if indexPath.row == 1 {
//                firstCell.parentView.backgroundColor = UIColor.red
//                firstCell.lblCategoryName.textColor = UIColor.white
//                print("i am not calling")
//            }
            
            if firstCell.isSelected {

                firstCell.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
                firstCell.lblCategoryName.textColor = UIColor.white

                print("if it's not selected - position 0")

            }
//             if firstCell.isSelected != true {
//                firstCell.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
//                firstCell.lblCategoryName.textColor = UIColor.white
//
////                firstCell.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
////                firstCell.lblCategoryName.textColor = UIColor.white
////
////                +collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
//            }
//            if indexPath.row == 1  {
//                  //firstCell.lblCategoryName.textColor = UIColor.blue
//                firstCell.parentView.backgroundColor = UIColor.red
//                firstCell.lblCategoryName.textColor = UIColor.white
//               // collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .bottom)
//            }

//            firstCell.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
//            firstCell.lblCategoryName.textColor = UIColor.white
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.bottom)

            return firstCell
        } else if indexPath.row != 0  {
            m.lblCategoryItemTitle!.text = categoryList[indexPath.row - 1].name
            m.imgView!.sd_setImage(with: URL(string: ApiManager.SERVER_BASE_URL+categoryList[indexPath.row-1].image!)!, placeholderImage: UIImage(named: "not_found"))
            
            m.isSelected = false
        }

        return m;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

//        if indexPath.row > 0 {
//            let estimatedFrameForTitle = NSString(string:categoryList[indexPath.row-1].name).boundingRect(with: CGSize(width:80, height: 1000), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.init(name: "Ridley Grotesk", size: 20.0)!], context: nil)
//
//            return CGSize(width: 80, height: 105+estimatedFrameForTitle.height);
//        }
        
        return CGSize(width: 62, height: 105);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            selectedIndexPath = indexPath.row
            print(CLASS_NAME + " first position \(swaponVai)")
            print(CLASS_NAME + "  selectedIndexPathNext  \(selectedIndexPath)")
            let firstCell = collectionView.cellForItem(at: indexPath) as! CategoryFirstItemCell
            
            firstCell.parentView.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
            firstCell.lblCategoryName.textColor = UIColor.white
        }
        
        if indexPath.row > 0 {
            print(CLASS_NAME + "2nd position \(swaponVai)")
            selectedIndexPath = indexPath.row
            print(CLASS_NAME + "selectedIndexPath  \(selectedIndexPath)")

            let cell = collectionView.cellForItem(at: indexPath) as! CategoryItemCollectionViewCell
            cell.isSelected = true
            
        } 
        delegate?.onCategoryItemSelected(itemPosition: Optional(indexPath.row))
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print(CLASS_NAME + "indexPath.row == 0 ")
        if swaponVai == 0{
             print(CLASS_NAME + "indexPath.row == 0 / didDeselectItemAt")
            if let firstCell = collectionView.cellForItem(at: indexPath) as? CategoryFirstItemCell {
            
                firstCell.parentView.backgroundColor = UIColor.white
                firstCell.lblCategoryName.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
            }
        }
    }
    
    
    @IBOutlet weak var catCollectionView: UICollectionView!
    
    
}

//string extension for excerpt
extension String{
    static func excerpt(value:String, length:Int) -> String{
        if value.count > length {
            return value.substring(to: value.index(value.startIndex, offsetBy: length))+"..."
        }else{
            return value
        }
    }
    
}
