//
//  FlashDealItemCollectionViewCell.swift
//  smartclub
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class FlashDealItemCollectionViewCell: UICollectionViewCell {
    //MARK: Instance variable
    internal var flashDealTitle:String?
    internal var flashDealImage:String?
    
    //MARK: Outlets
    @IBOutlet weak var lblFlashDealTitle: UILabel!
    @IBOutlet weak var ivFlashDealImage: UIImageView!
    
    override func awakeFromNib() {
//        ivFlashDealImage.layer.cornerRadius = ivFlashDealImage.frame.width / 2
//        ivFlashDealImage.clipsToBounds = true
        
    }
    
    //setup initial ui
    public func setupInitialUI() -> Void {
        lblFlashDealTitle!.sizeToFit()
    }
    
}
extension UIImage {
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
}


//For showing the image
//image.userInteractionEnabled = true
//
//let tapRecog = UITapGestureRecognizer(target: self, action: "imgTap:")
//image.addGestureRecognizer(tapRecog)
//
//func imgTap(gestureRecognizer: UITapGestureRecognizer)
//{
//    let tapImg = gestureRecognizer.view!
//
//    let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("NewViewController") as! NewViewController
//    secondViewController.DisplayImg = tapImg  //NewViewController create DisplayImg UIImageView object
//    self.navigationController!.pushViewController(secondViewController, animated: true)
//}

