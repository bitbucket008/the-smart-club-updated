//
//  FlashDealDeatilViewController.swift
//  smartclub
//
//  Created by Admin on 05/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SVProgressHUD

class FlashDealDeatilViewController: UIViewController, UIScrollViewDelegate{
    
    //MARK: Instance Variable
    fileprivate let localStorage = LocalStorage()
    open var spChangedDelegate:SmartPointChangeDelegate?
    fileprivate let CLASS_NAME:String = "FlashDealDeatilViewController"
    internal var flashDealId:String?
    open var flashDealDetails:FlashDealDetails?
    open var flashDealDetailImageUrl:String?
    fileprivate var isScrollDownFirstTime = true
    
    //MARK: Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var swipeMsgView: UIView!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var ivVendorImage: UIImageView!
    @IBOutlet weak var lblSP: UILabel!
    @IBOutlet weak var lblExpiryTime: UIBorderButton!
    @IBOutlet weak var lblFlashDealName: UILabel!
    @IBOutlet weak var lblVendorNameDetails: UILabel!
    @IBOutlet weak var lblFlashDealDescription: UILabel!
    @IBOutlet weak var btnRedeem: UIBorderButton!
    @IBOutlet weak var btnRedeemTop: UIBorderButton!
    @IBOutlet weak var lblExpiredTime: UILabel!
    @IBOutlet weak var ivVendorImageHeight: NSLayoutConstraint!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // init
        initialize()
        
        //get vendor details from server
        getVendorDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UI Method
    //init method
    private func initialize() -> Void {
        print(CLASS_NAME+" -- initialize() -- flash deal id - "+(flashDealId)!)
        print(CLASS_NAME+" -- initialize() -- screenSize - \(UIScreen.main.bounds.size)")
        
        scrollView.delegate = self
        //        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height + 300)
        
        //vendor image
        if let imgUrl = flashDealDetailImageUrl{
            ivVendorImage.sd_setImage(with: URL(string: ApiManager.SERVER_BASE_URL+imgUrl)!, placeholderImage: UIImage(named: "not_found"))
        }
        
        ivVendorImageHeight.constant = UIScreen.main.bounds.size.height
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print(CLASS_NAME+" -- initialize() -- "+"iPhone 5 or 5S or 5C")
            case 1334:
                print(CLASS_NAME+" -- initialize() -- "+"iPhone 6/6S/7/8")
            case 1920, 2208:
                print(CLASS_NAME+" -- initialize() -- "+"iPhone 6+/6S+/7+/8+")
            case 2436:
                print(CLASS_NAME+" -- initialize() -- "+"iPhone X")
                ivVendorImageHeight.constant = 730
            default:
                print(CLASS_NAME+" -- initialize() -- "+"unknown")
            }
        }
        
        print(CLASS_NAME+" -- initialize() -- ivVendorImageSize - \(ivVendorImage.frame.size)")
        print(CLASS_NAME+" -- initialize() -- scrollViewContentSize - \(scrollView.contentSize)")
    }
    
    //MARK: Network call
    //get vendor details from server
    private func getVendorDetails() -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_FLASH_DEAL_DETAILS+"/"+flashDealId!)!,
            method: .get,
            parameters:["flash":"true","platform":"mobile"])//,"customer_id":localStorage.getUser().getId()!,"customer_type":localStorage.getUser().customerType!
            .responseJSON { (response) -> Void in
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON (response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        let dealId = swiftyJson["data"]["dealid"].stringValue
                        let name = swiftyJson["data"]["name"].stringValue
                        let vendorDescription = swiftyJson["data"]["description"].string
                        let orgName = swiftyJson["data"]["org_name"].string
                        let createdAt = swiftyJson["data"]["created_at"].string
                        let expiryTime = swiftyJson["data"]["expiry_time"].string
                        let identityCode = swiftyJson["data"]["identity_code"].string
                        let vendorId = swiftyJson["data"]["vendor_id"].string
                        let gainSP = swiftyJson["data"]["gain_sp"].intValue
                        
                        //make vendor details object
                        self.flashDealDetails = FlashDealDetails(dealId:dealId, name:name, vendorDescription:vendorDescription, orgName:orgName, createdAt:createdAt, expiryTime:expiryTime, gainSP:gainSP, identityCode:identityCode!, vendorId:vendorId!)
                        
                        //populate data with views
                        self.populateDataWithView(flashDealDetails: self.flashDealDetails)
                        
                        //                            SnackBarManager.showSnackBar(message: "Vebdor details loaded")
                        print(self.CLASS_NAME+" -- getVendorDetails()  -- response -- category loaded")
                        
                    }else{
                        //                                SnackBarManager.showSnackBar(message: "Request failed")
                        print(self.CLASS_NAME+" -- getVendorDetails()  -- response -- Request failed")
                    }
                }else{
                    print(self.CLASS_NAME+" -- getVendorDetails()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getVendorDetails()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //post redeem now offer to server
    private func redeemNowOffer() -> Void {
        //show progress dialog
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_FLASH_DEAL_REDEEM)!,
            method: .post,
            parameters: ["customer_id": self.localStorage.getUser().getId()!, "vendor_id": self.flashDealDetails!.vendorId, "deal_id": self.flashDealDetails!.dealId, "vendor_code": self.flashDealDetails!.identityCode, "redeem": true], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        //                        self.offerList.remove(at: offerPosition)
                        //reload tableview data
                        //                        self.cvOwnedOffer.reloadData()
                        SnackBarManager.showSnackBar(message: "You redeemed a deal")
                        print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- You redeemed a deal")
                        
                        print(self.CLASS_NAME+" -- redeemNowOffer()  -- current sp -- "+String(self.localStorage.getUser().earnedSp!))
                        
                        //change user sp
                        //                        let user = self.localStorage.getUser()
                        //                        user.earnedSp = Optional(self.localStorage.getUser().earnedSp! + (self.flashDealDetails?.gainSP)!)
                        //                        self.localStorage.storeUser(user: user)
                        //                        self.spChangedDelegate?.onSmartPointChanged(newSP: self.localStorage.getUser().earnedSp)
                        self.spChangedDelegate?.getSmartPointFromServer()
                        
                        //show vendor code
                        self.showVendorCodeDialog()
                        
                    }else{
                        SnackBarManager.showSnackBar(message: "Error from server")
                        print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- Error from server")
                    }
                }else{
                    print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- redeemNowOffer()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    //populate vendor details data to views
    private func populateDataWithView(flashDealDetails: FlashDealDetails?) -> Void {
        lblSP.text = String(flashDealDetails!.gainSP)
        lblFlashDealName.text = flashDealDetails!.name
        
        if let expiryTime = flashDealDetails!.expiryTime {
            lblExpiredTime.text = expiryTime
        }
        
        if let createdAt = flashDealDetails!.createdAt {
            lblCreatedAt.text = createdAt
        }
        
        if let orgName = flashDealDetails!.orgName {
            lblVendorNameDetails.text = orgName
            lblVendorName.text = orgName
        }
        
        if let vendorDescription = flashDealDetails!.vendorDescription {
            lblFlashDealDescription.text = vendorDescription
        }
    }
    
    //show vendor code dialog
    func showVendorCodeDialog() {
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "RedeemedSuccessViewController") as! RedeemedSuccessViewController
        vc.vendorCode = flashDealDetails!.identityCode
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //MARK: Scroll Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("Content y offet : \(scrollView.contentOffset.y)")
        
        //for toogle swipe view
        if scrollView.contentOffset.y < 120 {
            self.swipeMsgView.isHidden = false
        }else {
            self.swipeMsgView.isHidden = true
        }
        
        //for making bottom inset
        if scrollView.contentOffset.y < 190 {
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = scrollView.contentOffset.y-100
            scrollView.contentInset = contentInset
        }
        
        
        //when swipe down
        if scrollView.contentOffset.y == 0 {
            print(CLASS_NAME+" -- scrollViewDidScroll() -- contentOffset.y = 0")
            //self.goDashboardViewController()
            
            if !isScrollDownFirstTime{
                UIView.animate(withDuration: 0.5, animations: {
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }
        
        
        //when user swipe down first time
        if scrollView.contentOffset.y < 1 {
            print(CLASS_NAME+" -- scrollViewDidScroll() -- contentOffset.y = 0")
            //self.goDashboardViewController()
            
            UIView.animate(withDuration: 0.5, animations: {
                self.dismiss(animated: true, completion: nil)
            })
        }
        
        
        //for tracking first time scrolling
        if scrollView.contentOffset.y > 150 {
            isScrollDownFirstTime = false
        }
        
    }
    
    
    @IBAction func downSwipe(_ sender: UISwipeGestureRecognizer) {
        print("hi there")
        //self.view.backgroundColor = UIColor.red
    }
    
    //    @IBAction func downSwipe(_ sender: Any) {
    //        print(CLASS_NAME+"--i am working--")
    //        self.view.backgroundColor = UIColor.red
    //
    //    }
    
    
    //MARK: Actions
    
    
    //MARK: Actions
    //when click on redeem button
    @IBAction func onClickRedeemButton(_ sender: UIBorderButton) {
        print("Redeem button clicked ....")
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "vendorcodevc") as! VendorCodeViewController
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - on click show dashboard
    @IBAction func onClickDashboard(_ sender: Any) {
        goDashboardViewController()
    }
    
    
    //when click on redeem top button
    @IBAction func onClickRedeemTopButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        print("Redeem top button clicked ....")
        let stboard = UIStoryboard.init(name: "smartshop", bundle: nil)
        let vc = stboard.instantiateViewController(withIdentifier: "vendorcodevc") as! VendorCodeViewController
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    //go dashboard view controller
    private func goDashboardViewController() -> Void {
        let storyBoard = UIStoryboard.init(name: "dashboard", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        vc.isLoginFirstTime = false
        self.present(vc, animated: true, completion: nil)
    }
}


//MARK: Delegates implementation
extension FlashDealDeatilViewController : VendorCodeDelegate {
    
    //VendorcodeDelegate
    func onPressVendorCode(newCode: String, offerPosition:Int?) {
        print("\n VendorCodePress - "+newCode)
        
        if newCode == flashDealDetails!.identityCode{
            self.redeemNowOffer()
        }else{
            SnackBarManager.showSnackBar(message: "Invalid vendor code")
        }
    }
    
}
