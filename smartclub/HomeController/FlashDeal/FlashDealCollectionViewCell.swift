//
//  FlashDealCollectionViewCell.swift
//  smartclub
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import SDWebImage



protocol FlashDealCollectionViewCellDelegate:NSObjectProtocol {
    func onFlashDealItemClicked(position:Int);
}


class FlashDealCollectionViewCell: UICollectionViewCell , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    //MARK: Instance variable
    fileprivate let CLASS_NAME:String = "FlashDealCollectionViewCell"
    internal var flashDealList:[FlashDeal] = [FlashDeal]()
    weak var delegate: FlashDealCollectionViewCellDelegate?
    
    //MARK: UI_Objects
    fileprivate var lblDataNotFound:UILabel!
    
    
    //MARK: Outlets 
    @IBOutlet weak var lblFlashDealItemCount: UILabel!
    @IBOutlet weak var cvFlashDeal: UICollectionView!
    @IBOutlet weak var lblNoFlashDeal: UILabel!
    
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        print(CLASS_NAME+" -- awakeFromNib() -- loaded")
//        //setup initial ui
//        setupUI()
//        serInnerCollectionView()
//
//        //check flash deal
//        if flashDealList.count == 0 {
//            print(CLASS_NAME+" -- awakeFromNib() -- flash_deal_not_found")
//            showFlashDealNotFoundView()
//        }else{
//            print(CLASS_NAME+" -- awakeFromNib() -- flash_deal_available")
//            showFlashDealFoundView()
//        }
//
//        //reload collection view
//        cvFlashDeal.reloadData()
//    }

    override func layoutSubviews() {
        super.layoutSubviews()
        print(CLASS_NAME+" -- layoutSubviews() -- flash_deal_not_found")

        //setup initial ui
        setupUI()
        serInnerCollectionView()

        //check flash deal
//        if flashDealList.count == 0 {
//            print(CLASS_NAME+" -- layoutSubviews() -- flash_deal_not_found")
//            showFlashDealNotFoundView()
//        }else{
//            print(CLASS_NAME+" -- layoutSubviews() -- flash_deal_available")
//            showFlashDealFoundView()
//        }

        //reload collection view
        cvFlashDeal.reloadData()

    }
    
    //MARK: UI Method
    public func setupUI() -> Void {
        //lblFlashDealItemCount
        lblFlashDealItemCount.text = String(flashDealList.count)
        
        //lblDataNotFound
//        lblDataNotFound = UILabel()
//        lblDataNotFound = UILabel(frame: CGRect(x: self.contentView.frame.minX, y: self.contentView.center.y-30, width: self.contentView.frame.width, height: 25))
//        lblDataNotFound.center = self.contentView.center
//        lblDataNotFound.text = "No flash deal found"
//        lblDataNotFound.textAlignment = .center
//        lblDataNotFound.font = UIFont(name: "Ridley Grotesk", size: 16.0)
//        lblDataNotFound.textColor =  ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE)
////        lblDataNotFound.backgroundColor = UIColor.black
//        lblDataNotFound.numberOfLines = 0
//        lblDataNotFound.sizeToFit()
//        lblDataNotFound.isHidden = true
    }
    
    //show data not found view
    func showFlashDealNotFoundView() -> Void{
        print(CLASS_NAME+" -- showFlashDealNotFoundView()")
        cvFlashDeal.isHidden = true
        
//        lblDataNotFound.text = "No flash deal found"
//        self.contentView.addSubview(lblDataNotFound)
//        lblDataNotFound.isHidden = false
        lblNoFlashDeal.isHidden = false
    }
    
    //show data found view
    func showFlashDealFoundView() -> Void {
        print(CLASS_NAME+" -- showFlashDealFoundView()")
        cvFlashDeal.isHidden = false
        
//        lblDataNotFound.text = ""
//        lblDataNotFound.isHidden = true
//        lblDataNotFound.removeFromSuperview()
        lblNoFlashDeal.isHidden = true
    }
    
    //set flash deal cpllection view
    func serInnerCollectionView() {
//
//        let layout = UICollectionViewFlowLayout()
//
//            layout.scrollDirection = .horizontal
//        self.flashDealCollectionView.collectionViewLayout = layout
//
        //flashDealCollectionView.scroll
        cvFlashDeal.delegate = self
        cvFlashDeal.dataSource = self
        cvFlashDeal.register(UINib(nibName: "FlashDealItemView", bundle: nil), forCellWithReuseIdentifier: "flashdealitem")
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//         return 1
        
        return flashDealList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print(CLASS_NAME+" -- collectionView() -- cellForItemAt")
        
        let m:FlashDealItemCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "flashdealitem", for: indexPath) as! FlashDealItemCollectionViewCell
        m.clipsToBounds = true
        
        m.flashDealTitle = flashDealList[indexPath.row].orgName
        m.flashDealImage = flashDealList[indexPath.row].dealImage
        m.lblFlashDealTitle.text = flashDealList[indexPath.row].orgName
        m.ivFlashDealImage!.sd_setImage(with: URL(string: ApiManager.SERVER_BASE_URL+flashDealList[indexPath.row].dealImage!)!, placeholderImage: UIImage(named: "not_found"))
//        m.ivFlashDealImage!.sd_addActivityIndicator()
//        m.ivFlashDealImage!.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
//        m.ivFlashDealImage.sd_showActivityIndicatorView()
        
        return m;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = 80;

        return CGSize(width: width+10, height: 100);
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let delegate = delegate else {
            return
        }
        
        delegate.onFlashDealItemClicked(position: indexPath.row)
        
    }
    
    
    
    @IBOutlet weak var flashDealCollectionView: UICollectionView!
}
