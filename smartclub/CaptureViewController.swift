//
//  CaptureViewController.swift
//  smartclub
//
//  Created by Admin on 05/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import AVFoundation



class CaptureViewController: UIViewController {

    
    var session: AVCaptureSession!
    var stillImageOutput: AVCaptureStillImageOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var cameraPreview:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        session = AVCaptureSession()
        session!.sessionPreset = .photo
        let backCamera = AVCaptureDevice.default(for: .video)
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
          
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
               
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspect
                videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                cameraPreview.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
                
            }
            
            
        }
        
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
       // startTutorilasSlider()
        
        videoPreviewLayer!.frame = cameraPreview.frame
    }
    
    
    
    @IBAction func onCaptureImage(sender:UIButton?) {
        
        self.captureImage()
        
    }
    
    
    
    
    func captureImage() {
        
       // stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
       // session.addOutput(stillImageOutput)
        
        if let videoConnection = stillImageOutput.connection(with: .video) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: {
                (sampleBuffer, error) in

                if sampleBuffer != nil {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                    let dataProvider = CGDataProvider(data: imageData as! CFData)
                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                    let image = UIImage.init(cgImage: cgImageRef!, scale: 1.0, orientation: .right)
                    
                    
                    
                    self.startTutorilasSlider()
                    
                }
                
               
                //Save the captured preview to image
                //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                
            })
        }
        
        
    }
    
    
    
    func startTutorilasSlider() {
        
        let storyBoard = UIStoryboard.init(name: "tutorial", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "tutorialvc") as! TutorialSliderViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
