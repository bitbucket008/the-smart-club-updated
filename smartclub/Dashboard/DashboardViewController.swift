//
//  DashboardViewController.swift
//  smartclub
//
//  Created by Admin on 26/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import SVProgressHUD


//MARK: Protocols
protocol SmartPointChangeDelegate : NSObjectProtocol {
    func onSmartPointChanged(newSP:Int?)
    func getSmartPointFromServer()
}

class DashboardViewController: UIViewController {
    
    //MARK: Properties
    open let CLASS_NAME = "DashboardViewController"
    fileprivate let localStorage = LocalStorage()
    open var isLoginFirstTime:Bool!
    fileprivate static var sharedInstance:DashboardViewController!
    
    
    //MARK: Outlets
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var homeIcon:UIImageView!
    @IBOutlet weak var smartBagIcon:UIImageView!
    @IBOutlet weak var cameraIcon:UIImageView!
    @IBOutlet weak var walletIcon:UIImageView!
    @IBOutlet weak var accountIcon:UIImageView!
    @IBOutlet weak var homeTitle:UILabel!
    @IBOutlet weak var smartBagTitle:UILabel!
    @IBOutlet weak var walletTitle:UILabel!
    @IBOutlet weak var accountTitle:UILabel!
    @IBOutlet weak var iblSP: UILabel!
    
    var previousSelectedIcon:UIImageView!
    var previousSeelctedTitle:UILabel!
    
    
    
    var unTappedIcon = ["home","smart_bag","wallet","profile"]
    var tappedIcon = ["home_tapped","smart_bag_tapped","wallet_tapped","profile_img"]
    
    var previousIndex:Int!
    var currentIndex:Int!
    
    var previousVc:UIViewController!
    
    //get static instance
    open static func getsharedInstance() -> DashboardViewController {
        print("DashboardViewController -- getsharedInstance()")
        
        if sharedInstance == nil {
            sharedInstance = UIStoryboard.init(name: "dashboard", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        }
        
        return sharedInstance
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //initialization
        initialization()
        
        // Do any additional setup after loading the view.
        let strBoard = UIStoryboard.init(name: "dashboard", bundle: nil)
        
        let vc:DashboardNavigationController = strBoard.instantiateViewController(withIdentifier: "dashboardnvc") as! DashboardNavigationController
        
        let homeVC:DashboardHomeViewController = strBoard.instantiateViewController(withIdentifier: "dashboardhome") as! DashboardHomeViewController
        homeVC.spChangedDelegate = self
        homeVC.isloginFirstTime = isLoginFirstTime
        
        vc.pushViewController(homeVC, animated: true)
        
        previousVc = vc;
        
        showSubViewContrller(subViewController: vc)
        
        
        previousSelectedIcon = homeIcon
        previousSeelctedTitle = homeTitle
        
        previousIndex = 0;
        currentIndex = 0;
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(openSmartCart), name: NSNotification.Name(rawValue: "openSmartCartObs"), object: nil)
        
    }
    
    //MARK: Init method
    //initialization
    private func initialization() -> Void {
        print(CLASS_NAME+" -- initialization() -- cusomerId -- "+localStorage.getUser().getId()!)
        
        //get cstomer updated sp
        self.iblSP!.text = String(describing: RoundNumberFormatter.roundedWithAbbreviations(num: Double(localStorage.getUser().earnedSp!)))
        getCustomerUpdatedSp()
        
        print(CLASS_NAME+" -- initialization() -- SP -- "+String(describing: localStorage.getUser().earnedSp!))
    }
    
    
    @objc func openSmartCart() {
        self.smartBagIcon.image = UIImage.init(named: "smart_bag_tapped")
        previousIndex = currentIndex
        currentIndex = 1
        updateTapView(title: smartBagTitle, icon: smartBagIcon)
        
        previousSeelctedTitle = smartBagTitle
        previousSelectedIcon = smartBagIcon
        
        removePreviousViewController()
        
        
        let storyBoard = getStoryBoardByIndentifier(identifier: "smartshop")
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "SmartShopViewController") as! SmartShopViewController
        vc.spChangeDelegate = self
        
        showSubViewContrller(subViewController: vc)
    }
    
    
    func showSubViewContrller(subViewController:UIViewController) {
        
        self.addChildViewController(subViewController)
        
        subViewController.view.frame = containerView.frame
        self.containerView.addSubview(subViewController.view)
        
        subViewController.didMove(toParentViewController: self)
        previousVc = subViewController
    }
    
    
    
    func getStoryBoardByIndentifier(identifier:String)->UIStoryboard {
        return  UIStoryboard.init(name: identifier, bundle: nil)
    }
    
    
    @IBAction func homeTapAction(_ sender: UITapGestureRecognizer) {
        
        self.homeIcon.image = UIImage.init(named: "home_tapped")
        
        previousIndex = currentIndex
        currentIndex = 0
        updateTapView(title: homeTitle, icon: homeIcon)
        
        previousSeelctedTitle = homeTitle
        previousSelectedIcon = homeIcon
        
        
        removePreviousViewController()
        
        
        let storyBoard = getStoryBoardByIndentifier(identifier: "dashboard")
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "dashboardnvc") as! DashboardNavigationController
        
        let homeVC:DashboardHomeViewController = storyBoard.instantiateViewController(withIdentifier: "dashboardhome") as! DashboardHomeViewController
        homeVC.spChangedDelegate = self
        
        vc.pushViewController(homeVC, animated: true)
        
        showSubViewContrller(subViewController: vc)
        
    }
    
    
    
    
    func removePreviousViewController() {
        previousVc.willMove(toParentViewController: nil)
        previousVc.view.removeFromSuperview()
        previousVc.removeFromParentViewController()
    }
    
    
    @IBAction func smartBagTapAction(_ sender: UITapGestureRecognizer) {
        self.smartBagIcon.image = UIImage.init(named: "smart_bag_tapped")
        previousIndex = currentIndex
        currentIndex = 1
        updateTapView(title: smartBagTitle, icon: smartBagIcon)
        
        previousSeelctedTitle = smartBagTitle
        previousSelectedIcon = smartBagIcon
        
        removePreviousViewController()
        
        
        let storyBoard = getStoryBoardByIndentifier(identifier: "smartshop")
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "SmartShopViewController") as! SmartShopViewController
        vc.spChangeDelegate = self
        
        showSubViewContrller(subViewController: vc)
        
    }
    
    
    @IBAction func smartPointTapAction(_ sender: UITapGestureRecognizer) {
        
        self.homeIcon.image = UIImage.init(named: "home")
        self.homeTitle.textColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0)
        self.smartBagIcon.image = UIImage.init(named: "smart_bag")
        self.smartBagTitle.textColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0)
        self.walletIcon.image = UIImage.init(named: "wallet")
        self.walletTitle.textColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0)
        self.accountIcon.image = UIImage.init(named: "profile")
        self.accountTitle.textColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0)

        
        removePreviousViewController()
        
        
        let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //let vc = storyBoard.instantiateViewController(withIdentifier: "earnpointvc") as! EarnPointViewController
        let vc = storyBoard.instantiateViewController(withIdentifier: "EarnPointNavigationController") as! EarnPointNavigationController
        vc.isBackButtonNeeded = false
        vc.spChangeDelegate = self
        showSubViewContrller(subViewController: vc)
    }
    
    
    @IBAction func walletTapAction(_ sender: UITapGestureRecognizer) {
        self.walletIcon.image = UIImage.init(named: "wallet_tapped")
        previousIndex = currentIndex
        currentIndex = 2
        
        updateTapView(title: walletTitle, icon: walletIcon)
        
        previousSeelctedTitle = walletTitle
        previousSelectedIcon = walletIcon
        
        
        
        removePreviousViewController()
        
        
        let storyBoard = getStoryBoardByIndentifier(identifier: "wallet")
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "walletvc") as! WalletViewController
        
        showSubViewContrller(subViewController: vc)
        
        
        
    }
    @IBAction func accountTapAction(_ sender: UITapGestureRecognizer) {
        
        self.accountIcon.image = UIImage.init(named: "profile_tapped")
        
        previousIndex = currentIndex
        currentIndex = 3
        updateTapView(title: accountTitle, icon: accountIcon)
        
        previousSeelctedTitle = accountTitle
        previousSelectedIcon = accountIcon
        
        
        
        removePreviousViewController()
        let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        let vc = storyBoard.instantiateViewController(withIdentifier: "customnvc") as! CustomNavigation
        
        
        //
        //                let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //
        //                let vc = storyBoard.instantiateViewController(withIdentifier: "goalvc") as! GoalViewController
        
        
        //                        let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //
        //                        let vc = storyBoard.instantiateViewController(withIdentifier: "surveyvc") as! SurveyViewController
        
        
        
        
        //        let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //        let vc = storyBoard.instantiateViewController(withIdentifier: "surveyformvc") as! SurveyFormViewController
        //
        //achivementvc
        
        
        //        let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //        let vc = storyBoard.instantiateViewController(withIdentifier: "achivementvc") as! AchivementViewController
        //
        //                let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //                let vc = storyBoard.instantiateViewController(withIdentifier: "settingsvc") as! SettingsViewController
        
        
        //faqvc
        
        //        let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //        let vc = storyBoard.instantiateViewController(withIdentifier: "faqvc") as! TempViewController
        //
        //
        //        showSubViewContrller(subViewController: vc)
        
        //
        //                let storyBoard = getStoryBoardByIndentifier(identifier: "account")
        //                let vc = storyBoard.instantiateViewController(withIdentifier: "termsconditionvc") as! TermsConditionViewController
        //
        
        showSubViewContrller(subViewController: vc)
        
        //termsconditionvc
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isLoginFirstTime{
            showCongrssPointsPopUp()
        }
    }
    
    func updateTapView(title:UILabel, icon:UIImageView) {
        
        previousSeelctedTitle.textColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0)
        previousSelectedIcon.image = UIImage.init(named: unTappedIcon[previousIndex])
        
        title.textColor = UIColor(red:0.26, green:0.18, blue:0.49, alpha:1.0)
        icon.image = UIImage.init(named: tappedIcon[currentIndex])
        
    }
    
    func showCongrssPointsPopUp() {
        
        let  dashboard = UIStoryboard.init(name: "dashboard", bundle: nil)
        let vc = dashboard.instantiateViewController(withIdentifier: "congrasspop") as! CongrassViewController
        
        self.present(vc, animated: false, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UpdateUI Method
    open func updateSP(sp:Int) -> Void {
        print(CLASS_NAME+" -- updateSP() -- new-sp : \(sp)")
        self.iblSP!.text = String(RoundNumberFormatter.roundedWithAbbreviations(num: Double(sp)))
        
    }
    
    //MARK: Network Call
    //get customer updated sp  information
    open func getCustomerUpdatedSp() -> Void {
//        SVProgressHUD.show()
        
        print("Profile get url - " + ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)!,
            method: .get,
            parameters: ["platform":"mobile"])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
//                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        //                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        let sp = swiftyJson["data"]["sp_earned"].intValue
                        let user = self.localStorage.getUser()
                            user.earnedSp = Optional(sp)
                        self.localStorage.storeUser(user: user)
                        self.updateSP(sp: sp)
                        
                    }else{
                        //                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getCustomerUpdatedSp()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getCustomerUpdatedSp()  -- response -- "+(String(describing: response)))
                
        }
    }
    
}

//MARK: Delegatation
extension DashboardViewController : SmartPointChangeDelegate{
    
    //SmartPointChangeDelegate
    func onSmartPointChanged(newSP: Int?) {
        print(CLASS_NAME+" -- SP changed ...." + String(newSP!))
        if let sp = newSP{
//            self.iblSP!.text = String(sp)
            updateSP(sp: sp)
        }
    }
    
    //get customer smart point
    func getSmartPointFromServer() {
        self.getCustomerUpdatedSp()
    }
}
