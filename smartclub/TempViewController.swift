//
//  TempViewController.swift
//  smartclub
//
//  Created by Admin on 17/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit


class TempViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , FAQTableViewCellDelegate {
    
    
    
    
    
    
    @IBOutlet weak var tableView:UITableView!
    
    
    
    
    var data = [FAQData.init(title: "Question Title", details: "Question details ", isExpandable: false),FAQData.init(title: "Question Title", details: "Question details ", isExpandable: false),FAQData.init(title: "Question Title", details: "Question details ", isExpandable: true),FAQData.init(title: "Question Title", details: "Question details ", isExpandable: false)];
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tableView.register(UINib(nibName: "FAQTableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "faqtableviewcell")
        
        
        
        tableView.register(UINib(nibName: "FAQExpendableTableViewCell", bundle: nil), forCellReuseIdentifier: "faqexpendabletableviewcell")
        
        
    }
    
    
    
    
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        let d = data[section]
        
        
        
        print("Called from row section \(d.isExpandable) " )
        
        
        
        
        if d.isExpandable == true {
            return 1;
        }
        
        return 0;
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "faqexpendabletableviewcell", for: indexPath) as! FAQExpendableTableViewCell
        
        return cell;
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "faqtableviewcell") as! FAQTableViewCell
        // cell.questionTitle.text = "This is a question title"
        cell.delegate = self
        cell.section = section
        
        var d = data[section];
        
        //        if d.isExpandable == false {
        //            d.isExpandable = true
        //        } else {
        //            d.isExpandable = false
        //
        //        }
        
        cell.questionTitle.text = d.title;
        return cell;
    }
    
    
    
    func toggleSection(header: FAQTableViewCell, section: Int) {
        
        print("current expendble  delegate")
        var d = data[section]
        
        if d.isExpandable == false {
            d.expendableStatus(status: true)
        } else {
            d.expendableStatus(status: false)
        }
        
       tableView.reloadSections([section], with: .fade)
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80;
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45;
}

}

