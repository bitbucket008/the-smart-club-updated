//
//  InstagramWebViewController.swift
//  
//
//  Created by Mominur Rahman on 6/7/18.
//

import UIKit
import WebKit

class InstagramWebViewController: UIViewController {
    
    @IBOutlet var webView: WKWebView!
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //MARK: - Spinner animation
        
        spinner.startAnimating()
        
        //spinner.isHidden = false
        
        // Do any additional setup after loading the view, typically from a nib.
        
        let myURL = URL(string:"https://www.instagram.com/?hl=en")
        
        let myRequest = URLRequest(url: myURL!)
        
        webView.navigationDelegate = self
        
        webView.load(myRequest)
        
        
        
        //DispatchQueue.main.asyncAfter(deadline: .now() + 13.0) {
        
        //self.spinner.stopAnimating()
        
        //self.spinner.isHidden = true
        
        //}
        
        //MARK: - Activity Indicator
        
        increaseSizeOfActivityIndicator()
        
    }
    
    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }
    
    func increaseSizeOfActivityIndicator(){
        
        let transform = CGAffineTransform(scaleX: 2, y: 2)
        
        spinner.transform = transform
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBAction func backbuttonToSubscribe(_ sender: Any) {
        print("I am in")
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension InstagramWebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) {
        
        self.spinner.stopAnimating()
        
        self.spinner.isHidden = true
        
    }
    
}
