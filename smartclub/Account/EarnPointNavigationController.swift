//
//  EarnPointNavigationController.swift
//  smartclub
//
//  Created by Mac-Admin on 3/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class EarnPointNavigationController: UINavigationController {

    open  var isBackButtonNeeded:Bool!
    open var spChangeDelegate:SmartPointChangeDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // load vc
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        
        let vc = strBoard.instantiateViewController(withIdentifier: "earnpointvc") as! EarnPointViewController
        vc.isNeededBackButton = false
        vc.spChangeDelegate = spChangeDelegate
        self.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
