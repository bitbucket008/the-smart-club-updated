//
//  TermsConditionViewController.swift
//  smartclub
//
//  Created by Admin on 17/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class TermsConditionViewController: UIViewController {
    fileprivate let  CLASS_NAME = "TermsConditionViewController"
    open var loadedFrom:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func backBtnAction(sender:UIButton) {
        if let loadedFrom = loadedFrom, loadedFrom == "Registration"{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
