//
//  InviteFridendViewController.swift
//  smartclub
//
//  Created by Admin on 13/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class InviteFridendViewController: UIViewController {

    //MARK: Properties
    fileprivate let CLASS_NAME = "InviteFridendViewController"
    fileprivate let localStorage = LocalStorage()
    fileprivate var invitationLetter:String?
    fileprivate var invitationCode:String!

    
    //MARK: Outlets
    @IBOutlet weak var lblTapToCopy: UILabel!
    @IBOutlet weak var lblRedeemedCode: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(CLASS_NAME+" -- viewDidLoad()")      
        
        // Do any additional setup after loading the view.
        lblRedeemedCode.text = localStorage.getUser().referralCode
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     //MARK: Actions
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickTapToCopy(_ sender: UITapGestureRecognizer) {
        print(CLASS_NAME+" -- onClickTapToCopy()")
        
        
    }
    
    //when click on invitaion code
    @IBAction func onClickInvitationCode(_ sender: UITapGestureRecognizer) {
        print(CLASS_NAME+" -- onClickInvitationCode() -- code -- \(sender)")
        generateInvitationLetterAndShare()
    }
    
    
    //Utils method 
    private func generateInvitationLetterAndShare() -> Void {
        invitationLetter = "You're invited to check out The Smart Club for a better way to spend, save and smile. Signup & get free credit. Let's Go! \n\n http://beta.thesmartclubapp.com/register?ref_code="+localStorage.getUser().referralCode!
        
        //copy the content to paste board
        UIPasteboard.general.string = invitationLetter
        
        print(CLASS_NAME+" -- generateInvitationLetterAndShare() -- "+UIPasteboard.general.string!)
        
        // set up activity view controller
        let contentToShare = [ invitationLetter! ]
        let activityViewController = UIActivityViewController(activityItems: contentToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }

}
