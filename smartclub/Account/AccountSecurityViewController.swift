//
//  AccountSecurityViewController.swift
//  smartclub
//
//  Created by RASHED on 1/17/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class AccountSecurityViewController: UIViewController {
    //MARK: Properties
    fileprivate let CLASS_NAME:String = "AccountSecurityViewController"
    let localStorage = LocalStorage()
    fileprivate var profileScore = 20
    private var fullname:String = ""
    private var gender:String = ""
    private var nationality:String = ""
    private var phoneNumber:String = ""
    private var birthday:String = ""
    private var maritalStatus:String = ""
    private var anniverseryDay:String = ""
    
    //MARK: Outlets
    @IBOutlet weak var progressViewPartOne: UIView!
    @IBOutlet weak var progressViewPartTwo: UIView!
    @IBOutlet weak var progressViewPartThree: UIView!
    @IBOutlet weak var progressViewPartFour: UIView!
    @IBOutlet weak var ivProgressPartOne: UIImageView!
    @IBOutlet weak var ivProgressPartTwo: UIImageView!
    @IBOutlet weak var ivProgressPartThree: UIImageView!
    @IBOutlet weak var ivProgressPartFour: UIImageView!
    
    @IBOutlet weak var pvPartTwoContainer: UIView!
    @IBOutlet weak var pvPartThreeContainer: UIView!
    @IBOutlet weak var pvPartFourContainer: UIView!
    
    @IBOutlet var securityUIView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundLabelForsecurityUIView()
        
        // get profile scrore
//        getProfileInformation()
       
    }
   
    func roundLabelForsecurityUIView() {
        securityUIView.layer.cornerRadius = 6
        securityUIView.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0).cgColor
        securityUIView.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        securityUIView.layer.shadowRadius = 1
        securityUIView.layer.shadowOpacity = 0.25
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.  EmailEditViewController
        
        
    }
    //MARK: Action
    @IBAction func onClickEmailEdit(_ sender: Any) {
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)

        let vc = strBoard.instantiateViewController(withIdentifier: "EmailEditViewController") as!  EmailEditViewController

        self.navigationController?.pushViewController(vc, animated: true)
        
       
        
    }
    
    //MARK: Action
    
    @IBAction func onClickEditPassword(_ sender: Any) {
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        
        let vc = strBoard.instantiateViewController(withIdentifier: "EditPasswordViewController") as!  EditPasswordViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Mark: Action
    @IBAction func backButtonToProfileViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Profile Calculation
    private func calculateProfileScore() -> Void {
        if fullname != "" {
            profileScore = profileScore + 10
        }
        if nationality != "" {
            profileScore = profileScore + 10
        }
        if gender != "" {
            profileScore = profileScore + 10
        }
        if phoneNumber != "" {
            profileScore = profileScore + 10
        }
        if birthday != "" {
            profileScore = profileScore + 10
        }
        if maritalStatus != "" {
            profileScore = profileScore + 10
        }
        if anniverseryDay != "" {
            profileScore = profileScore + 10
        }
        
        print(CLASS_NAME+" -- calculateProfileScore() -- "+String(profileScore))
    }
    
    //show profile complete progressbar
    private func showProfileCompleteProgressBar() -> Void {
        if profileScore >= 25 {
            progressViewPartOne.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
        if profileScore >= 50 {
            pvPartTwoContainer.alpha = 1.0
            progressViewPartTwo.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
        if profileScore >= 75 {
            pvPartThreeContainer.alpha = 1.0
            progressViewPartThree.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
        if profileScore == 100 {
            pvPartFourContainer.alpha = 1.0
            progressViewPartFour.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
    }
    
    //MARK: Network Call
    //get profile information
    private func getProfileInformation() -> Void {
        SVProgressHUD.show()
        
        print("Profile update url - " + ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)!,
            method: .get,
            parameters: nil)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        self.fullname = swiftyJson["data"]["full_name"].stringValue
                        self.gender = swiftyJson["data"]["gender"].stringValue
                        self.nationality = swiftyJson["data"]["nationality"].stringValue
                        self.birthday = swiftyJson["data"]["birth_date"].stringValue
                        self.anniverseryDay = swiftyJson["data"]["anniversary_date"].stringValue
                        self.phoneNumber = swiftyJson["data"]["phone"].stringValue
                        self.maritalStatus = swiftyJson["data"]["marital_status"].stringValue
                        
                        //calculate profile scvore and polpulate ui
                        self.calculateProfileScore()
                        self.showProfileCompleteProgressBar()
                        
                    }else{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- request failed")
//                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
    
}
