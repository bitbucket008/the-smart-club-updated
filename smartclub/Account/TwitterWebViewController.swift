//
//  TwitterWebViewController.swift
//  smartclub
//
//  Created by Mominur Rahman on 6/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit

class TwitterWebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // spinner animation
        spinner.startAnimating()
        
        let myURL = URL(string:"https://twitter.com/?lang=en")
        let myRequest = URLRequest(url: myURL!)
        webView.navigationDelegate = self
        webView.load(myRequest)
        
        
        increaseSizeOfActivityIndicator()
        
    }
    
    func increaseSizeOfActivityIndicator(){
        
        let transform = CGAffineTransform(scaleX: 2, y: 2)
        
        spinner.transform = transform
        
    }
    
}

extension TwitterWebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) {
        
        self.spinner.stopAnimating()
        
        self.spinner.isHidden = true
        
    }
    
}
