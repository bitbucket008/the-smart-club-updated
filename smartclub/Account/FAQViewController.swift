//
//  FAQViewController.swift
//  smartclub
//
//  Created by Admin on 14/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit


class FAQData {
    var title:String!
    var details:String!
    open var isExpandable:Bool!
    
    init(title:String, details:String,isExpandable:Bool) {
        self.title = title
        self.details = details
        self.isExpandable = isExpandable
    }
    
    
 func expendableStatus(status:Bool)  {
        self.isExpandable = status
    }
    
    
}


class FAQViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , FAQTableViewCellDelegate {
    
    @IBOutlet weak var tableView:UITableView!

    
    var data = [FAQData.init(title: "Lorem Ipsum Question's", details: "Lorem Ipsum Question's", isExpandable: false),FAQData.init(title: "Lorem Ipsum Question's", details: "Question details ", isExpandable: false),FAQData.init(title: "Lorem Ipsum Question's", details: "Question details ", isExpandable: true),FAQData.init(title: "Lorem Ipsum Question's", details: "Question details ", isExpandable: false)];
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tableView.register(UINib(nibName: "FAQTableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "faqtableviewcell")
      
       
        
            tableView.register(UINib(nibName: "FAQExpendableTableViewCell", bundle: nil), forCellReuseIdentifier: "faqexpendabletableviewcell")
        
        
    }
    
    
    
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        let d = data[section]
        
        
       
        
        
        if d.isExpandable == true {
            return 1;
        }
        
        return 0;
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "faqexpendabletableviewcell", for: indexPath) as! FAQExpendableTableViewCell

        return cell;
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "faqtableviewcell") as! FAQTableViewCell
       // cell.questionTitle.text = "This is a question title"
        cell.delegate = self
        cell.section = section
        
        var d = data[section];
        
//        if d.isExpandable == false {
//            d.isExpandable = true
//        } else {
//            d.isExpandable = false
//
//        }
        
        cell.questionTitle.text = d.title;
        return cell;
    }
    
    
    
    func toggleSection(header: FAQTableViewCell, section: Int) {
        
         print("current expendble  delegate")
        var d = data[section]
        
        if d.isExpandable == false {
            d.expendableStatus(status: true)
        } else {
            d.expendableStatus(status: false)
        }
        
        tableView.beginUpdates()
        tableView.reloadData()
       //tableView.reloadSections([IndexSet.init(integer: section)], with: .fade)
        //tableView.reloadSections(<#T##sections: IndexSet##IndexSet#>, with: <#T##UITableViewRowAnimation#>)
        tableView.endUpdates()
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let currentFAQ = data[indexPath.row];
        if currentFAQ.isExpandable == true {
            return 100;
        }

        return 45;
  
    }
    

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45;
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
