//
//  ContactUsViewController.swift
//  smartclub
//
//  Created by Admin on 14/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    //MARK: Properties
    fileprivate let CLASS_NAME = "ContactUsViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Actions
    //when click on back button
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }


    //when click on email us button
    
    @IBAction func onClickEmailUsButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        
        print(CLASS_NAME+" -- onClickEmailUsButton()")
        
        let email = "support@thesmartclubapp.com"
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
    }
    
    //when click on call us button
    @IBAction func onClickCallUsBitton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        
        print(CLASS_NAME+" -- onClickCallUsBitton()")
        
        let number = "+97477952452"
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
