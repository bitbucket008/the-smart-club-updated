//
//  FacebookWebViewController.swift
//  smartclub
//
//  Created by Mominur Rahman on 6/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit

class FacebookWebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //start spinner
        spinner.startAnimating()
        //URL load
        let myURL = URL(string: "https://www.facebook.com/")
        let myRequest = URLRequest(url: myURL!)
        webView.navigationDelegate = self
        webView.load(myRequest)
        
        increaseSizeOfActivityIndicator()
    }
    
    //SIze of activity indicator
    func increaseSizeOfActivityIndicator(){
        let transform = CGAffineTransform(scaleX: 2, y: 2)
        spinner.transform = transform
    }
    
    //back button
    @IBAction func backToSubscribe(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//Extension
extension FacebookWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.spinner.stopAnimating()
        self.spinner.isHidden = true
    }
}
