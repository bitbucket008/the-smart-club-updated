//
//   EarnPointViewController.swift
//  smartclub
//
//  Created by Admin on 12/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class EarnPointViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    open var isNeededBackButton:Bool!
    open var spChangeDelegate:SmartPointChangeDelegate!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var btnBack: UIButton!
    
    
    var titles = ["BUY POINTS","COMPLETE YOUR PROFILE", "SUBSCRIBE TO SOCIAL MEDIA", "REFER A FRIEND", "TAKE SURVEYS"] //,"COMPLETE YOUR PROFILE", "SUBSCRIBE TO SOCIAL MEDIA", "REFER A FRIEND","TAKE SURVEYS"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // check back button
        if !isNeededBackButton {
            btnBack.isHidden = true
        }
        
        tableView.register(UINib(nibName: "EarnPointsTableViewCell", bundle: nil), forCellReuseIdentifier: "earnpointtableviewcell")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5 //make it 5 when uncomment those items in titles
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "earnpointtableviewcell", for: indexPath) as! EarnPointTableViewCell
        
        cell.selectionStyle = .none
        
        let imgName = "ern_point_\(indexPath.row)"
        cell.imgView.image = UIImage.init(named: imgName)
        cell.titleLabel.text = titles[indexPath.row]
        
        // Configure the cell...
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let stboard = UIStoryboard.init(name: "mycart", bundle: nil)
            let vc = stboard.instantiateViewController(withIdentifier: "popupbuyspvc") as! PopupBuySPViewController
            vc.spChangeDelegate = spChangeDelegate
            vc.headerText = ""
            //            self.navigationController?.pushViewController(vc, animated: true)
            self.present(vc, animated: true, completion: nil)
        }
            
        else if indexPath.row == 1 {
            //complete your profile
            let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            
            let vc = strBoard.instantiateViewController(withIdentifier: "profilevc") as! ProfileViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
            //            //Hide social media
        else if indexPath.row == 2 {
            
            //subscribe to social media
            let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            
            let vc = strBoard.instantiateViewController(withIdentifier: "followsocialvc") as! FlowSocialViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if indexPath.row == 3 {
            //invitefriendvc
            let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            
            let vc = strBoard.instantiateViewController(withIdentifier: "invitefriendvc") as! InviteFridendViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
            //            //Hide Take Survey
        else if indexPath.row == 4 {
            //invitefriendvc
            let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            
            let vc = strBoard.instantiateViewController(withIdentifier: "surveyvc") as! SurveyViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
