//
//  SurveyFormButtonCellCollectionViewCell.swift
//  smartclub
//
//  Created by Mac-Admin on 2/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

//MARK: Protocols
protocol SurveySubmitDelegate : NSObjectProtocol{
    func onSubmitSurvey(sender:UIBorderButton)
}

class SurveyFormButtonCell: UICollectionViewCell {
    //MARK: Properties
    fileprivate let CLASS_NAME = "SurveyFormButtonCell"
    open var delegate:SurveySubmitDelegate?
    
    
    //MARK: Outlets
    @IBOutlet weak var btnSubmit: UIBorderButton!
    @IBOutlet weak var uivButtonContainer: UIView!
    @IBOutlet weak var constrainHorizontalCenter: NSLayoutConstraint!
    
    override func awakeFromNib() {
//        uivButtonContainer.frame = CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: 150)
//        addConstraint(NSLayoutConstraint(item: btnSubmit, attribute: .trailing, relatedBy: .equal, toItem: uivButtonContainer, attribute: .trailing, multiplier: 1.0, constant: (uivButtonContainer.frame.width/2)-(uivButtonContainer.frame.width*60/100)))
    }
    override func layoutSubviews() {
        uivButtonContainer.frame = CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: 150)
        addConstraint(NSLayoutConstraint(item: btnSubmit, attribute: .leading, relatedBy: .equal, toItem: uivButtonContainer, attribute: .leading, multiplier: 1.0, constant: ((UIScreen.main.bounds.width/2)-btnSubmit.frame.width/2)))
    }
    
    //MARK: Actions
    @IBAction func onClickSubmitButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        delegate?.onSubmitSurvey(sender: sender)
    }
    
}
