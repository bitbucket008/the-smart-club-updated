//
//  SurveyFormMcqCellCollectionViewCell.swift
//  smartclub
//
//  Created by Mac-Admin on 2/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

//MARK: Protocols
protocol SurveyMcqAnswerDelegate : NSObjectProtocol{
    func onSelectedMcqAnswer(index:Int, answer:String, question:String)
}

class SurveyFormMcqCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    //MARK: Properties
    fileprivate let CLASS_NAME = "SurveyFormMcqCell"
    open var mcqAnswerList:[Any]!
    open var index:Int!
    open var question:String!
    open var delegate:SurveyMcqAnswerDelegate?
    
    //MARK: Outlets
    @IBOutlet weak var lblSurveyQuestion: UILabel!
    @IBOutlet weak var cvMcq: UICollectionView!
    
    
    override func layoutSubviews() {
//        super.layoutSubviews()
        cvMcq.delegate = self
        cvMcq.dataSource = self
        
        print(CLASS_NAME+" -- layoutSubviews() -- \(mcqAnswerList)")
        
        //register cells
        let surveyMcqCollectionViewCell = UINib(nibName: "SurveyMcqCollectionViewCell", bundle: nil)
        cvMcq?.register(surveyMcqCollectionViewCell, forCellWithReuseIdentifier: "SurveyMcqCollectionViewCell")
//        cvMcq.reloadData()
    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        cvMcq.delegate = self
//        cvMcq.dataSource = self
//
//        print(CLASS_NAME+" -- awakeFromNib() -- \(mcqAnswerList)")
//
//        //register cells
//        let surveyMcqCollectionViewCell = UINib(nibName: "SurveyMcqCollectionViewCell", bundle: nil)
//        cvMcq?.register(surveyMcqCollectionViewCell, forCellWithReuseIdentifier: "SurveyMcqCollectionViewCell")
////        cvMcq.reloadData()
//    }
    
    //MARK: CollectionView Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 3
        return (mcqAnswerList.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (mcqAnswerList[indexPath.row] as! AnyObject).description.count + 80, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let  cell:SurveyMcqCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SurveyMcqCollectionViewCell", for: indexPath) as! SurveyMcqCollectionViewCell
        
        cell.layer.cornerRadius = 15
        
        cell.index = index 
        cell.question = question
        
        cell.lblAnswer.text = (mcqAnswerList[indexPath.row] as!  AnyObject).value(forKey: "optionName") as? String
        
        print(CLASS_NAME+" -- collectionView() -- cellForItemAt  -- cell label -- \(cell.lblAnswer.text!)")
        
        return cell 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.onSelectedMcqAnswer(index: index, answer: (mcqAnswerList[indexPath.row] as!  AnyObject).value(forKey: "optionName") as! String, question: question)
    }
    
    
}
