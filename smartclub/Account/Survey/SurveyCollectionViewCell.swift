//
//  SurveyCollectionViewCell.swift
//  smartclub
//
//  Created by Admin on 13/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class SurveyCollectionViewCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var lblSurveyTitle: UILabel!
    @IBOutlet weak var lblSurveyDescription: UILabel!
    @IBOutlet weak var lblSP: UILabel!
    
    @IBOutlet weak var lbldescr: UILabel!
    
    override func awakeFromNib() {
        
    }
    
}
