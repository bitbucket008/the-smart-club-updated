//
//  SurveyFormRangeCell.swift
//  smartclub
//
//  Created by Mac-Admin on 2/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TGPControls

//MARK: protocols 
protocol SurveyRangeAnswerDelegate : NSObjectProtocol{
    func onSelectedRangeAnswer(index:Int, answer:String, question:String)
}

class SurveyFormRangeCell: UICollectionViewCell {
    
    //MARK: Properties
    fileprivate let CLASS_NAME = "SurveyFormRangeCell"
    open var rangeList:[Any]!
    fileprivate var rangeValues:[String] = [String]()
    open var index:Int!
    open var question:String!
    open var delegate:SurveyRangeAnswerDelegate?
    
    //MARK: Outlets
    @IBOutlet weak var lblSurveyQuestion: UILabel!
    @IBOutlet weak var labelList: TGPCamelLabels!
    @IBOutlet weak var slider: TGPDiscreteSlider!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        print(CLASS_NAME+" -- awakeFromNib() -- rangelist -- \(rangeList)")
        
//        labelList.names = ["Good", "Better", "Best"]
        
        for i in 0...(rangeList.count-1) {
            print(CLASS_NAME+" -- awakeFromNib() -- rangelist - \(i) -- \(rangeList[i])")
            rangeValues.append((rangeList[i] as!  AnyObject).value(forKey: "optionName") as! String)
        }
        
        labelList.names = rangeValues
        slider.tickCount = labelList.tickCount
        slider.tickStyle = 2
        slider.ticksListener = labelList
//        slider.ticksDistance = 30
        
//        labelList.ticksDistance = 30
        slider.addTarget(self, action: #selector(SurveyFormRangeCell.valueChanged(_:event:)), for: .valueChanged)
        labelList.addTarget(self, action: #selector(SurveyFormRangeCell.valueChangedLabels(_:event:)), for: .valueChanged)
    }
    
    
    //slider value change
    @objc func valueChanged(_ sender: TGPDiscreteSlider, event:UIEvent) {
        print(CLASS_NAME+" -- valueChanged() -- \(sender.value)")
        slider.value = sender.value
        delegate?.onSelectedRangeAnswer(index: index, answer: String(describing: Int(sender.value)), question: question)
    }
    
    @objc func valueChangedLabels(_ sender: TGPCamelLabels, event:UIEvent) {
        print(CLASS_NAME+" -- valueChangedLabels() -- \(sender.value)")
    }
}
