//
//  SurveyFormTextCell.swift
//  smartclub
//
//  Created by Mac-Admin on 2/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

//MARK: Protocols
protocol SurveyTextAnswerDelegate : NSObjectProtocol{
    func onTypeTextAnswer(index:Int, answer:String, question:String)
}

class SurveyFormTextCell: UICollectionViewCell {
    //MARK: Properties
    fileprivate let CLASS_NAME = "SurveyFormTextCell"
    open var index:Int!
    open var question:String!
    open var delegate:SurveyTextAnswerDelegate?
    
    //MARK: Outlets
    @IBOutlet weak var lblSurveyQuestion: UILabel!
    @IBOutlet weak var tfSurveyAnswer: UITextField!

    
    //MARK: ACTIONS
    //on keypress on uitextfield
    @IBAction func onTypeTextAnswer(_ sender: UITextField, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onTypeTextAnswer() -- \(sender.text!)")
        delegate?.onTypeTextAnswer(index: index, answer: sender.text!, question: question)
    }
    
    
}
