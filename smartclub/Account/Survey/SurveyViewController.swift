//
//  SurveyViewController.swift
//  smartclub
//
//  Created by Admin on 13/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SurveyViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //MARK: Properties
    open let CLASS_NAME = "SurveyViewController"
    fileprivate var surveyList:[Survey] = [Survey]()
    
    //MARK: Outlets
    @IBOutlet weak var collectionView:UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // init ui
        initialize()
        
        //get survey list
        getSurveyList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Initilization
    //initialization
    private func initialize() -> Void{
        let nib1 = UINib(nibName: "SurveyCollectionViewCell", bundle: nil)
        collectionView?.register(nib1, forCellWithReuseIdentifier: "surveycollectionviewcell")
        
//        surveyList = [
//                Survey(id:1, title:"Title One", surveyDescription:"Test description one ", sp:500),
//                Survey(id:2, title:"Title Two", surveyDescription:"Test description two", sp:200),
//                Survey(id:3, title:"Title Two", surveyDescription:"Test description two", sp:200),
//                Survey(id:4, title:"Title Two", surveyDescription:"Test description two", sp:200),
//                Survey(id:5, title:"Title Two", surveyDescription:"Test description two", sp:200),
//                Survey(id:6, title:"Title Two", surveyDescription:"Test description two", sp:200),
//            ]
//
//        collectionView.reloadData()
    }
    
    //MARK: Network call
    //get survey list from server
    private func getSurveyList() -> Void {
        //show progress dialog
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_SURVEY_LIST)!,
            method: .get,
            parameters:[ "status":"active"])
            .responseJSON { (response) -> Void in
                
                //end refreshing
//                self.rcOffers.endRefreshing()
                
                //hide progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        
                        if(swiftyJson["data"].arrayValue.count > 0){
                            for i in 0 ..< swiftyJson["data"].arrayValue.count {
                                let id = swiftyJson["data"][i]["id"].intValue
                                let title = swiftyJson["data"][i]["name"].stringValue
                                let surveyDescription = swiftyJson["data"][i]["surveyDescription"].stringValue
                                let descr = swiftyJson["data"][i]["descr"].stringValue
                                let sp = swiftyJson["data"][i]["sp_gain"].intValue

                                self.surveyList.append(Survey(id:id, title:title, surveyDescription: surveyDescription,sp:sp, descr:descr))
                                print(self.CLASS_NAME+" -- getSurveyList()  -- offer  name -- "+(self.surveyList[i].title))
                            }
                            
                            //reload tableview data
                            self.collectionView.reloadData()
                        }
                        
                        //                            SnackBarManager.showSnackBar(message: "Offer list loaded")
                        print(self.CLASS_NAME+" -- getSurveyList()  -- response -- survey list loaded")
                        
                    }else{
                        //                            SnackBarManager.showSnackBar(message: "Request failed")
                        print(self.CLASS_NAME+" -- getSurveyList()  -- response -- Error response")
                    }
                }else{
                    print(self.CLASS_NAME+" -- getSurveyList()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getSurveyList()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //        if section == 0 {
        //            return 1
        //        }
        
        return surveyList.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SurveyCollectionViewCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "surveycollectionviewcell", for: indexPath) as! SurveyCollectionViewCell
      
        cell.layer.cornerRadius = 10
        
        cell.lblSurveyTitle.text = surveyList[indexPath.row].title
        cell.lblSurveyDescription.text = surveyList[indexPath.row].descr
        cell.lblSP.text = String(surveyList[indexPath.row].sp)
        
        return cell;
        
    }
    
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        
        let vc = strBoard.instantiateViewController(withIdentifier: "surveyformvc") as! SurveyFormViewController
        
        vc.surveyId = Optional(surveyList[indexPath.row].id)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: (collectionView.frame.size.width/2)-15 , height: 205);
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10)
        
    }
    

}
