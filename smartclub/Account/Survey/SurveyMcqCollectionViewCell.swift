//
//  SurveyMcqCollectionViewCell.swift
//  smartclub
//
//  Created by Mac-Admin on 3/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SurveyMcqCollectionViewCell: UICollectionViewCell {
    
    //MARK: Properties
    fileprivate var CLASS_NAME = "SurveyMcqCollectionViewCell"
    fileprivate var isChecked = false
    open var index:Int!
    open var question:String!
    open var delegate:SurveyMcqAnswerDelegate?
    
    //MARK: Outlets
    @IBOutlet weak var uivAnswerContainer: UIView!
    @IBOutlet weak var lblAnswer: UILabel!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    //check selection
    override var isSelected: Bool{
        didSet{
            if self.isSelected {
                print(CLASS_NAME+" -- isChecked : \(isChecked)")
                self.uivAnswerContainer.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x9AD2D2, alpha: 1.0)
                self.lblAnswer.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0xFFFFFF, alpha: 1.0)
                isChecked = true
            }else{
                    self.uivAnswerContainer.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0xEBEBEB, alpha: 1.0)
                    self.lblAnswer.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x273D8E, alpha: 1.0)
                    isChecked = false
                }
                
            }
        
    }
    
}
