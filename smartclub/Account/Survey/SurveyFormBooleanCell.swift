//
//  SurveyFormBoolCell.swift
//  smartclub
//
//  Created by Mac-Admin on 3/17/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

//MARK: Protocols
protocol SurveyBooleanAnswerDelegate : NSObjectProtocol{
    func onSelectedBooleanAnswer(index:Int, answer:String, question:String)
}

class SurveyFormBooleanCell : UICollectionViewCell {
    //MARK: Properties
    fileprivate var CLASS_NAME = "SurveyFormBooleanCell"
    open var value = ""
    open var index:Int!
    open var question:String!
    open var delegate:SurveyBooleanAnswerDelegate?
    
    //MARK: Outlets
    @IBOutlet weak var lblSurveyQuestion: UILabel!
    @IBOutlet weak var radioButtonYes: UIButton!
    @IBOutlet weak var radioButtonNo: UIButton!
    @IBOutlet weak var lblYes: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    
    override func awakeFromNib() {
        radioButtonYes.layer.cornerRadius = radioButtonYes.frame.height/2
        radioButtonNo.layer.cornerRadius = radioButtonNo.frame.height/2
    }
    
    
    //MARK: Actions
    //WHen click on yes button
    @IBAction func onClickYesButton(_ sender: UIButton, forEvent event: UIEvent) {
        radioButtonYes.setTitle(".", for: .normal)
        radioButtonNo.setTitle("", for: .normal)
        value = "yes"
        delegate?.onSelectedBooleanAnswer(index: index, answer: value, question: question)
    }
    
    //when click on no buton
    @IBAction func onClickNoButton(_ sender: UIButton, forEvent event: UIEvent) {
        radioButtonYes.setTitle("", for: .normal)
        radioButtonNo.setTitle(".", for: .normal)
        value = "no"
        delegate?.onSelectedBooleanAnswer(index: index, answer: value, question: question)
    }
    
}
