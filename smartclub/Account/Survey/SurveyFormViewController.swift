//
//  SurveyFormViewController.swift
//  smartclub
//
//  Created by Admin on 13/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SurveyFormViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: Properties
    fileprivate let CLASS_NAME = "SurveyFormViewController"
    fileprivate let localStorage = LocalStorage()
    open var surveyId:Int?
    fileprivate var questionRecord:[AnyObject] = [AnyObject]()
    fileprivate var answerRecord:NSMutableDictionary = NSMutableDictionary()

    
    //MARK: Outlet
    @IBOutlet weak var cvSurveyForm: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //initialization
        initialize()
        
        //dismiss keyboarrd
        dissmisKeyboard()
        
        
        // get survey data by id
        if let sId = surveyId{
            getSurveyById(surveyId: sId)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Init ui
    private func initialize() -> Void {
        //init collectionview
        cvSurveyForm.delegate = self
        cvSurveyForm.dataSource = self
        
        //register cells
        let surveyFormRangeCell = UINib(nibName: "SurveyFormRangeCell", bundle: nil)
        cvSurveyForm?.register(surveyFormRangeCell, forCellWithReuseIdentifier: "SurveyFormRangeCell")
        
        let surveyFormTextCell = UINib(nibName: "SurveyFormTextCell", bundle: nil)
        cvSurveyForm?.register(surveyFormTextCell, forCellWithReuseIdentifier: "SurveyFormTextCell")
        
        let surveyFormMcqCell = UINib(nibName: "SurveyFormMcqCell", bundle: nil)
        cvSurveyForm?.register(surveyFormMcqCell, forCellWithReuseIdentifier: "SurveyFormMcqCell")
        
        let surveyFormBooleanCell = UINib(nibName: "SurveyFormBooleanCell", bundle: nil)
        cvSurveyForm?.register(surveyFormBooleanCell, forCellWithReuseIdentifier: "SurveyFormBooleanCell")
        
        let surveyFormButtonCell = UINib(nibName: "SurveyFormButtonCell", bundle: nil)
        cvSurveyForm?.register(surveyFormButtonCell, forCellWithReuseIdentifier: "SurveyFormButtonCell")
    }
    
    
    //MARK: Actions
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitBtnAction(sender:UIButton) {
        self.showCongrssPointsPopUp()
    }
    
    func showCongrssPointsPopUp() {
        let  dashboard = UIStoryboard.init(name: "dashboard", bundle: nil)
        let vc = dashboard.instantiateViewController(withIdentifier: "congrasspop") as! CongrassViewController
        
        self.present(vc, animated: false, completion: nil)
    }
    
    //MARK: Network call
    //get survey list from server
    private func getSurveyById(surveyId:Int) -> Void {
        //show progress dialog
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_SURVEY_LIST+"/"+String(surveyId))!,
            method: .get,
            parameters:[ "status":"active"])
            .responseJSON { (response) -> Void in
                
                //end refreshing
                // self.rcOffers.endRefreshing()
                
                //hide progress dialog
                SVProgressHUD.dismiss()
                
                //parse json
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        
                        if(swiftyJson["data"]["question_record"].arrayValue.count > 0){
                            for i in 0 ..< swiftyJson["data"]["question_record"].arrayValue.count {
                                let type = swiftyJson["data"]["question_record"][i]["type"].stringValue
                                
                                //if question type is text
                                if type == "text" {
                                    let surveyQuestionText = SurveyQuestionText()
                                    surveyQuestionText.index = Optional(swiftyJson["data"]["question_record"][i]["index"].intValue)
                                    surveyQuestionText.type = Optional(swiftyJson["data"]["question_record"][i]["type"].stringValue)
                                    surveyQuestionText.title = Optional(swiftyJson["data"]["question_record"][i]["title"].stringValue)
                                    self.questionRecord.append(surveyQuestionText)
                                    print(self.CLASS_NAME+" -- getSurveyById()  -- survey description for text -- "+((self.questionRecord[i] as! SurveyQuestionText).title)!)
                                }
                                
                                //if question type is mcq
                                 if type == "mcq" {
                                    let surveyQuestionMcq = SurveyQuestionMcq()
                                    surveyQuestionMcq.index = swiftyJson["data"]["question_record"][i]["index"].int
                                    surveyQuestionMcq.title = swiftyJson["data"]["question_record"][i]["title"].stringValue
                                    surveyQuestionMcq.type = swiftyJson["data"]["question_record"][i]["type"].stringValue
                                    surveyQuestionMcq.optionNames = swiftyJson["data"]["question_record"][i]["options"].arrayObject
                                    
                                    self.questionRecord.append(surveyQuestionMcq)
                                    
//                                    print(self.CLASS_NAME+" -- getSurveyById()  -- survey description for mcq-- "+((surveyQuestionMcq.optionNames?[0] as AnyObject)["optionName"] as! String))
                                    
                                    print(self.CLASS_NAME+" -- getSurveyById()  -- survey description for mcq-- \(surveyQuestionMcq.optionNames)")
                                }
                                
                                
                                //if question type is mcq
                                if type == "range" {
                                    let surveyQuestionRange = SurveyQuestionRange()
                                    surveyQuestionRange.index = swiftyJson["data"]["question_record"][i]["index"].int
                                    surveyQuestionRange.title = swiftyJson["data"]["question_record"][i]["title"].stringValue
                                    surveyQuestionRange.type = swiftyJson["data"]["question_record"][i]["type"].stringValue
                                    surveyQuestionRange.optionNames = swiftyJson["data"]["question_record"][i]["options"].arrayObject
                                    
                                    self.questionRecord.append(surveyQuestionRange)
                                    
                                    //                                    print(self.CLASS_NAME+" -- getSurveyById()  -- survey description for mcq-- "+((surveyQuestionMcq.optionNames?[0] as AnyObject)["optionName"] as! String))
                                    
                                    print(self.CLASS_NAME+" -- getSurveyById()  -- survey description for mcq-- \(surveyQuestionRange.optionNames)")
                                }
                                
                                
                                //if question type is bool
                                if type == "bool" {
                                    let surveyQuestionBoolean = SurveyQuestionBoolean()
                                    surveyQuestionBoolean.index = swiftyJson["data"]["question_record"][i]["index"].int
                                    surveyQuestionBoolean.title = swiftyJson["data"]["question_record"][i]["title"].stringValue
                                    surveyQuestionBoolean.type = swiftyJson["data"]["question_record"][i]["type"].stringValue
                                    surveyQuestionBoolean.optionNames = swiftyJson["data"]["question_record"][i]["options"].arrayObject
                                    
                                    self.questionRecord.append(surveyQuestionBoolean)
                                    
                                    //                                    print(self.CLASS_NAME+" -- getSurveyById()  -- survey description for mcq-- "+((surveyQuestionMcq.optionNames?[0] as AnyObject)["optionName"] as! String))
                                    
                                    print(self.CLASS_NAME+" -- getSurveyById()  -- survey description for mcq-- \(surveyQuestionBoolean.optionNames)")
                                }
                                
//                                self.surveyList.append(Survey(id:id, title:title, surveyDescription:surveyDescription, sp:sp))
//                                print(self.CLASS_NAME+" -- getSurveyList()  -- offer  name -- "+(self.questionRecord[i].title))
                            }

                            self.questionRecord.append("Submit" as AnyObject)
                            
//                            print(self.CLASS_NAME+" -- getSurveyById()  -- survey questionRecord -- \((self.questionRecord[0] as! SurveyQuestionMcq).optionNames)")
                            
                            //reload tableview data
                            self.cvSurveyForm.reloadData()
//                         self.answerRecord.reserveCapacity(self.questionRecord.count)
                        }
                        
                        //                            SnackBarManager.showSnackBar(message: "Offer list loaded")
                        print(self.CLASS_NAME+" -- getSurveyList()  -- response -- survey list loaded")
                        
                    }else{
                        //                            SnackBarManager.showSnackBar(message: "Request failed")
                        print(self.CLASS_NAME+" -- getSurveyList()  -- response -- Error response")
                    }
                }else{
                    print(self.CLASS_NAME+" -- getSurveyList()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getSurveyList()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //store survey answer to server
    fileprivate func storeSurveyAnswerToServer(surveyResult:NSMutableDictionary) -> Void {
        //show progressbar
        SVProgressHUD.show()
        var jsonString:String!
        print(CLASS_NAME+" -- storeSurveyAnswerToServer() -- \(surveyResult)")
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: surveyResult, options:JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            
            print(CLASS_NAME+" -- storeSurveyAnswerToServer() -- json data --  \(jsonString!)")
           
            }catch {
                print(self.CLASS_NAME+" -- storeSurveyAnswerToServer() -- json error --"+error.localizedDescription)
            }

        
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_STORE_SURVEY_RESULTS)!,
            method: .post,
            parameters: ["survey_id" : surveyId!, "customer_id" : localStorage.getUser().getId()!, "submit_record" : jsonString], encoding: JSONEncoding.default) 

            .responseJSON { (response) -> Void in
                //hide progrees dialog
                SVProgressHUD.dismiss()

                //go to  login view controller
                let swiftyJson = JSON(response.data!)
                if  swiftyJson["success"].intValue == 1{
                    print(self.CLASS_NAME+" -- storeSurveyAnswerToServer()  -- message  -- "+(swiftyJson["message"].stringValue))
                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)

                    //parse json data
//                    if(swiftyJson["data"].arrayValue.count > 0){
//                        for i in 0 ..< swiftyJson["data"].arrayValue.count {
//                            let id = swiftyJson["data"][i]["id"].string
//                            let name = swiftyJson["data"][i]["name"].string
//                            let details = swiftyJson["data"][i]["details"].string
//                            let status = swiftyJson["data"][i]["status"].int
//                            self.companyList.append(Company(id:id, name:name, details:details, status:status))
//                            print(self.CLASS_NAME+" -- getCompanyList()  -- company  name -- "+(self.companyList[i].name!))
//                        }
//
//                        //set company list loaded to true
//                        self.isCompanyListLoaded = true
//                    }
                }else{
                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                }

                print(self.CLASS_NAME+" -- storeSurveyAnswerToServer()  -- response -- "+(String(describing: response)))

        }
    }
    
    
    //MARK: CollectionView method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return questionRecord.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cell =  collectionView.cellForItem(at: indexPath)
        
        //if cell is mcq type
        if cell is SurveyFormMcqCell {
            return CGSize(width: collectionView.frame.size.width, height: 180);
        }
        
        //if cell is range type
        else if cell is SurveyFormRangeCell {
            return CGSize(width: collectionView.frame.size.width, height: 180);
        }
        
            //if cell is button type
        else if cell is SurveyFormButtonCell {
            return CGSize(width: collectionView.frame.width, height: 150);
        }
        
        return CGSize(width: collectionView.frame.size.width, height: 150);
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //if cell mcq
        if questionRecord[indexPath.row] is SurveyQuestionMcq {
            let cell: SurveyFormMcqCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "SurveyFormMcqCell", for: indexPath) as! SurveyFormMcqCell
        
            print(CLASS_NAME+" -- mcq -- \((self.questionRecord[indexPath.row] as! SurveyQuestionMcq).optionNames!)")
            
            cell.delegate = self
            cell.index = (self.questionRecord[indexPath.row] as! SurveyQuestionMcq).index!
            cell.question = (self.questionRecord[indexPath.row] as! SurveyQuestionMcq).title!
            cell.mcqAnswerList = ((self.questionRecord[indexPath.row] as! SurveyQuestionMcq).optionNames as! [Any])
            
            cell.lblSurveyQuestion.text = String(indexPath.row+1)+". "+(self.questionRecord[indexPath.row] as! SurveyQuestionMcq).title!
            
            return cell
        }
        
        //if cell is range
        else if questionRecord[indexPath.row] is SurveyQuestionRange {
            let cell: SurveyFormRangeCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "SurveyFormRangeCell", for: indexPath) as! SurveyFormRangeCell
            
            print(CLASS_NAME+" -- range -- \((self.questionRecord[indexPath.row] as! SurveyQuestionRange).optionNames!)")
            
            cell.delegate = self
            cell.index = (self.questionRecord[indexPath.row] as! SurveyQuestionRange).index!
            cell.question = (self.questionRecord[indexPath.row] as! SurveyQuestionRange).title!
            cell.rangeList = ((self.questionRecord[indexPath.row] as! SurveyQuestionRange).optionNames as! [Any])
            
            //            cell.lblSurveyQuestion.text = String(indexPath.row+1)+". "+((questionRecord[indexPath.row] as AnyObject)["title"] as! String)
            cell.lblSurveyQuestion.text = String(indexPath.row+1)+". "+(self.questionRecord[indexPath.row] as! SurveyQuestionRange).title!
            
//            cell.layoutSubviews()
            
            return cell
        }
        
            //if the question text type
        else if questionRecord[indexPath.row] is SurveyQuestionText{
            let cell:SurveyFormTextCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "SurveyFormTextCell", for: indexPath) as! SurveyFormTextCell
            
//            cell.lblSurveyQuestion.text = ((questionRecord[indexPath.row] as AnyObject)["title"] as! String)
            
            cell.delegate = self
            cell.index = (self.questionRecord[indexPath.row] as! SurveyQuestionText).index!
            cell.question = (self.questionRecord[indexPath.row] as! SurveyQuestionText).title!
            cell.lblSurveyQuestion.text = String(indexPath.row+1)+". "+(self.questionRecord[indexPath.row] as! SurveyQuestionText).title!
            
            return cell
        }
        
        
            //if the question text type
        else if questionRecord[indexPath.row] is SurveyQuestionBoolean{
            let cell:SurveyFormBooleanCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "SurveyFormBooleanCell", for: indexPath) as! SurveyFormBooleanCell
            
            //            cell.lblSurveyQuestion.text = ((questionRecord[indexPath.row] as AnyObject)["title"] as! String)
            
            cell.delegate = self
            cell.index = (self.questionRecord[indexPath.row] as! SurveyQuestionBoolean).index!
            cell.question = (self.questionRecord[indexPath.row] as! SurveyQuestionBoolean).title!
            cell.lblSurveyQuestion.text = String(indexPath.row+1)+". "+(self.questionRecord[indexPath.row] as! SurveyQuestionBoolean).title!
            
            return cell
        }
        //if cell is submit button type
        else {
            let cell: SurveyFormButtonCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "SurveyFormButtonCell", for: indexPath) as! SurveyFormButtonCell
//            cell.backgroundColor = UIColor.blue
            
            cell.delegate = self
            
            return cell
        }
    }
    
    //MARK: Keyboard
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }
    
}


//MARK: Delegation
extension SurveyFormViewController : SurveySubmitDelegate, SurveyTextAnswerDelegate, SurveyMcqAnswerDelegate, SurveyRangeAnswerDelegate, SurveyBooleanAnswerDelegate{
    
    //SurveyTextAnswerDelegate
    func onTypeTextAnswer(index: Int, answer: String, question: String) {
        print(CLASS_NAME+" -- onTypeTextAnswer() -- \(index) \(question+" : "+answer)")
        
        var textAnswer = NSMutableDictionary()
        textAnswer.setValue(String(index), forKey: "id")
        textAnswer.setValue(question, forKey: "question")
        textAnswer.setValue(answer, forKey: "answer")
        
//        for i in 0...answerRecord.count {
//            answerRecord.updateValue(["":""], forKey: i)
//        }
        
//        answerRecord.updateValue(textAnswer, forKey: index)
        answerRecord.setValue(textAnswer, forKey: String(index))
    }
    
    //SurveyMcqAnswerDelegate
    func onSelectedMcqAnswer(index: Int, answer: String, question: String) {
        print(CLASS_NAME+" -- onSelectedMcqAnswer() -- \(index) \(" : "+question+" : "+answer)")
        
        var mcqAnswer = NSMutableDictionary()
        mcqAnswer.setValue(String(index), forKey: "id")
        mcqAnswer.setValue(question, forKey: "question")
        mcqAnswer.setValue(answer, forKey: "answer")
        
        //        for i in 0...answerRecord.count {
        //            answerRecord.updateValue(["":""], forKey: i)
        //        }
        
        //        answerRecord.updateValue(textAnswer, forKey: index)
        answerRecord.setValue(mcqAnswer, forKey: String(index))
    }
    
    //SurveyRangeAnswerDelegate
    func onSelectedRangeAnswer(index: Int, answer: String, question: String) {
        print(CLASS_NAME+" -- onSelectedRangeAnswer() -- \(index) \(" : "+question+" : "+answer)")
        
        var rangeAnswer = NSMutableDictionary()
        rangeAnswer.setValue(String(index), forKey: "id")
        rangeAnswer.setValue(question, forKey: "question")
        rangeAnswer.setValue(answer, forKey: "answer")
        
        //        for i in 0...answerRecord.count {
        //            answerRecord.updateValue(["":""], forKey: i)
        //        }
        
        //        answerRecord.updateValue(textAnswer, forKey: index)
        answerRecord.setValue(rangeAnswer, forKey: String(index))
        
    }
    
    //SurveyBooleanAnswerDelegate
    func onSelectedBooleanAnswer(index: Int, answer: String, question: String) {
        print(CLASS_NAME+" -- onSelectedBooleanAnswer() -- \(index) \(" : "+question+" : "+answer)")
        
        var booleanAnswer = NSMutableDictionary()
        booleanAnswer.setValue(String(index), forKey: "id")
        booleanAnswer.setValue(question, forKey: "question")
        booleanAnswer.setValue(answer, forKey: "answer")
        
        //        for i in 0...answerRecord.count {
        //            answerRecord.updateValue(["":""], forKey: i)
        //        }
        
        //        answerRecord.updateValue(textAnswer, forKey: index)
        answerRecord.setValue(booleanAnswer, forKey: String(index))
        
    }
    
    //SurveySubmitDelegate
    func onSubmitSurvey(sender: UIBorderButton) {
//        answerRecord =  answerRecord.filter{
//            $0.value != (["":""])
//        }
        
        print(CLASS_NAME+" -- onSubmitSurvey() -- count - \(answerRecord.count) -- record : \(answerRecord)")
        
        if answerRecord.count > 0 {
            storeSurveyAnswerToServer(surveyResult: answerRecord)
        }else{
            print(CLASS_NAME+" -- onSubmitSurvey() -- no answer listed....")
        }
    }
}
