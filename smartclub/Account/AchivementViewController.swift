//
//  AchivementViewController.swift
//  smartclub
//
//  Created by Admin on 13/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class AchivementViewController: UIViewController , UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var collectionView:UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let nib1 = UINib(nibName: "AchivementCollectionViewCell", bundle: nil)
        collectionView?.register(nib1, forCellWithReuseIdentifier: "achivementcollectionviewcell")
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //        if section == 0 {
        //            return 1
        //        }
        
        return 15
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell: AchivementCollectionViewCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "achivementcollectionviewcell", for: indexPath) as! AchivementCollectionViewCell
        
        
        return cell;
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        let vendorDeatilStoryboard = UIStoryboard.init(name: "vendordetails", bundle: nil)
        //
        //
        //        let vc = vendorDeatilStoryboard.instantiateViewController(withIdentifier: "vendordetailvc") as! VendorDetailTableViewController
        //
        //
        //        self.present(vc, animated: true, completion: nil)
        //
        //
        
        
        
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        
        return CGSize(width: (collectionView.frame.size.width/2)-30 , height: 170);
    }
    
    
    
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
        return UIEdgeInsetsMake(20, 20, 20, 20)
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
