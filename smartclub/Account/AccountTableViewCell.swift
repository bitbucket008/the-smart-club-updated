//
//  AccountTableViewCell.swift
//  smartclub
//
//  Created by Admin on 11/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet weak var titleLabel:UILabel!
    
    override func layoutSubviews() {
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(-7, 0, -7, 0))
        
    }
    

}
