//
//  FlowSocialViewController.swift
//  smartclub
//
//  Created by Admin on 13/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookLogin
import FacebookCore

class FlowSocialViewController: UIViewController {
    
    //MARK: Properties
    fileprivate let CLASS_NAME = "FlowSocialViewController"
    
    //MARK: Outlets
    @IBOutlet weak var ivInstagram: UIImageView!
    @IBOutlet weak var ivTwitter: UIImageView!
    @IBOutlet weak var ivFacebook: UIImageView!
    
    
    var unTappedIcon = ["follow_ins","follow_tr","follow_fc"]
    var tappedIcon = ["follow_ins_tapped","follow_tr_tapped","follow_fc_tapped"]
    
    var previousIndex:Int!
    var currentIndex:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(CLASS_NAME+" -- viewDidLoad()")
        
        // Do any additional setup after loading the view.
        //        let tapGestureRecognizer = UITapGestureRecognizer()
        //        tapGestureRecognizer.addTarget(self, action: #selector(onClickFacebook))
        ////        ivFacebook.isUserInteractionEnabled = true
        //        ivFacebook.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Actions
    //    @objc func onClickFacebook() -> Void {
    //        print(CLASS_NAME+" -- onClickFacebook()")
    //
    //        //show progress dialog
    //        SVProgressHUD.show()
    //
    ////        if let token = FBSDKAccessToken.current() {
    ////             print(CLASS_NAME+" -- onClickFacebook() -- access token - "+token.appID)
    ////        }else{
    ////            print(CLASS_NAME+" -- onClickFacebook() -- access token nil ")
    ////            return
    ////        }
    //
    //        let url = "https://graph.facebook.com/1118347154894163/likes/DailyProthomAlo"
    //
    //        SVProgressHUD.show()
    //
    //        Alamofire.request(
    //            URL(string: url)!,
    //            method: .get,
    //            parameters:["access_token": "1968947126755100"], encoding: JSONEncoding.default)
    //            .responseJSON { (response) -> Void in
    //
    //                //dismiss progress dialog
    //                SVProgressHUD.dismiss()
    //
    //                //parse json
    //                if response.result.isSuccess{
    //                    let swiftyJson =  JSON(response.data!)
    //                    if  swiftyJson["success"].intValue == 1{
    //                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
    //                        print(self.CLASS_NAME+" -- getSpBuyList()  -- response -- SP list loaded")
    //                    }else{
    //                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
    //                        print(self.CLASS_NAME+" -- getSpBuyList()  -- response -- Request failed")
    //                    }
    //
    //                }else{
    //                    print(self.CLASS_NAME+" -- getSpBuyList()  -- response -- request failed")
    //                    SnackBarManager.showSnackBar(message: "Request failed")
    //                }
    //
    //                print(self.CLASS_NAME+" -- onClickFacebook()  -- response -- "+(String(describing: response)))
    //
    //        }
    //
    //
    ////        let connection = GraphRequestConnection()
    ////        connection.add(MyProfileRequest()) { response, result in
    ////            switch result {
    ////            case .success(let response):
    ////                print("Custom Graph Request Succeeded: \(response)")
    ////            case .failed(let error):
    ////                print("Custom Graph Request Failed: \(error)")
    ////            }
    ////        }
    ////        connection.start()
    //
    //    }
    
    
    
    @IBAction func onTapInstagram(_ sender: UITapGestureRecognizer) {
        print("I am instagram")
        
        self.ivInstagram.image = UIImage.init(named: "follow_ins_tapped")
        self.ivTwitter.image = UIImage.init(named: "follow_tr")
        self.ivFacebook.image = UIImage.init(named: "follow_fc")
        
        let storyBoard  = UIStoryboard.init(name: "account", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InstagramWebViewController") as! InstagramWebViewController
        self.present(vc, animated:true, completion:nil)
        
    }
    
    
    //    @IBAction func onTapTwitter(_ sender: UITapGestureRecognizer) {
    //        print("I am twitter")
    //
    //        self.ivTwitter.image = UIImage.init(named: "follow_tr_tapped")
    //        self.ivInstagram.image = UIImage.init(named: "follow_ins")
    //        self.ivFacebook.image = UIImage.init(named: "follow_fc")
    //
    //        let storyBoard = UIStoryboard.init(name: "account", bundle: nil)
    //        let vc = storyBoard.instantiateViewController(withIdentifier: "TwitterWebViewController") as! TwitterWebViewController
    //        self.present(vc, animated: true, completion: nil)
    //    }
    
    
    @IBAction func onTapFacebook(_ sender: UITapGestureRecognizer) {
        print("I am facebook")
        self.ivFacebook.image = UIImage.init(named: "follow_fc_tapped")
        self.ivInstagram.image = UIImage.init(named: "follow_ins")
        self.ivTwitter.image = UIImage.init(named: "follow_tr")
        
        let storyBoard = UIStoryboard.init(name: "account", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "FacebookWebViewController") as! FacebookWebViewController
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
}

//struct MyProfileRequest: GraphRequestProtocol {
//    struct Response: GraphResponseProtocol {
//        init(rawResponse: Any?) {
//            // Decode JSON from rawResponse into other properties here.
//        }
//    }
//
//    var graphPath = "/me"
//    var parameters: [String : Any]? = ["fields": "id, name"]
//    var accessToken = AccessToken.current
//    var httpMethod: GraphRequestHTTPMethod = .GET
//    var apiVersion: GraphAPIVersion = .defaultVersion
//}
