//
//  UpdateProfileViewController.swift
//  smartclub
//
//  Created by RASHED on 1/17/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import DropDown

class UpdateProfileViewController: UIViewController {
    
    //MARK: Properties
    fileprivate let CLASS_NAME:String = "LoginViewController"
    let localStorage = LocalStorage()
    fileprivate var profileScore = 20
    private var fullname:String = ""
    private var gender:String = ""
    private var nationality:String = ""
    private var phoneNumber:String = ""
    private var birthday:String = ""
    private var maritalStatus:String = ""
    private var anniverseryDay:String = ""
    fileprivate var countryList: [String] = [String]()
//  fileprivate var isCountryListLoaded = false
    fileprivate var selectedCountry:String!
    fileprivate var selectedMaritalStatus:String!
    
    //MARK: Outlets
    @IBOutlet var ScrollView: UIScrollView!
    @IBOutlet weak var tfFullname: UITextField!
    @IBOutlet weak var btnGendermale: UIBorderButton!
    @IBOutlet weak var btnGenderFemale: UIBorderButton!
    @IBOutlet weak var tfNationality: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfBirthday: UITextField!
    @IBOutlet weak var tfMaritalStatus: UITextField!
    @IBOutlet weak var tfAnniverseryDay: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var progressViewPartOne: UIView!
    @IBOutlet weak var progressViewPartTwo: UIView!
    @IBOutlet weak var progressViewPartThree: UIView!
    @IBOutlet weak var progressViewPartFour: UIView!
    @IBOutlet weak var ivProgressPartOne: UIImageView!
    @IBOutlet weak var ivProgressPartTwo: UIImageView!
    @IBOutlet weak var ivProgressPartThree: UIImageView!
    @IBOutlet weak var ivProgressPartFour: UIImageView!
    
    @IBOutlet weak var pvPartTwoContainer: UIView!
    @IBOutlet weak var pvPartThreeContainer: UIView!
    @IBOutlet weak var pvPartFourContainer: UIView!
    
    
    //MARK UI Objects
    fileprivate var datePickerView:UIDatePicker!
    fileprivate let countryListDropDown = DropDown()
    fileprivate let maritalStatusDropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //initializataion
        initialization()
        
        ScrollView.contentSize.height = 1000
        
        //get profile data
        getProfileInformation()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Instance method
    //initialization
    private func initialization(){
        tfFullname!.text = localStorage.getUser().getUsername()
//        self.view.frame.origin.y = 0
        tfNationality.delegate = self
        
        //load country list
        loadCountryList()
    }
    
    
    private func loadCountryList() -> Void {
        countryList.removeAll()
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            
            if !name.elementsEqual("Israel"){
                countryList.append(name)
                print(CLASS_NAME+" -- country name - \(name)")
            }
        }
        
        //sort country list
        countryList.sort() 
        
        print(CLASS_NAME+" -- country list - \(countryList.count) -- \(countryList)")
        
        
        // The view to which the drop down will appear on
        countryListDropDown.anchorView = tfNationality // UIView or UIBarButtonItem
//        countryListDropDown.frame.origin.y = tfNationality.frame.origin.y + -50
        countryListDropDown.direction = .bottom
        countryListDropDown.bounds = CGRect(x: tfNationality.bounds.minX, y: tfNationality.bounds.minY-33, width: tfNationality.frame.width, height: view.frame.height/2)
        DropDown.appearance().backgroundColor = .white
        DropDown.appearance().tintColor = UIColor.gray
        
//       populateCountryListToDropdown(text: "")
        
    }
    
    //load marital status dropdown
    private func loadMaritialStatusDropdown() -> Void {
        let maritalStatuslist = ["Married", "Unmarried", "Divorced"]
        maritalStatusDropDown.anchorView = tfMaritalStatus // UIView or UIBarButtonItem
        maritalStatusDropDown.frame.origin.y = tfMaritalStatus.frame.origin.y
        maritalStatusDropDown.dataSource = maritalStatuslist
        
        DropDown.appearance().backgroundColor = .white
        DropDown.appearance().tintColor = UIColor.gray
        
        // Action triggered on selection
        self.maritalStatusDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.tfMaritalStatus.text = item
            self.selectedMaritalStatus = maritalStatuslist[index]
            self.maritalStatusDropDown.hide()
        }
        
        maritalStatusDropDown.show()
    }
    
    
    //Populate company list to dropdown
    private func populateCountryListToDropdown() -> Void {
            self.countryListDropDown.dataSource = countryList
            // Action triggered on selection
            self.countryListDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.tfNationality.text = item
                self.selectedCountry = self.countryList[index]
                self.countryListDropDown.hide()
            }
            self.countryListDropDown.show()
    }

    private func searchCountryListToDropdown(text:String) -> Void {
        var searchCountryList:[String] = [String]()
        for i in 0..<countryList.count {
            if countryList[i].lowercased().contains(text.lowercased()){
                searchCountryList.append(countryList[i].lowercased())
            }
        }
        
        
        self.countryListDropDown.dataSource = searchCountryList
        // Action triggered on selection
        self.countryListDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.tfNationality.text = item
            self.selectedCountry = self.countryList[index]
            self.countryListDropDown.hide()
        }
        self.countryListDropDown.show()
    }
    
    
    @IBAction func onTouchBirthdayField(_ sender: UITextField, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onTouchBirthdayField() -- \(sender)")
    }
    
    
    @IBAction func onTouchDateField(_ sender:UITextField) {
        print(CLASS_NAME+" -- onTouchDateField() -- \(sender)")
        
        datePickerView = UIDatePicker()
        
        if sender.tag == 101 {
            datePickerView.datePickerMode = .date
        }
        
        if sender.tag == 102 {
            datePickerView.datePickerMode = .date
        }
        
        sender.inputView = datePickerView
        datePickerView.tag = sender.tag
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        
        if sender.tag == tfBirthday.tag {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            tfBirthday.text = dateFormatter.string(from: sender.date)
            tfBirthday.resignFirstResponder()
        }
        
        if sender.tag == tfAnniverseryDay.tag {
            dateFormatter.dateFormat = "dd-MM"
            tfAnniverseryDay.text = dateFormatter.string(from: sender.date)
            tfAnniverseryDay.resignFirstResponder()
        }
    }
    
    // MARK: - Action
    //when click on back button
    @IBAction func backButtonToProfileViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //when tap nationality
    @IBAction func onTouchNationalityField(_ sender: UITextField, forEvent event: UIEvent) {
        
        print(CLASS_NAME+" -- onTouchNationalityField() -- text : \(sender.text)")
       
//       sender.resignFirstResponder()
        populateCountryListToDropdown()
    }
    
    //on key pressing nationality field
    @IBAction func onKeyPressNationalityField(_ sender: UITextField, forEvent event: UIEvent) {
        searchCountryListToDropdown(text: sender.text!.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    
    //when tap maital status field
    @IBAction func onTouchMaritalStatusField(_ sender: UITextField, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onTouchMaritalStatusField() -- text : \(sender.text)")
        
        sender.resignFirstResponder()
        loadMaritialStatusDropdown()
    }
    
    
    //when click on male button
    @IBAction func onClickGenderMaleButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        //changes on btnGenderMale
        btnGendermale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
        btnGendermale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
        btnGendermale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
        
        //changes on btnGenderFemale
        btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
        btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
        btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
        
        //set gender value
        gender = "male"
    }
    
    //when click on female button
    @IBAction func onCLickGenderFemaleButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        //changes on btnGenderFemale
        btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
        btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
        btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
        
        //changes on btnGenderMale
        btnGendermale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
        btnGendermale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
        btnGendermale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
        
        //set gender value
        gender = "female"
    }
    
    //when click on update button
    @IBAction func onClickUpdateButton(_ sender: UIButton, forEvent event: UIEvent) {
        //get data
        if let text = tfFullname!.text {
            fullname = text
        }
        
        if let text = tfNationality!.text {
            nationality = text
        }
        
        if let text = tfPhoneNumber!.text {
            phoneNumber = text
        }
        
        if let text = tfBirthday!.text {
            birthday = text
        }
        
        if let text = tfMaritalStatus!.text {
            maritalStatus = text
        }
        
        if let text = tfAnniverseryDay!.text {
            anniverseryDay = text
        }
        
        //send data to server
        updateProfileInformation()
    
    }
    
    
    //MARK: Network Call
    //get profile information
    private func getProfileInformation() -> Void {
        SVProgressHUD.show()
        
        print("Profile update url - " + ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)!,
            method: .get,
            parameters: ["platform":"mobile"])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        self.fullname = swiftyJson["data"]["full_name"].stringValue
                        self.gender = swiftyJson["data"]["gender"].stringValue
                        self.nationality = swiftyJson["data"]["nationality"].stringValue
                        self.birthday = swiftyJson["data"]["birth_date"].stringValue
                        self.anniverseryDay = swiftyJson["data"]["anniversary_date"].stringValue
                        self.phoneNumber = swiftyJson["data"]["phone"].stringValue
                        self.maritalStatus = swiftyJson["data"]["marital_status"].stringValue
                        
                        //calculate profile scvore and polpulate ui
                        self.calculateProfileScore()
                        self.populateUI()
                        //self.showProfileCompleteProgressBar()
                        
                    }else{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- request failed")
//                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //update profile information
    private func updateProfileInformation() -> Void {
        SVProgressHUD.show()
        
        print("Profile update url - " + ApiEndpoints.URI_UPDATE_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_UPDATE_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)!,
            method: .put,
            parameters: ["full_name": fullname, "gender": gender, "nationality": nationality, "phone": phoneNumber, "birth_date": birthday, "marital_status": maritalStatus, "anniversary_date": anniverseryDay, "age": "25"], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                        let swiftyJson = JSON(response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)

                            print(self.CLASS_NAME+" localstorage - "+self.localStorage.getUser().username!)
                           
                        }else{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        }
                    
                }else{
                    print(self.CLASS_NAME+" -- updateProfileInformation()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- updateProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    //MARK: CAlculation
    private func calculateProfileScore() -> Void {
        if fullname != "" {
            profileScore = profileScore + 10
        }
        if nationality != "" {
            profileScore = profileScore + 10
        }
        if gender != "" {
            profileScore = profileScore + 10
        }
        if phoneNumber != "" {
            profileScore = profileScore + 10
        }
        if birthday != "" {
            profileScore = profileScore + 10
        }
        if maritalStatus != "" {
            profileScore = profileScore + 10
        }
        if anniverseryDay != "" {
            profileScore = profileScore + 10
        }
        
        print(CLASS_NAME+" -- calculateProfileScore() -- "+String(profileScore))
    }
    
    //populate ui
    private func populateUI() -> Void {
        tfFullname.text = fullname
        tfNationality.text = nationality
        tfPhoneNumber.text = phoneNumber
        tfBirthday.text = birthday
        tfMaritalStatus.text = maritalStatus
        tfAnniverseryDay.text = anniverseryDay
        
        if self.gender == "female" {
            //changes on btnGenderFemale
            btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
            btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
            btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
            
            //changes on btnGenderMale
            btnGendermale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
            btnGendermale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
            btnGendermale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
            
            //set gender value
            gender = "female"
        }else {
            //changes on btnGenderMale
            btnGendermale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
            btnGendermale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
            btnGendermale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
            
            //changes on btnGenderFemale
            btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
            btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
            btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
            
            //set gender value
            gender = "male"
        }
    }
    
    //show profile complete progressbar
//    private func showProfileCompleteProgressBar() -> Void {
//        if profileScore >= 25 {
//            progressViewPartOne.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
//        }
//        if profileScore >= 50 {
//            pvPartTwoContainer.alpha = 1.0
//            progressViewPartTwo.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
//        }
//        if profileScore >= 75 {
//            pvPartThreeContainer.alpha = 1.0
//            progressViewPartThree.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
//        }
//        if profileScore == 100 {
//            pvPartFourContainer.alpha = 1.0
//            progressViewPartFour.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
//        }
//    }
}


//extension for UITextField
extension UpdateProfileViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print(CLASS_NAME+" -- textFieldShouldBeginEditing() ")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(CLASS_NAME+" -- textField() -- shouldChangeCharactersIn() ")
        return true
    }
    
}
