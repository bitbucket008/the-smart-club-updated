//
//  EditPasswordViewController.swift
//  smartclub
//
//  Created by RASHED on 1/17/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class EditPasswordViewController: UIViewController, UITextFieldDelegate {
    //MARK: Properties
    fileprivate let CLASS_NAME:String = "EditPasswordViewController"
    let localStorage = LocalStorage()
    var currentPassword: String = ""
    var newPassword: String = ""
    var confirmPassword: String = ""
    
    fileprivate var profileScore = 20
    private var fullname:String = ""
    private var gender:String = ""
    private var nationality:String = ""
    private var phoneNumber:String = ""
    private var birthday:String = ""
    private var maritalStatus:String = ""
    private var anniverseryDay:String = ""
    
    
    //MARK: IBLayout
    @IBOutlet var tfCurrentPassword: UITextField!
    @IBOutlet var tfNewPassword: UITextField!
    @IBOutlet var tfConfirmPassword: UITextField!
    
    @IBOutlet weak var progressViewPartOne: UIView!
    @IBOutlet weak var progressViewPartTwo: UIView!
    @IBOutlet weak var progressViewPartThree: UIView!
    @IBOutlet weak var progressViewPartFour: UIView!
    @IBOutlet weak var ivProgressPartOne: UIImageView!
    @IBOutlet weak var ivProgressPartTwo: UIImageView!
    @IBOutlet weak var ivProgressPartThree: UIImageView!
    @IBOutlet weak var ivProgressPartFour: UIImageView!
    
    @IBOutlet weak var pvPartTwoContainer: UIView!
    @IBOutlet weak var pvPartThreeContainer: UIView!
    @IBOutlet weak var pvPartFourContainer: UIView!
    
    @IBAction func updatePassword(_ sender: Any) {
        currentPassword = tfCurrentPassword.text!
        newPassword = tfNewPassword.text!
        confirmPassword = tfConfirmPassword.text!
        
        postPasswordData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get profile scoreı
//        getProfileInformation()
        
        tfNewPassword.delegate = self
        tfConfirmPassword.delegate = self
        tfCurrentPassword.delegate = self
        dissmisKeyboard()
    }
    
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }
    
    // Start Editing The Text Field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -2, up: true)
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -2, up: false)
    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    
    //MARK: POST Email Data
    //get profile information
    private func getProfileInformation() -> Void {
        SVProgressHUD.show()
        
        print("Profile update url - " + ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)!,
            method: .get,
            parameters: nil)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        self.fullname = swiftyJson["data"]["full_name"].stringValue
                        self.gender = swiftyJson["data"]["gender"].stringValue
                        self.nationality = swiftyJson["data"]["nationality"].stringValue
                        self.birthday = swiftyJson["data"]["birth_date"].stringValue
                        self.anniverseryDay = swiftyJson["data"]["anniversary_date"].stringValue
                        self.phoneNumber = swiftyJson["data"]["phone"].stringValue
                        self.maritalStatus = swiftyJson["data"]["marital_status"].stringValue
                        
                        //calculate profile scvore and polpulate ui
                        self.calculateProfileScore()
                        self.showProfileCompleteProgressBar()
                        
                    }else{
//                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- request failed")
//                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //MARK: Network Call
    //send password data to server
    func postPasswordData(){
        SVProgressHUD.show()
        
        print("pass - "
            + currentPassword + " -- " +  newPassword + " -- " + confirmPassword)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_EDIT_PASSWORD+"/"+localStorage.getUser().getId()!+"/security")!,
            method: .patch,
            parameters:
            ["old_passkey": currentPassword, "new_passkey": newPassword, "confirm_passkey": confirmPassword])
            
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- postPasswordData()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- postPasswordData()  -- response -- "+(String(describing: response)))
        }
    }
    
    
    
    // MARK: Action
    @IBAction func backButtonToAccountSecurityViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Profile Calculation
    private func calculateProfileScore() -> Void {
        if fullname != "" {
            profileScore = profileScore + 10
        }
        if nationality != "" {
            profileScore = profileScore + 10
        }
        if gender != "" {
            profileScore = profileScore + 10
        }
        if phoneNumber != "" {
            profileScore = profileScore + 10
        }
        if birthday != "" {
            profileScore = profileScore + 10
        }
        if maritalStatus != "" {
            profileScore = profileScore + 10
        }
        if anniverseryDay != "" {
            profileScore = profileScore + 10
        }
        
        print(CLASS_NAME+" -- calculateProfileScore() -- "+String(profileScore))
    }
    
    //show profile complete progressbar
    private func showProfileCompleteProgressBar() -> Void {
        if profileScore >= 25 {
            progressViewPartOne.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
        if profileScore >= 50 {
            pvPartTwoContainer.alpha = 1.0
            progressViewPartTwo.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
        if profileScore >= 75 {
            pvPartThreeContainer.alpha = 1.0
            progressViewPartThree.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
        if profileScore == 100 {
            pvPartFourContainer.alpha = 1.0
            progressViewPartFour.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
        }
    }
    
}
