//
//  ProfileViewController.swift
//  smartclub
//
//  Created by Admin on 12/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD


class ProfileViewController: UIViewController {
    //MARK: Properties
    fileprivate let CLASS_NAME:String = "ProfileViewController"
    let localStorage = LocalStorage()
    fileprivate var profileScore = 20
    private var fullname:String = ""
    private var gender:String = ""
    private var nationality:String = ""
    private var phoneNumber:String = ""
    private var birthday:String = ""
    private var maritalStatus:String = ""
    private var anniverseryDay:String = ""
    
    //MARK: Outlets 
    @IBOutlet weak var progressViewPartOne: UIView!
    @IBOutlet weak var progressViewPartTwo: UIView!
    @IBOutlet weak var progressViewPartThree: UIView!
    @IBOutlet weak var progressViewPartFour: UIView!
    @IBOutlet weak var ivProgressPartOne: UIImageView!
    @IBOutlet weak var ivProgressPartTwo: UIImageView!
    @IBOutlet weak var ivProgressPartThree: UIImageView!
    @IBOutlet weak var ivProgressPartFour: UIImageView!
    
    @IBOutlet weak var pvPartTwoContainer: UIView!
    @IBOutlet weak var pvPartThreeContainer: UIView!
    @IBOutlet weak var pvPartFourContainer: UIView!
    
    @IBOutlet weak var btnAccountContainer: UIBorderButton!
    @IBOutlet weak var btnSecurityContainer: UIBorderButton!
    @IBOutlet weak var btnPaymentContainer: UIBorderButton!
    
    @IBOutlet weak var ivAccount: UIImageView!
    @IBOutlet weak var lblAccountTitle: UILabel!
    @IBOutlet weak var lblAccountDescription: UILabel!
    @IBOutlet weak var ivSecurity: UIImageView!
    @IBOutlet weak var lblSecurityTitle: UILabel!
    @IBOutlet weak var lblSecurityDescription: UILabel!
    @IBOutlet weak var ivPayment: UIImageView!
    @IBOutlet weak var lblPaymentTitle: UILabel!
    @IBOutlet weak var lblPaymentDescription: UILabel!
    @IBOutlet weak var lblProfileCompleteness: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get profile score
        getProfileInformation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backBtnAction(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: Actions
    @IBAction func onClickAccount(_ sender: UITapGestureRecognizer) {
        
    }
    
    @IBAction func onClickAccountView(_ sender: UITapGestureRecognizer) {
        btnAccountContainer.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x47C1CE, alpha: 1.0)
        ivAccount.image = UIImage(named: "profile_selected")
        lblAccountTitle.textColor = UIColor.white
        lblAccountDescription.textColor = UIColor.white
        
        BackgroundThread.background(delay: 0.2, background: nil, completion: {
            self.btnAccountContainer.backgroundColor = UIColor.white
            self.ivAccount.image = UIImage(named: "profile_unselected")
            self.lblAccountTitle.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1.0)
            self.lblAccountDescription.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1.0)
            
            let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            let vc = strBoard.instantiateViewController(withIdentifier: "UpdateProfileViewController") as! UpdateProfileViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        
    }
    
    //MARK: Security
    @IBAction func onClickProfileSecurity(_ sender: UITapGestureRecognizer) {
        btnSecurityContainer.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x47C1CE, alpha: 1.0)
        ivSecurity.image = UIImage(named: "security_selected")
        lblSecurityTitle.textColor = UIColor.white
        lblSecurityDescription.textColor = UIColor.white
        
        BackgroundThread.background(delay: 0.2, background: nil, completion: {
            self.btnSecurityContainer.backgroundColor = UIColor.white
            self.ivSecurity.image = UIImage(named: "security_unselected")
            self.lblSecurityTitle.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1.0)
            self.lblSecurityDescription.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1.0)
            
            let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            let vc = strBoard.instantiateViewController(withIdentifier: "AccountSecurityViewController") as! AccountSecurityViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        
    }
    
    //when click payment section
    @IBAction func onClickPaymentItem(_ sender: Any) {
        btnPaymentContainer.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x47C1CE, alpha: 1.0)
        ivPayment.image = UIImage(named: "payment_selected")
        lblPaymentTitle.textColor = UIColor.white
        lblPaymentDescription.textColor = UIColor.white
        
        BackgroundThread.background(delay: 0.2, background: nil, completion: {
            self.btnPaymentContainer.backgroundColor = UIColor.white
            self.ivPayment.image = UIImage(named: "payment_unselected")
            self.lblPaymentTitle.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1.0)
            self.lblPaymentDescription.textColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1.0)
            
            let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            let vc = strBoard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
    }
    
    
    //MARK: Profile Calculation
    private func calculateProfileScore() -> Void {
        if fullname != "" {
            profileScore = profileScore + 10
        }
        if nationality != "" {
            profileScore = profileScore + 10
        }
        if gender != "" {
            profileScore = profileScore + 10
        }
        if phoneNumber != "" {
            profileScore = profileScore + 10
        }
        if birthday != "" {
            profileScore = profileScore + 10
        }
        if maritalStatus != "" {
            profileScore = profileScore + 10
        }
        if anniverseryDay != "" {
            profileScore = profileScore + 10
        }
        
        print(CLASS_NAME+" -- calculateProfileScore() -- "+String(profileScore))
    }
    
    //show profile complete progressbar
    //    private func showProfileCompleteProgressBar() -> Void {
    //        if profileScore >= 25 {
    //            progressViewPartOne.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
    //        }
    //        if profileScore >= 50 {
    //            pvPartTwoContainer.alpha = 1.0
    //            progressViewPartTwo.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
    //        }
    //        if profileScore >= 75 {
    //            pvPartThreeContainer.alpha = 1.0
    //            progressViewPartThree.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
    //        }
    //        if profileScore == 100 {
    //            pvPartFourContainer.alpha = 1.0
    //            progressViewPartFour.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x283E8E, alpha: 1)
    //        }
    //
    //        lblProfileCompleteness.text = "YOUR PROFILE IS "+String(profileScore)+"% COMPLETE "
    //    }
    
    //MARK: Network Call
    //get profile information
    private func getProfileInformation() -> Void {
        SVProgressHUD.show()
        
        print("Profile update url - " + ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_PROFILE_INFORMATION+"/"+localStorage.getUser().getId()!)!,
            method: .get,
            parameters: nil)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        //                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        self.fullname = swiftyJson["data"]["full_name"].stringValue
                        self.gender = swiftyJson["data"]["gender"].stringValue
                        self.nationality = swiftyJson["data"]["nationality"].stringValue
                        self.birthday = swiftyJson["data"]["birth_date"].stringValue
                        self.anniverseryDay = swiftyJson["data"]["anniversary_date"].stringValue
                        self.phoneNumber = swiftyJson["data"]["phone"].stringValue
                        self.maritalStatus = swiftyJson["data"]["marital_status"].stringValue
                        
                        //calculate profile scvore and polpulate ui
                        self.calculateProfileScore()
                        //MARK: Code for imoji - hididng
                        //self.showProfileCompleteProgressBar()
                        
                    }else{
                        //                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- request failed")
                    //                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
}

