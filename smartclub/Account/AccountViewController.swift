//
//  AccountViewController.swift
//  smartclub
//
//  Created by Admin on 11/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: Properties
    private let localStorage = LocalStorage()
    
    //MARK:Outlets
    @IBOutlet weak var tableView:UITableView!
    
    
    //previous
//    var titles = ["PROFILE","EARN POINTS","MY GOAL","ACHIEVEMENTS","SETTINGS","CONTACT US","RULES OF USE", "PRIVACY POLICY","FAQ'S","Log Out"]
    
    //new
    var titles = ["PROFILE","CONTACT US","RULES OF USE", "PRIVACY POLICY","FAQ'S","LOG OUT"] //,"ACHIEVEMENTS", ,"SETTINGS"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        tableView.rowHeight = 50
//        tableView.contentInset = UIEdgeInsetsMake(2, 2, 2, 2)
    
        tableView.register(UINib(nibName: "AccountTableViewCell", bundle: nil), forCellReuseIdentifier: "accounttableviewcell")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //#warning Incomplete implementation, return the number of rows
        //return 10
        return titles.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "accounttableviewcell", for: indexPath) as! AccountTableViewCell
        
        cell.selectionStyle = .none
//        cell.layoutMargins = UIEdgeInsetsMake(2, 2, 2, 2)
    
        
        let imgName = "ac_\(indexPath.row + 1)"
        
        cell.imgView.image = UIImage.init(named: imgName)
        cell.titleLabel.text = titles[indexPath.row]
        
        // Configure the cell...
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        
        if indexPath.row == 0 {
            let vc = strBoard.instantiateViewController(withIdentifier: "profilevc") as! ProfileViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
//        else if indexPath.row  == 1 {
//           // let strBoard = UIStoryboard.init(name: "account", bundle: nil)
//
//            let vc = strBoard.instantiateViewController(withIdentifier: "earnpointvc") as! EarnPointViewController
//
//             self.navigationController?.pushViewController(vc, animated: true)
//
//           // self.present(vc, animated: true, completion: nil)
//        }
//
//        else if indexPath.row == 2 {
//           // let strBoard = UIStoryboard.init(name: "account", bundle: nil)
//
//            let vc = strBoard.instantiateViewController(withIdentifier: "goalvc") as! GoalViewController
//
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
        
//        else if indexPath.row == 1 {
//            //let strBoard = UIStoryboard.init(name: "account", bundle: nil)
//
//            let vc = strBoard.instantiateViewController(withIdentifier: "achivementvc") as! AchivementViewController
//
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
//        else if indexPath.row == 1 {
//           // let strBoard = UIStoryboard.init(name: "account", bundle: nil)
//
//            let vc = strBoard.instantiateViewController(withIdentifier: "settingsvc") as! SettingsViewController
//
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
        
        else if indexPath.row == 1 {
           // let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            
            let vc = strBoard.instantiateViewController(withIdentifier: "contactus") as! ContactUsViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if indexPath.row == 2 {
           // let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            
            let vc = strBoard.instantiateViewController(withIdentifier: "termsconditionvc") as! TermsConditionViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if indexPath.row == 3 {
            //let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            
            let vc = strBoard.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
         }
        
        else if indexPath.row == 4 {
           // let strBoard = UIStoryboard.init(name: "account", bundle: nil)
            let vc = strBoard.instantiateViewController(withIdentifier: "faqvc") as! TempViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else if indexPath.row == 5 {
            //logout and empty local storage
            localStorage.setUserLoggedIn(loggedIn: false)
            localStorage.logOut()
            
            //go to user type view controller
            let strBoard = UIStoryboard.init(name: "Login", bundle: nil)
            let vc = strBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        }
    
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70;
        
    }
    
}
