//
//  AddCardViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 2/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class AddCardViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet weak var tfCardNumber: UITextField!
    @IBOutlet weak var tfCardEndMonth: UITextField!
    @IBOutlet weak var tfCardEndYear: UITextField!
    @IBOutlet weak var tfCVV: UITextField!
    @IBOutlet weak var tfCardHolderName: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    
    //MARK: Properties
    open let CLASS_NAME = "AddCardViewController"
    fileprivate let localStorage = LocalStorage()
    private var cardNumber:String?
    private var cardEndMonth:String?
    private var cardEndYear:String?
    private var cvv:String?
    private var cardHolderName:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK Actions
    @IBAction func onBackPress(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: true)
    }
    
    //when click on add button
    @IBAction func onClickAddButton(_ sender: UIButton) {
        //get value
        cardNumber = tfCardNumber.text
        cardEndMonth = tfCardEndMonth.text
        cardEndYear = tfCardEndYear.text
        cvv = tfCVV.text
        cardHolderName = tfCardHolderName.text
        
        //validate and add card
        validateAndAddCard()
    }
    
    //validate and add card
    private func validateAndAddCard() -> Void {
        if !Validator.isValidString(value: cardNumber!){
            SnackBarManager.showSnackBar(message: "Invalid card number")
        }else if !Validator.isValidString(value: cardEndMonth!){
            SnackBarManager.showSnackBar(message: "Invalid end month")
        }else if !Validator.isValidString(value: cardEndYear!){
            SnackBarManager.showSnackBar(message: "Invalid end year")
        }else if !Validator.isValidString(value: cvv!){
            SnackBarManager.showSnackBar(message: "Invalid CVV")
        }else if !Validator.isValidString(value: cardHolderName!){
            SnackBarManager.showSnackBar(message: "Invalid card holder name")
        }else{
            //send card data to server
            addCardDataToServer()
        }
    }
    
    
    //MARK: Network call
    //send card data to server
    private func addCardDataToServer() -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_ADD_CREDIT_CARD+"/"+localStorage.getUser().getId()! + ApiEndpoints.URI_CREDIT_CARD)!,
            method: .put,
            parameters: ["card_holder_name":cardHolderName!, "credit_card_no":cardNumber!, "cvv":cvv!, "expiry_year":cardEndYear!, "expiry_month":cardEndMonth!])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        print(self.CLASS_NAME+" -- "+swiftyJson["message"].stringValue)
                        
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
    
}
