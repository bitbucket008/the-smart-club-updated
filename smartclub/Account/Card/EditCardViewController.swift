//
//  EditCardViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 2/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class EditCardViewController: UIViewController {
    //MARK: Properties
    open let CLASS_NAME = "EditCardViewController"
    fileprivate let localStorage = LocalStorage()
    private var cardNumber:String?
    private var cardEndMonth:String?
    private var cardEndYear:String?
    private var cvv:String?
    private var cardHolderName:String?
    
    //MARK:Outlets
    @IBOutlet weak var tfCardNumber: UITextField!
    @IBOutlet weak var tfEndMonth: UITextField!
    @IBOutlet weak var tfEndYear: UITextField!
    @IBOutlet weak var tfCVC: UITextField!
    @IBOutlet weak var tfCardHolderName: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: Actions
    @IBAction func onBackPress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //when click on update button
    @IBAction func onClickUpdateButton(_ sender: UIButton) {
        //get value
        cardNumber = tfCardNumber.text
        cardEndMonth = tfEndMonth.text
        cardEndYear = tfEndYear.text
        cvv = tfCVC.text
        cardHolderName = tfCardHolderName.text
        
        //validate and update card
        validateAndUpdateCard()
    }
    
    //validate and update card
    private func validateAndUpdateCard() -> Void {
        if !Validator.isValidString(value: cardNumber!){
            SnackBarManager.showSnackBar(message: "Invalid card number")
        }else if !Validator.isValidString(value: cardEndMonth!){
            SnackBarManager.showSnackBar(message: "Invalid end month")
        }else if !Validator.isValidString(value: cardEndYear!){
            SnackBarManager.showSnackBar(message: "Invalid end year")
        }else if !Validator.isValidString(value: cvv!){
            SnackBarManager.showSnackBar(message: "Invalid CVV")
        }else if !Validator.isValidString(value: cardHolderName!){
            SnackBarManager.showSnackBar(message: "Invalid card holder name")
        }else{
            //send card data to server
            updateCardDataToServer()
        }
    }
    
    
    //MARK: Network call
    //send card data to server
    private func updateCardDataToServer() -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_ADD_CREDIT_CARD+"/"+localStorage.getUser().getId()! + ApiEndpoints.URI_CREDIT_CARD)!,
            method: .put,
            parameters: ["card_holder_name":cardHolderName!, "credit_card_no":cardNumber!, "cvv":cvv!, "expiry_year":cardEndYear!, "expiry_month":cardEndMonth!])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        print(self.CLASS_NAME+" -- "+swiftyJson["message"].stringValue)
                        
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getProfileInformation()  -- response -- "+(String(describing: response)))
                
        }
    }
    

}
