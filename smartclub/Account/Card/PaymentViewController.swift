//
//  PaymentViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 2/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD


//MARK: protocols
protocol CardListDelegate{
    func onClickAddEditCard(position:Int?) -> Void
}

class PaymentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK: Properties
    fileprivate let CLASS_NAME = "PaymentViewController"
    fileprivate let localStorage = LocalStorage()
    fileprivate var cardList:[Card] = [Card]()
    
    //MARK: Outlets
    @IBOutlet weak var tvCardList: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // initialization
        initialize()
        
        //get customer credit cards
        getCustomerCards()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: UI Method
    //initialization
    private func initialize() -> Void {
        tvCardList.dataSource = self
        tvCardList.delegate = self
        tvCardList.register(UINib(nibName: "cardlisttableviewcell", bundle: nil), forCellReuseIdentifier: "CardListTableViewCell")
        
//        cardList = [
//            Card(id:Optional("1"), cardHolderName:Optional("Test"), cardNumber:Optional("1234-5678-9012-1234"), cvv:Optional("151"), endMonth:Optional("10"), endYear:Optional("2020")),
//            Card(id:Optional("1"), cardHolderName:Optional("Test"), cardNumber:Optional("1234-5678-9012-1234"), cvv:Optional("151"), endMonth:Optional("10"), endYear:Optional("2020")),
//            Card(id:Optional("1"), cardHolderName:Optional("Test"), cardNumber:Optional("1234-5678-9012-1234"), cvv:Optional("151"), endMonth:Optional("10"), endYear:Optional("2020")),
//            Card(id:Optional("1"), cardHolderName:Optional("Test"), cardNumber:Optional("1234-5678-9012-1234"), cvv:Optional("151"), endMonth:Optional("10"), endYear:Optional("2020")),
//            Card(id:Optional("1"), cardHolderName:Optional("Test"), cardNumber:Optional("1234-5678-9012-1234"), cvv:Optional("151"), endMonth:Optional("10"), endYear:Optional("2020"))
//            ]
//        tvCardList.reloadData()
    }

    //MARK: Actions
    @IBAction func onBackPress(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: true)
    }
    
    //when click on add card button
    @IBAction func onClickAddButton(_ sender: UIBorderButton) {
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        
        let vc = strBoard.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //when click edit button
    @IBAction func onClickEditButton(_ sender: UIBorderButton) {
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        
        let vc = strBoard.instantiateViewController(withIdentifier: "EditCardViewController") as! EditCardViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: Network Call
    //get customer card
    private func getCustomerCards() -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_ADD_CREDIT_CARD+"/"+localStorage.getUser().getId()! + ApiEndpoints.URI_CREDIT_CARD)!,
            method: .get,
            parameters: nil)
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        print(self.CLASS_NAME+" -- getCustomerCards()() --  "+swiftyJson["message"].stringValue)
                        
                        if(swiftyJson["data"].arrayValue.count > 0){
                            for i in 1 ..< swiftyJson["data"].arrayValue.count {
                                let creditCardHolderName = swiftyJson["data"][i]["creditCardHolderName"].string
                                let creditCardNo = swiftyJson["data"][i]["creditCardNo"].string
                                let cvv = swiftyJson["data"][i]["cvv"].int
                                let expMonth = swiftyJson["data"][i]["expMonth"].int
                                let expYear = swiftyJson["data"][i]["expYear"].int
                                
                                self.cardList.append(Card(cardHolderName:creditCardHolderName, cardNumber:creditCardNo, cvv:cvv, endMonth:expMonth, endYear:expYear))
                                print(self.CLASS_NAME+" -- getCustomerCards()  -- holder name -- array size 0")
                            }
                    
                            //reload tableview data
                            self.tvCardList.reloadData()
                        }else{
                            print(self.CLASS_NAME+" -- getCustomerCards()  -- response -- array size 0")
                        }
                    
                        
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- getCustomerCards()()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- getCustomerCards()()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //MARK: TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cardList.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardListTableViewCell", for: indexPath) as! CardListTableViewCell
        
        print(CLASS_NAME+" -- index path - "+String(indexPath.row))
        
        cell.selectionStyle = .none
        cell.delegate = self
        cell.cardPosition = Optional(indexPath.row)
        
        if indexPath.row >= cardList.count{
            cell.addEditButton.setTitle("ADD", for: .normal)
            cell.lblText.text = "Add new credit card"
        }else{
            cell.addEditButton.setTitle("EDIT", for: .normal)
            cell.lblText.text = cardList[indexPath.row].cardHolderName!
        }
        
        return cell
    }
    
}

extension PaymentViewController : CardListDelegate{
    //when click on add/edit card
    func onClickAddEditCard(position: Int?) {
        if let cardPosition = position{
            print(CLASS_NAME+" -- onClickAddEditCard() -- position -- "+String(cardPosition))
            if cardPosition >= cardList.count{
                print(CLASS_NAME+" -- onClickAddEditCard() -- Add button is pressed...")
                let strBoard = UIStoryboard.init(name: "account", bundle: nil)
                let vc = strBoard.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                print(CLASS_NAME+" -- onClickAddEditCard() -- Edit button is pressed...")
                let strBoard = UIStoryboard.init(name: "account", bundle: nil)
                let vc = strBoard.instantiateViewController(withIdentifier: "EditCardViewController") as! EditCardViewController
                
                vc.tfCardNumber.text = String(cardList[position!].cardNumber!)
                vc.tfCardHolderName.text = String(cardList[cardPosition].cardHolderName!)
                vc.tfEndMonth.text = String(cardList[cardPosition].endMonth!)
                vc.tfEndYear.text = String(cardList[cardPosition].endYear!)
                vc.tfCVC.text = String(cardList[cardPosition].cvv!)
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            print(CLASS_NAME+" -- onClickAddEditCard() -- position is nil")
        }
    }
}
