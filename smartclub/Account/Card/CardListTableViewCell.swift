//
//  CardListTableViewCell.swift
//  smartclub
//
//  Created by Mac-Admin on 2/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class CardListTableViewCell: UITableViewCell {

    //MARK: Properties
    open static let CLASS_NAME = "CardListTableViewCell"
    open var delegate:CardListDelegate?
    open var cardPosition:Int?
    
    //MARK: Outlets
    @IBOutlet weak var addEditButton: UIBorderButton!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var uivPoint: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK Actions
    //when click on add edit button
    @IBAction func onClickAddEditButton(_ sender: UIBorderButton) {
        delegate?.onClickAddEditCard(position: cardPosition)
    }
    
}
