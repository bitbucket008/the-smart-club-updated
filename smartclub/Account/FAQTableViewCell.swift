//
//  FAQTableViewCell.swift
//  smartclub
//
//  Created by Admin on 14/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit



protocol FAQTableViewCellDelegate: class {
    func toggleSection(header: FAQTableViewCell, section: Int)
}


class FAQTableViewCell: UITableViewHeaderFooterView {
    
    
    weak var delegate : FAQTableViewCellDelegate?
    @IBOutlet weak var questionTitle:UILabel!
    
    
    @IBOutlet weak var imageV: UIImageView!
    
    var section: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))

    }
    @objc private func didTapHeader() {
        //delegate?.toggleSection(header: self, section: section)
        
        print("Called from tap header")
        delegate?.toggleSection(header: self, section: section)
        self.imageV.transform = CGAffineTransform(rotationAngle: .pi)
    }
    

    //Arrow rotation
    
        @IBAction func applyTransForm(sender: UIButton) {
            sender.isSelected = !sender.isSelected
            UIView.animate(withDuration: 0.2, animations: {
                if sender.isSelected {
                    self.imageV.transform = CGAffineTransform(rotationAngle: .pi)
                } else {
                    self.imageV.transform = CGAffineTransform(rotationAngle: 0)

                }
            })

        }
   
}
