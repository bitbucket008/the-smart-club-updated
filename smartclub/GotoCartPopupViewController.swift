//
//  GotoCartPopupViewController.swift
//  smartclub
//
//  Created by Admin on 03/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit




protocol GotoCartPopupViewControllerDelegate :NSObjectProtocol {
    
    func  onClickGotoCart()
    
    
}


class GotoCartPopupViewController: UIViewController {

    
    weak var delegate:GotoCartPopupViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func gotoCartBtnAction(_ sender: Any) {
        
        
        
        self.dismiss(animated: true, completion: nil)
        
       delegate?.onClickGotoCart()
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func topGestureAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
