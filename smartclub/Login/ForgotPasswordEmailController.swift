//
//  ForgotPasswordEmailController.swift
//  smartclub
//
//  Created by Mac-Admin on 4/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ForgotPasswordVerificationCodeController: UIViewController {
    
    //MARK: Properties
    fileprivate let CLASS_NAME = "ForgotPasswordVerificationCodeController"
    
    
    //MARK: Outlets
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    
    override func viewDidLoad() {
        btnNext.layer.cornerRadius = 20
        dissmisKeyboard()
    }
    
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }
    
    
    //MARK:Action
    @IBAction func onClickBackButton(_ sender: UIButton, forEvent event: UIEvent) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //when click on next button
    @IBAction func onClickNextButton(_ sender: UIButton, forEvent event: UIEvent){
        if !Validator.isValidEmail(email: self.tfEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)) {
            SnackBarManager.showSnackBar(message: "Provide a valid email")
        }else{
            self.forgotPasswordVerificationCodeRequest(email: self.tfEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        }
    }
    
    
    //MARK: Network Call
    //network call for forgot password email request
    private func forgotPasswordVerificationCodeRequest(email:String) -> Void {
        
        print(CLASS_NAME+" -- forgotPasswordVerificationCodeRequest() -- email : \(email)")
        
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_FORGOT_PASSWORD_VERIFICATION_CODE)!,
            method: .patch ,
            parameters: ["email": email], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            
                            //open next alert dialog for change password
                            self.showNewPasswordViewController()
                            
                        }else{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- loginUser() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- loginUser()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- loginUser()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //show new password screen for forgot password
    private func showNewPasswordViewController() -> Void {
        let strBoard = UIStoryboard.init(name: "Login", bundle: nil)
        let vc = strBoard.instantiateViewController(withIdentifier: "ForgotPasswordNewPasswordController") as! ForgotPasswordNewPasswordController
        
        vc.email = Optional(tfEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        
        self.present(vc, animated: true, completion: nil)
    }
}
