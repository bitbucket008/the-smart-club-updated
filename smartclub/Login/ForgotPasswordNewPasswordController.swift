//
//  ForgotPasswordVerificationCodeController.swift
//  smartclub
//
//  Created by Mac-Admin on 4/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ForgotPasswordNewPasswordController: UIViewController {
    
    //MARK:: Properties
    fileprivate let CLASS_NAME = "ForgotPasswordNewPasswordController"
    open var email:String?
    
    //MARK: Outlets
    @IBOutlet weak var tfVerificationCode: UILabel!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmNewPassword: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    
    override func viewDidLoad() {
        btnSubmit.layer.cornerRadius = 20
        dissmisKeyboard()
    }
    
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }

    
    
    
    //MARK:Actions
    @IBAction func onClickBackButton(_ sender: UIButton, forEvent event: UIEvent) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickSubmitButton(_ sender: UIButton, forEvent event: UIEvent) {
        self.changePasswordRequest(email: email!, verificationCode: tfVerificationCode.text!.trimmingCharacters(in: .whitespacesAndNewlines), password: tfNewPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines), confirmPassword: tfConfirmNewPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    
    //MARK:Network call
    //network call change password request
    private func changePasswordRequest(email:String, verificationCode:String, password:String, confirmPassword:String) -> Void {
        
        print(CLASS_NAME+" -- changePasswordRequest() -- email : \(email) -- verificationCode : \(verificationCode) -- password : \(password) -- confirmPassword: \(confirmPassword)")
        
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_CHANGE_PASSWORD_REQUEST)!,
            method: .patch ,
            parameters: ["email": email, "code": verificationCode, "passkey": password, "cofirm_passkey": confirmPassword], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            
                        }else{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- loginUser() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- loginUser()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- loginUser()  -- response -- "+(String(describing: response)))
                
        }
    }

}
