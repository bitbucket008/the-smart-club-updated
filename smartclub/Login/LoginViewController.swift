//
//  LoginViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 1/2/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {
    //MARK: instance variable
    fileprivate var isKeyboardAppeared = false
    fileprivate let CLASS_NAME:String = "LoginViewController"
    private var email:String?
    private var password:String?
    fileprivate var forgotPasswordEamil:String = ""
//    open var userType:String?
    private var isPasswordVisible:Bool = false
    
    //MARK: Outlets
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnPasswordVisibility: UIButton!
    @IBOutlet weak var btnLogin: UIBorderButton!
    @IBOutlet weak var lblForgotPassword: UILabel!
    
    //var scrollView: UIScrollView!
    //let scrollView = UIScrollView(frame: UIScreen.main.bounds)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let screenSize = UIScreen.main.bounds
        //let screenWidth = screenSize.width
        //let screenHeight = screenSize.height
        
        //self.view.addSubview(scrollView)
        //scrollView.addSubview(imageView)
        

        //setup ui
        setupUI()
        
        //initialization
        initialization()
        
        //dismiss keyboard when press to outside
        dissmisKeyboard()
        
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        scrollView.frame = view.bounds
//        self.scrollView.contentSize = CGSize(width:2000, height: 5678)
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//       scrollView.contentSize = CGSize(width: UIViewHeader.frame.size.width + lblName.frame.size.width + tableView.frame.size.width + btnRegisteredCourses.frame.size.width , height: scrollView.frame.size.height)
//    }
    
    
    
    //MARK: init methods
    private func setupUI() -> Void {
        // tfPassword
        tfPassword.isSecureTextEntry = true
    }

    //initialization
    private func initialization() -> Void{
        self.tfEmail!.delegate = self
        self.tfPassword!.delegate = self
        
        //for  forgot password
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onClickForgotPassword(sender:)))
        self.lblForgotPassword.addGestureRecognizer(gesture)
    }


    //MARK: Actions
    //when click on btnPasswordVisibility
    @IBAction func onClickBtnPasswordVisibility(_ sender: UIButton, forEvent event: UIEvent) {
        if !isPasswordVisible {
            tfPassword?.isSecureTextEntry = false
//            sender.setImage(UIImage(named: "ic_invisible")!, for: UIControlState.normal)
            isPasswordVisible = true
        }else{
            tfPassword?.isSecureTextEntry = true
//            sender.setImage(UIImage(named: "ic_visible")!, for: UIControlState.normal)
            isPasswordVisible = false
        }
    }
    
    //when click on btnLogin
    @IBAction func onClickBtnLogin(_ sender: UIBorderButton, forEvent event: UIEvent) {
        email = tfEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        password = tfPassword.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //hoide keyboard
        self.view.endEditing(true)
        
        //validate and login user
        validateAndLoginUser()
    }
    
    //when click on forgot password
    @objc func onClickForgotPassword(sender:UITapGestureRecognizer) -> Void {
        print(CLASS_NAME+" -- onClickForgotPassword() ")
        
//        showAlertControllerForgotPassword()
        
        let strBoard = UIStoryboard.init(name: "Login", bundle: nil)
        let vc = strBoard.instantiateViewController(withIdentifier: "ForgotPasswordVerificationCodeController") as! ForgotPasswordVerificationCodeController
        
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //MARK: AlertDialog
    //alert controller for email
    private func showAlertControllerForgotPassword() -> Void {
        let alertController = UIAlertController(title: "Forgot password?", message: "You need to provide your email and we will send you a verification code to that email, which will be required to change password on next screen", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Go", style: .default, handler: { alert -> Void in
            let emailField = alertController.textFields![0] as UITextField
            self.forgotPasswordEamil = emailField.text!
            
            if !Validator.isValidEmail(email: self.forgotPasswordEamil) {
                SnackBarManager.showSnackBar(message: "Provide a valid email")
            }else{
//                self.showAlertControllerToChangePassword()
                self.forgotPasswordVerificationCodeRequest(email: self.forgotPasswordEamil)
            }
            
            // do something with textField
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter your email "
            textField.keyboardType = .emailAddress
            textField.enablesReturnKeyAutomatically = false
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //alert controller for change password
    private func showAlertControllerToChangePassword() -> Void {
        let alertController = UIAlertController(title: "Reset password", message: "Enter the verification code which we have sent to your email", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: { alert -> Void in
            let verificationCodeField = alertController.textFields![0] as UITextField
            let passwordField = alertController.textFields![1] as UITextField
            let confirmPasswordField = alertController.textFields![2] as UITextField
            
            self.changePasswordRequest(email: self.forgotPasswordEamil, verificationCode: verificationCodeField.text!, password: passwordField.text!, confirmPassword: confirmPasswordField.text!)
            
            // do something with textField
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter verification code "
            textField.keyboardType = .default
            textField.enablesReturnKeyAutomatically = false
        })
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Password "
            textField.keyboardType = .default
            textField.enablesReturnKeyAutomatically = false
        })
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Confirm password"
            textField.keyboardType = .default
            textField.enablesReturnKeyAutomatically = false
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK: Method -> Login
    //validate and register user
    private func validateAndLoginUser() -> Void {
         if !Validator.isValidEmail(email: email!){
            SnackBarManager.showSnackBar(message: "Invalid email")
        }else if !Validator.isValidPassword(password: password!){
            SnackBarManager.showSnackBar(message: "Invalid password")
        }else if !ConnectivityDetector.isInternetAvailable(){
            SnackBarManager.showSnackBarForInternetSetting()
            print(CLASS_NAME+" -- validateAndLoginUser()  -- No internet connection.....")
        }else{
            //login user
            loginUser()
        }
    }
    
    //Loign user
    private func loginUser() -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
        URL(string: ApiEndpoints.URI_LOGIN)!,
        method: .post,
        parameters: ["email": email!, "passkey": password!, "user_type": "customer"], encoding: JSONEncoding.default)
        .responseJSON { (response) -> Void in
            SVProgressHUD.dismiss()
            
            //go to  login view controller
            if response.result.isSuccess{
                do{
                     let swiftyJson = try JSON(data: response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                        let username = swiftyJson["data"]["full_name"].string
                        let email = swiftyJson["data"]["email"].string
                        let id = swiftyJson["data"]["id"].string
                        let earnedSp = swiftyJson["data"]["sp_earned"].intValue
                         let referralCode = swiftyJson["data"]["referral_code"].string
                        let userType = swiftyJson["data"]["user_type"].string
                        let customerType = swiftyJson["data"]["customer_type"].string
                        let profilePicture = swiftyJson["data"]["profile_img"].string
                        let lastLogin = swiftyJson["data"]["last_login"].boolValue
                        
                        let user = User()
                        user.username = username
                        user.email = email
                        user.id = id
                        user.earnedSp = earnedSp
                        user.userType = userType
                        user.customerType = customerType
                        user.profilePicture = profilePicture
                        user.referralCode = referralCode
                        
                        let localStorage = LocalStorage()
                        localStorage.storeUser(user: user)
                        localStorage.setUserLoggedIn(loggedIn: true)
                        print(self.CLASS_NAME+" localstorage - "+localStorage.getUser().id!)
                        
                        let dashboardStoryboard = UIStoryboard.init(name: "dashboard", bundle: nil)
                        let dashboardViewController = dashboardStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                        dashboardViewController.isLoginFirstTime = !lastLogin
                        //wait few seconds
                        BackgroundThread.background(delay: 3.0, background: nil, completion: {
                            self.present(dashboardViewController, animated: true, completion: nil)
                        })
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                }catch _{
                    print(self.CLASS_NAME+" -- loginUser() -- swiftyJSON parsing error")
                }
                
                
            }else{
                print(self.CLASS_NAME+" -- loginUser()  -- response -- request failed")
                SnackBarManager.showSnackBar(message: "Request failed")
            }
            
            print(self.CLASS_NAME+" -- loginUser()  -- response -- "+(String(describing: response)))
            
        }
    }
    
    
    //network call for forgot password email request
    private func forgotPasswordVerificationCodeRequest(email:String) -> Void {
        
        print(CLASS_NAME+" -- changePasswordRequest() -- email : \(email)")
        
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_FORGOT_PASSWORD_VERIFICATION_CODE)!,
            method: .patch ,
            parameters: ["email": email], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            
                            //open next alert dialog for change password
                            self.showAlertControllerToChangePassword()
                            
                        }else{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- loginUser() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- loginUser()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- loginUser()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //network call change password request
    private func changePasswordRequest(email:String, verificationCode:String, password:String, confirmPassword:String) -> Void {
        
        print(CLASS_NAME+" -- changePasswordRequest() -- email : \(email) -- verificationCode : \(verificationCode) -- password : \(password) -- confirmPassword: \(confirmPassword)")
        
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_CHANGE_PASSWORD_REQUEST)!,
            method: .patch ,
            parameters: ["email": email, "code": verificationCode, "passkey": password, "cofirm_passkey": confirmPassword], encoding: JSONEncoding.default)
            .responseJSON { (response) -> Void in
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    do{
                        let swiftyJson = try JSON(data: response.data!)
                        if  swiftyJson["success"].intValue == 1{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                            
                        }else{
                            SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        }
                    }catch _{
                        print(self.CLASS_NAME+" -- loginUser() -- swiftyJSON parsing error")
                    }
                    
                    
                }else{
                    print(self.CLASS_NAME+" -- loginUser()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- loginUser()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //MARK: TextFieldDelegate
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.endEditing(true)
//        self.view.endEditing(true)
//        return true
//    }
    
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }
    
    // Start Editing The Text Field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -220, up: true)
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -220, up: false)
    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    //MARK: Actions
    //go to register view controller
    @IBAction func onClickToRegister(_ sender: UIButton, forEvent event: UIEvent) {
        let registrationStoryBoard = UIStoryboard.init(name: "Registration", bundle: nil)
        //goto select user type view controller
        let userTypeViewController = registrationStoryBoard.instantiateViewController(withIdentifier: "UserTypeViewController") as! UserTypeViewController
        self.present(userTypeViewController, animated: true, completion: nil)
        
//        if userType! == "student"{
//            let registrationStudentViewController = registrationStoryBoard.instantiateViewController(withIdentifier: "RegistrationStudentViewController") as! RegistrationStudentViewController
//            registrationStudentViewController.userType = userType
//            self.present(registrationStudentViewController, animated: true, completion: nil)
//        }else{
//            let registrationNormalViewController = registrationStoryBoard.instantiateViewController(withIdentifier: "RegistrationNormalViewController") as! RegistrationNormalViewController
//            registrationNormalViewController.userType = userType
//            self.present(registrationNormalViewController, animated: true, completion: nil)
//        }
    }
    
    
}
