//
//  OwnedOffer.swift
//  smartclub
//
//  Created by Mac-Admin on 1/7/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class OwnedOffer: NSObject {
    open var id:String
    open var name:String
    open var spNeed:Int
    open var offerDescription:String?
    open var orgName:String?
    open var identityCode:String?
    open var vendorId:String?
    open var image:String?
    
    init(id:String, name:String, spNeed:Int, offerDescription:String?, orgName:String?, identityCode:String?,  vendorId:String?, image:String?) {
        self.id = id
        self.name = name
        self.spNeed = spNeed
        self.orgName = orgName
        self.offerDescription = offerDescription
        self.identityCode = identityCode
        self.vendorId = vendorId
        self.image = image
    }
}
