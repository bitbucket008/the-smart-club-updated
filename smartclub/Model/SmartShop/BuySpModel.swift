//
//  BuySpModel.swift
//  smartclub
//
//  Created by Mac-Admin on 1/16/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class BuySpModel {
    open var id:String?
    open var isLock:Int?
    open var qarPrice:Float?
    open var spVal:Int?
    open var status:String?
    
    init(id:String?, isLock:Int?, qarPrice:Float?, spVal:Int?, status:String?) {
        self.id = id
        self.isLock = isLock
        self.qarPrice = qarPrice
        self.spVal = spVal
        self.status = status
    }
}
