//
//  VendorDetails.swift
//  smartclub
//
//  Created by Mac-Admin on 1/4/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class VendorDeal: NSObject {
    //MARK: Property
    open var id:String
    open var name:String
    open var vendorDealDescription:String?
    open var orgName:String?
    open var gainSP:Int
    open var identityCode:String
    open var dealType:String?
    open var isExpanded:Bool?
    open var isDownArrowPressed:Bool?
    
    init(id:String, name:String, vendorDealDescription:String?, orgName:String?, gainSP:Int, identityCode:String, dealType:String?){
        self.id = id
        self.name = name
        self.vendorDealDescription = vendorDealDescription
        self.orgName = orgName
        self.gainSP = gainSP
        self.identityCode = identityCode
        self.dealType = dealType
    }
}
