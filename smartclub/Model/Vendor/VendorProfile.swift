//
//  VendorProfile.swift
//  smartclub
//
//  Created by Mac-Admin on 1/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class VendorProfile: NSObject {
    //MARK: Property
    open var additionalPhone:String?
    open var email:String?
    open var orgName:String?
    open var phone:String?
    open var websiteUrl:String?
    open var coverImage:String?
    open var businessHour:String?
    open var address:String?
    open var latitude:String?
    open var longitude:String?
    open var linkFaceebok:String?
    open var linkTwitter:String?
    open var linkInstagram:String?
    open var linkSnapchat:String?
    open var linkWeb:String?
    open var mapLink:String? 
    
    
    override init() {
        
    }
    
    init(additionalPhone:String?, email:String?, orgName:String?, phone:String?, websiteUrl:String?, coverImage:String?, businessHour:String?){
        self.additionalPhone = additionalPhone
        self.email = email
        self.orgName = orgName
        self.phone = phone
        self.websiteUrl = websiteUrl
        self.coverImage = coverImage
        self.businessHour = businessHour
    }
}
