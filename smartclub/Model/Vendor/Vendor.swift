//
//  Vendor.swift
//  smartclub
//
//  Created by Mac-Admin on 1/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class Vendor: NSObject {
    //MARK: Property
    open var vendorId:String?
    open var orgName:String?
    open var profileImage:String?
    open var spLeast:String?
    open var distance:String?
    
    init(vendorId:String?, orgName:String?, profileImage:String?, spLeast:String?, distance:String?) {
        self.vendorId = vendorId
        self.orgName = orgName
        self.profileImage = profileImage
        self.spLeast = spLeast
        self.distance = distance
    }
}
