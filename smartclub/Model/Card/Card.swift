//
//  Card.swift
//  smartclub
//
//  Created by Mac-Admin on 2/15/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class Card: NSObject {
    open var id:String?
    open var cardHolderName:String?
    open var cardNumber:String?
    open var cvv:Int?
    open var endMonth:Int?
    open var endYear:Int?
    
    init(cardHolderName:String?, cardNumber:String?, cvv:Int?, endMonth:Int?, endYear:Int?) {
        self.cardHolderName = cardHolderName
        self.cardNumber = cardNumber
        self.cvv = cvv
        self.endMonth = endMonth
        self.endYear = endYear
    }
}
