//
//  Wallet.swift
//  smartclub
//
//  Created by RASHED on 1/16/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class Wallet: NSObject {
    open var left: Int
    open var main: Int
    open var right: Int
    
    init(left: Int, main: Int, right: Int){
        self.left = left
        self.main = main
        self.right = right
        
    }
}
