//
//  BusinessCategory.swift
//  smartclub
//
//  Created by Mac-Admin on 1/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

open class BusinessCategory: NSObject {
    open var id:Int
    open var name:String
    open var image:String?
    open var status:Int
    
    init(id:Int, name:String, image:String?, status:Int) {
        self.id = id
        self.name = name
        self.image = image
        self.status = status
    }
    
}
