//
//  Survey.swift
//  smartclub
//
//  Created by Mac-Admin on 2/19/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class Survey: NSObject {
    open var id:Int
    open var title:String
   // open var surveyDescription:String
    open var sp:Int
    open var descr:String
    
    init(id:Int, title:String, surveyDescription:String, sp:Int, descr:String) {
        self.id = id
        self.title = title
        //self.surveyDescription = surveyDescription
        self.sp = sp
        self.descr = descr
    }
}
