//
//  SurveyAnswer.swift
//  smartclub
//
//  Created by Mac-Admin on 3/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SurveyAnswer: NSObject {
    open var id:Int?
    open var title:String?
    open var surveyDescription:String?
    open var sp:Int?
}
