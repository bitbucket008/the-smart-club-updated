//
//  SurveyDetails.swift
//  smartclub
//
//  Created by Mac-Admin on 2/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SurveyDetails: NSObject {
    open var id:Int?
    open var name:String?
    open var questionRecord:[Any]?
    open var spGain:Int?
}
