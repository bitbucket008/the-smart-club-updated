//
//  SurveyQuestionRange.swift
//  smartclub
//
//  Created by Mac-Admin on 3/17/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class SurveyQuestionRange: NSObject {
    open var index:Int?
    open var title:String?
    open var type:String?
    open var optionNames:[Any]?
}
