//
//  User.swift
//  smartclub
//
//  Created by RASHED on 1/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

open class User : NSObject{
    open var id:String?
    open var username:String?
    open var email:String?
    open var earnedSp:Int?
    open var userType:String?
    open var customerType:String?
    open var profilePicture:String?
    open var referralCode:String?
//    open var deviceToken:String?
    
    override init() {
        
    }
    
    init( id:String?, username:String?, email:String?) {
        self.username = username
        self.email = email
        self.id = id
    }
    
    open func setUsername(username:String?) {
        self.username = username
    }
    
    open func getUsername() -> String? {
        return self.username
    }
    
    open func setEmail(email:String?) {
        self.email = email
    }
    
    open func getEmail() -> String? {
        return self.email
    }
    
    open func setId(id:String?) {
        self.id = id
    }
    
    open func getId() -> String? {
        return self.id
    }
}
