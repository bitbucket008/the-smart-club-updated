//
//  FlashDealDetails.swift
//  smartclub
//
//  Created by Mac-Admin on 1/4/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class FlashDealDetails: NSObject {
    //MARK: Property
    open var dealId:String
    open var name:String
    open var vendorDescription:String?
    open var orgName:String?
    open var createdAt:String?
    open var expiryTime:String?
    open var gainSP:Int
    open var identityCode:String
    open var vendorId:String
    
    init(dealId:String, name:String, vendorDescription:String?, orgName:String?,
         createdAt:String?, expiryTime:String?, gainSP:Int, identityCode:String, vendorId:String){
        self.dealId = dealId
        self.name = name
        self.vendorDescription = vendorDescription
        self.orgName = orgName
        self.createdAt = createdAt
        self.expiryTime = expiryTime
        self.gainSP = gainSP
        self.identityCode = identityCode
        self.vendorId = vendorId
    }
}
