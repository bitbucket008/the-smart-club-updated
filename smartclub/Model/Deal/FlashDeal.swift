//
//  FlashDeal.swift
//  smartclub
//
//  Created by Mac-Admin on 1/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

open class FlashDeal: NSObject {
    open var id:String
    open var orgName:String
    open var dealImage:String?
    open var gainSp:String?
    
    init(id:String, orgName:String, dealImage:String?, gainSp:String?) {
        self.id = id
        self.orgName = orgName
        self.dealImage = dealImage
        self.gainSp = gainSp
    }
        
}
