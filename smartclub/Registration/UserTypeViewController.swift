//
//  UserTypeViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 1/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class UserTypeViewController: UIViewController {
    
    //MARK: Properties
    private var userType:String = ""
    
    //MARK: Outlets
    @IBOutlet weak var btnStudent: UIButton!
    @IBOutlet weak var studentButtonContainer: UIView!
    @IBOutlet weak var btnEmployee: UIButton!
    @IBOutlet weak var employeeButtonContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: Actions
    //when click on employee button
    @IBAction func obClickEmployeeButton(_ sender: UIButton, forEvent event: UIEvent) {
        //change button size
        //        btnEmployee.bounds = CGRect(x: btnEmployee.bounds.origin.x, y: btnEmployee.frame.origin.y, width: 247, height: 214)
        //        btnStudent.bounds = CGRect(x: btnStudent.bounds.origin.x, y: btnStudent.frame.origin.y, width: 190, height: 177)
        
        //change user type
        userType = "customer"
        
        //goto login view controller
        if userType.count < 1 {
            SnackBarManager.showSnackBar(message: "Select an user type")
        }else {
            //            let loginStoryboard = UIStoryboard.init(name: "Login", bundle: nil)
            //            let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            //            loginViewController.userType = Optional(userType)
            //            self.present(loginViewController, animated: true, completion: nil)
            
            //goto registration normal  view controller
            let registrationStoryBoard = UIStoryboard.init(name: "Registration", bundle: nil)
            let registrationNormalViewController = registrationStoryBoard.instantiateViewController(withIdentifier: "RegistrationNormalViewController") as! RegistrationNormalViewController
            registrationNormalViewController.userType = Optional(userType)
            self.present(registrationNormalViewController, animated: true, completion: nil)
        }
        
    }
    
    
    //when click on student button
    @IBAction func onClickStudentButton(_ sender: UIButton, forEvent event: UIEvent) {
        //change button size
        //        btnStudent.bounds = CGRect(x: btnStudent.bounds.origin.x, y: btnStudent.frame.origin.y, width: 250, height: 215)
        //        btnEmployee.bounds = CGRect(x: btnEmployee.bounds.origin.x, y: btnEmployee.frame.origin.y, width: 190, height: 177)
        
        //change user type
        userType = "student"
        
        //goto login view controller
        if userType.count < 1 {
            SnackBarManager.showSnackBar(message: "Select an user type")
        }else {
            //            let loginStoryboard = UIStoryboard.init(name: "Login", bundle: nil)
            //            let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            //            loginViewController.userType = Optional(userType)
            //
            //            self.present(loginViewController, animated: true, completion: nil)
            
            //goto registration normal  view controller
            let registrationStoryBoard = UIStoryboard.init(name: "Registration", bundle: nil)
            let registrationStudentViewController = registrationStoryBoard.instantiateViewController(withIdentifier: "RegistrationStudentViewController") as! RegistrationStudentViewController
            registrationStudentViewController.userType = Optional(userType)
            self.present(registrationStudentViewController, animated: true, completion: nil)
        }
    }
    
    //    //when click on next button
    //    @IBAction func onClickNextButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
    //        if userType.count < 1 {
    //            SnackBarManager.showSnackBar(message: "Select an user type")
    //        }else {
    //            let loginStoryboard = UIStoryboard.init(name: "Login", bundle: nil)
    //            let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    //            loginViewController.userType = Optional(userType)
    //
    //            self.present(loginViewController, animated: true, completion: nil)
    //        }
    //    }
    
    
    //MARK: backbutton
    
    @IBAction func onClickLoginButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    
    
}
