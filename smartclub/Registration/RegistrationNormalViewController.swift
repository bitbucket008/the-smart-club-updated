//
//  RegistrationViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 1/1/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import DropDown

class RegistrationNormalViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: Instance variable
    fileprivate let CLASS_NAME:String = "RegistrationNormalViewController"
    fileprivate var isKeyboardAppeared = false
    open var userType:String?
    private var gender:String = "male"
    private var name:String?
    private var email:String?
    private var companyName:String?
    private var password:String?
    private var confirmPassword:String?
    fileprivate var companyList:[Company] = [Company]()
    fileprivate let localStorage = LocalStorage()
    fileprivate let dropDown = DropDown()
    fileprivate var isCompanyListLoaded = false
    fileprivate var selectedCompanyId:String?
    fileprivate var isAgreeButtonSelected:Bool = false
    fileprivate var isSearchCompanyButtonSelected:Bool = false

    //MARK: Outlets
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnGenderMale: UIBorderButton!
    @IBOutlet weak var btnGenderFemale: UIBorderButton!
    @IBOutlet weak var tfCompanyName: UITextField!
    @IBOutlet weak var btnSearchCompany: UIButton!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnCreateAccount: UIBorderButton!
    @IBOutlet weak var uvCompanyListContainer: UIView!
    @IBOutlet weak var uvCompanyContainer: UIView!
    @IBOutlet weak var passwordContainer: UIView!
    @IBOutlet weak var btnAgreeRules: UIButton!
    @IBOutlet weak var lblRulesOfUse: UILabel!
    @IBOutlet weak var lblCantFindCompany: UILabel!
    
    
    
    //For smooth keyboard
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var uivContentView: UIView!
    @IBOutlet weak var activeTextField: UITextField? //deliberately left and is optional
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // setup ui
        setupUI()
        
        //initialization
        initialization()
        
        //get Company list from server
//        self.getCompanyList()
        
        //dismiss keyboard when press to outside
         dissmisKeyboard()
        
        // register notification for mving up/down view when keyboard show/hide
//        registerNotificationForKeyboard()
        
       
        
        
    }
    
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //keyboard -start
    override func viewWillAppear(_ animated: Bool) {
        // call method for keyboard notification
        self.setNotificationKeyboard()
    }
    
    // Notification when keyboard show
    func setNotificationKeyboard ()  {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    // get current text field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField;
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    
    // when show keyboard increase height of scroll view
    @objc func keyboardWasShown(notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height+180, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeTextField
        {
            if (!aRect.contains(activeField.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    // when keyboard hide reduce height of scroll view
    @objc func keyboardWillBeHidden(notification: NSNotification){
        print(CLASS_NAME+" -- keyboardWillBeHidden() ")
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,0.0, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        // Try to find next responder
        if let nextField = textField.superview?.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        
        return false
    }
    
    // END
    
    
    //MARK: Init methods
    private func setupUI() -> Void {
        //tfCompanyName
//        tfCompanyName.layer.borderWidth = 2
//        tfCompanyName.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
//        tfCompanyName.layer.cornerRadius = 25
        
        // tfPassword
        tfPassword.isSecureTextEntry = true
        tfConfirmPassword.isSecureTextEntry = true
        
        // The view to which the drop down will appear on
        dropDown.anchorView = tfCompanyName // UIView or UIBarButtonItem
//        dropDown.bounds.origin.y = dropDown.bounds.origin.y - 30
        dropDown.direction = .bottom
        dropDown.bounds = CGRect(x: tfCompanyName.bounds.minX, y: tfCompanyName.bounds.minY-33, width: tfCompanyName.frame.width, height: view.frame.height/2)
        DropDown.appearance().backgroundColor = .white
        DropDown.appearance().tintColor = UIColor.gray
        
        //Agree button
        btnAgreeRules.layer.cornerRadius = btnAgreeRules.frame.height/2
        
        //scrollview
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 200, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        scrollView.layoutIfNeeded()
        
        //layout parent view
        self.view.layoutIfNeeded()
        
    }
    
    //initialization
    private func initialization() -> Void{
        self.tfName!.delegate = self
        self.tfEmail!.delegate = self
        self.tfPassword!.delegate = self
        self.tfConfirmPassword!.delegate = self
        self.tfCompanyName!.delegate = self
        
        //tap gesture for lblRulesOfUse
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showRulesOfUseViewCobtroller))
        lblRulesOfUse.addGestureRecognizer(tapGesture)
        
        //tap gesture for lblCantFindCompany
        let tapGestureForlblCantFindCompany = UITapGestureRecognizer(target: self, action: #selector(showAddCompanydialog))
        lblCantFindCompany.addGestureRecognizer(tapGestureForlblCantFindCompany)
    }


    //show rules of use view cobtroller
    @objc func showRulesOfUseViewCobtroller(sender:UITapGestureRecognizer){
        print(CLASS_NAME+" -- showRulesOfUseViewCobtroller() ")
        
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        let vc = strBoard.instantiateViewController(withIdentifier: "termsconditionvc") as! TermsConditionViewController
        
        vc.loadedFrom = Optional("Registration")
        
//        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //show add company dialog
    @objc func showAddCompanydialog(sender:UITapGestureRecognizer){
        print(CLASS_NAME+" -- showAddCompanydialog() ")
        
        let alertController = UIAlertController(title: "Can't find your company?", message: "Please write your company name below and after verification we will add it to the database", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
            let companyNameField = alertController.textFields![0] as UITextField
            
            if companyNameField.text! == "" {
                SnackBarManager.showSnackBar(message: "Enter company name")
            }else{
                // network call
                self.registerCompany(companyName: companyNameField.text!)
            }
            
            // do something with textField
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter company name "
            textField.keyboardType = .default
            textField.enablesReturnKeyAutomatically = false
        })
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: Notifications
    //register notification for moving view
//    func registerNotificationForKeyboard() -> Void {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.moveUpViewWhenShowingKeyboard), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.moveDownViewWhenHidingKeyboard), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//    }
//
//    //MARK: TextFieldDelegate
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.endEditing(true)
//        self.view.endEditing(true)
//        return true
//    }
    
    //MARK: Keyboard
    //dismiss keyboard
//    func dissmisKeyboard() {
//        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
//        tapper.cancelsTouchesInView = false
//        self.view.addGestureRecognizer(tapper)
//    }
//
//    //release keyboard from input field
//    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
//        self.view.endEditing(true)
//    }
//
//    //move view controller when keyboard appears
//    @objc func moveUpViewWhenShowingKeyboard(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            isKeyboardAppeared = true
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
//    }
//
//    //move back to default place when keyboard hides
//    @objc func moveDownViewWhenHidingKeyboard(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            isKeyboardAppeared = false
//            if self.view.frame.origin.y != 0{
//                self.view.frame.origin.y += keyboardSize.height
//            }
//        }
//    }
//
    
    //MARK: Actions
    //when click on male button 
    @IBAction func onClickGenderMaleButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        //changes on btnGenderMale
        btnGenderMale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
        btnGenderMale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
        btnGenderMale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
        
        //changes on btnGenderFemale
        btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
        btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
        btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
        
        //set gender value 
        gender = "male"
    }
    
    //when click on female button
    @IBAction func onCLickGenderFemaleButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        //changes on btnGenderFemale
        btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
        btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
        btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
        
        //changes on btnGenderMale
        btnGenderMale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
        btnGenderMale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
        btnGenderMale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
        
        //set gender value
        gender = "female"
    }
    
    
    //on agree rules button
    @IBAction func onClickAgreeRulesButton(_ sender: UIButton, forEvent event: UIEvent) {
        
        isAgreeButtonSelected = !isAgreeButtonSelected
        
        if isAgreeButtonSelected {
            btnAgreeRules.setTitle(".", for: .normal)
        }else{
            btnAgreeRules.setTitle("", for: .normal)
        }
    }
    
    
    //when on key press on tfcomapnylist
    @IBAction func onKeyPressCompanyField(_ sender: UITextField, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onKeyPressCompanyField() -- val: \(sender.text!)")
        //get company list from server
        let text = tfCompanyName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
//        tfCompanyName.text = ""
        
        //Hide the search button
//        if text.count > 0{
//            btnSearchCompany.isHidden = false
//        }else{
//            btnSearchCompany.isHidden = true
//        }
        
        //get company list
        if text.count > 2{
            getCompanyList(text:text)
        }
        
//        else if text.count < 1{
//            self.companyList.removeAll()
//            self.view.window?.endEditing(true)
//            print("i am removed")
//        }
        
        if text.count < 1 {
            print("i am removed")
            self.companyList.removeAll()
        }
        //view.endEditing(true)
        
        //populate company list to dropdown
        if isCompanyListLoaded {
            populateCompanyListToDropdown()
        } else {
            SnackBarManager.showSnackBar(message: "Company not found")
        }
    }
    
    //when click company search
    @IBAction func onClickCompanySearchButton(_ sender: UIButton, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onClickCompanySearchButton() ")
        //get company list from server
        let text = tfCompanyName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
//        tfCompanyName.text = ""
        
        //get company list
        if text.count > 0 {
            getCompanyList(text:text)
        }
        
        
        //populate company list to dropdown
//        if isCompanyListLoaded {
//            populateCompanyListToDropdown()
//            getCompanyList(text:text)
//        }else{
//            SnackBarManager.showSnackBar(message: "Company not found")
//        }
    }
    
    
    //when click on create account button
    @IBAction func onClickCreateAccountButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        
        //hide keyboard
        self.view.endEditing(true)
        
        //get values
        name = tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        email = tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        password = tfPassword.text
        confirmPassword = tfConfirmPassword.text
        companyName = tfCompanyName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //validate info and register user     
        validateAndRegisterUser()
    } 
    
    
    //Populate company list to dropdown
    private func populateCompanyListToDropdown() -> Void {
        var companyListForDropdown:[Company] = [Company]()
        var companyNameListForDropdown:[String] = [String]()
        
        for i in 0 ..< companyList.count {
//            if companyList[i].name!.lowercased().contains(text.lowercased()){
                companyListForDropdown.append(companyList[i])
                companyNameListForDropdown.append(companyList[i].name!)
//            }
        }
        
        // The list of items to display. Can be changed dynamically
        if companyNameListForDropdown.count > 0 {
            self.dropDown.dataSource = companyNameListForDropdown
            // Action triggered on selection
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.tfCompanyName.text = item
                self.selectedCompanyId = companyListForDropdown[index].id
                self.dropDown.hide()
            }
            self.dropDown.show()
        }
    }
    
    
    //validate and register user
    private func validateAndRegisterUser() -> Void {
        if !Validator.isValidUsername(username: name!) {
            SnackBarManager.showSnackBar(message: "Invalid user name")
        }else if !Validator.isValidEmail(email: email!){
            SnackBarManager.showSnackBar(message: "Invalid email")
        }else if !Validator.isValidPassword(password: password!){
                SnackBarManager.showSnackBar(message: "Invalid password")
        }else if(!Validator.isValidConfirmPassword(password: password!, confirmPassword: confirmPassword!)){
                    SnackBarManager.showSnackBar(message: "Password needs to be matched")
        }
//        else if(!Validator.isValidCompanyName(companyId: selectedCompanyId!)){
//            SnackBarManager.showSnackBar(message: "Invalid company name")
//        }
        //check agree button is selected or not
        else if !isAgreeButtonSelected {
            SnackBarManager.showSnackBar(message: "Please accept the Rules of Use")
        }
        
//        else if !isSearchCompanyButtonSelected {
//            SnackBarManager.showSnackBar(message: "Invalid company name.Please tap on the SEARCH icon.")
//        }
        
        else if(!Validator.isValidCompanyName(companyId: selectedCompanyId)){
            SnackBarManager.showSnackBar(message: "Please tap on the SEARCH icon to find your Company.")
        }
            
        else if !ConnectivityDetector.isInternetAvailable(){
            SnackBarManager.showSnackBarForInternetSetting()
            print(CLASS_NAME+" -- validateAndRegisterUser()  -- No internet connection.....")
        }else{
            //register user
            registerUser()
        }
    }
    
    //Register user
    private func registerUser() -> Void {
        //show progressbar
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_REGISTRATION_OTHER)!,
            method: .post,
            parameters: ["full_name": name!, "email": email!, "age":"20" ,"passkey": password!, "confirm_passkey": confirmPassword!, "company_id": selectedCompanyId!, "device_token": localStorage.getUserDeviceToken()!], encoding: JSONEncoding.default)
            
            .responseJSON { (response) -> Void in
                
                //hide progrees dialog
                SVProgressHUD.dismiss()
                
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    SnackBarManager.showSnackBar(message: "Registration failed! provide valid information")
                    return
                }
                
                
                //go to  login view controller
                let swiftyJson = JSON(response.data!)
                if  swiftyJson["success"].intValue == 1{
//                if response.result.isSuccess{
                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    
                    let loginStoryboard = UIStoryboard.init(name: "Login", bundle: nil)
                    let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    
                    //wait few seconds
                    BackgroundThread.background(delay: 3.0, background: nil, completion: {
                        self.present(loginViewController, animated: true, completion: nil)
                    })
                }else{
                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                }
                
                print(self.CLASS_NAME+" -- registerUser()  -- response -- "+(String(describing: response.result)))
            
        }
    }
    
    //get comapny list from server
    private func getCompanyList(text:String) -> Void {
        //show progressbar
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_REGISTRATION_COMPANY_LIST)!,
            method: .get,
            parameters: ["platform": "mobile","search":text, "status":"active","approve": "true"])
            
            .responseJSON { (response) -> Void in
                
                //go to  login view controller
                let swiftyJson = JSON(response.data!)
                if  swiftyJson["success"].intValue == 1{
                    self.companyList.removeAll()
//                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    
//                     print(self.CLASS_NAME+" -- getCompanyList()  -- response -- "+(String(describing: response)))
                    
                    //parse json data
                    if(swiftyJson["data"].arrayValue.count > 0){
                        for i in 0 ..< swiftyJson["data"].arrayValue.count {
                            let id = swiftyJson["data"][i]["id"].string
                            let name = swiftyJson["data"][i]["name"].string
                            let details = swiftyJson["data"][i]["details"].string
                            let status = swiftyJson["data"][i]["status"].int
                            self.companyList.append(Company(id:id, name:name, details:details, status:status))
                            print(self.CLASS_NAME+" -- getCompanyList()  -- company  name -- "+(self.companyList[i].name!))
                        }
                        
                      //populate dropdown
                        self.populateCompanyListToDropdown()
                        
                        //set company list loaded to true
                        self.isCompanyListLoaded = true
                    }else{
                        SnackBarManager.showSnackBar(message: "Company not found")
                    }
                }else{
//                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                }
                
                //hide progrees dialog
                SVProgressHUD.dismiss()
                
                print(self.CLASS_NAME+" -- getCompanyList()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    
    //register company to server
    private func registerCompany(companyName:String) -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_REGISTRATION_ADD_COMPANY)!,
            method: .post,
            parameters: [ "name": companyName, "type":1, "approve": "false"])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- registerInstitute()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- registerInstitute()  -- response -- "+(String(describing: response)))
        }
    }
}


