//
//  RegistrationStudentViewController.swift
//  smartclub
//
//  Created by Mac-Admin on 1/2/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import DropDown

class RegistrationStudentViewController: UIViewController, UITextFieldDelegate {
    //MARK: Instance variable
    fileprivate let CLASS_NAME:String = "RegistrationNormalViewController"
    open var userType:String?
    private var gender:String = "male"
    private var name:String?
    private var email:String?
    private var password:String?
    private var confirmPassword:String?
    private var institutionName:String!
    fileprivate var instituteList:[Company] = [Company]()
    fileprivate let dropDown = DropDown()
    fileprivate var isInstituteListLoaded = false
    fileprivate var selectedInstituteId:String?
    fileprivate var isAgreeButtonSelected:Bool = false
    
    //MARK: Outlets
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnGenderMale: UIBorderButton!
    @IBOutlet weak var btnGenderFemale: UIBorderButton!
    @IBOutlet var tfInstitutionName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnCreateAccount: UIBorderButton!
    @IBOutlet weak var btnAgreeRules: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var activeTextField: UITextField?
    @IBOutlet weak var lblRulesOfUse: UILabel!
    @IBOutlet weak var lblCantFindInstitute: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup ui
        setupUI()
        
        //initialization
        initialization()
        
        //get institute list from server
        getInstituteList()
        
        //dismiss keyboard when press to outside
        dissmisKeyboard()

        
    }
    
    
    //dismiss keyboard
    func dissmisKeyboard() {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
        tapper.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapper)
    }
    
    //release keyboard from input field
    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
        self.view.endEditing(true)
    }
    
    //MARK: UI methods
    private func setupUI() -> Void {
        // tfPassword
        tfPassword.isSecureTextEntry = true
        tfConfirmPassword.isSecureTextEntry = true
        
        //Agree button
        btnAgreeRules.layer.cornerRadius = btnAgreeRules.frame.height/2
        
        //scrollview
//        scrollView.contentSize = CGSize(width: self.view.frame.width, height: 900)
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 200, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        scrollView.layoutIfNeeded()
        
        //layout parent view
        self.view.layoutIfNeeded()
    }
    
    //initialization
    private func initialization() -> Void{
        self.tfName!.delegate = self
        self.tfEmail!.delegate = self
        self.tfInstitutionName!.delegate = self
        self.tfPassword!.delegate = self
        self.tfConfirmPassword!.delegate = self
        
        // The view to which the drop down will appear on
        dropDown.anchorView = tfInstitutionName // UIView or UIBarButtonItem
//        dropDown.bounds.origin.y = dropDown.bounds.origin.y - 30
        dropDown.direction = .bottom
        dropDown.bounds = CGRect(x: tfInstitutionName.bounds.minX, y: tfInstitutionName.bounds.minY-33, width: tfInstitutionName.frame.width, height: view.frame.height/2)
        DropDown.appearance().backgroundColor = .white
        DropDown.appearance().tintColor = UIColor.gray
        
        //tap gesture for lblRulesOfUse
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showRulesOfUseViewCobtroller))
        lblRulesOfUse.addGestureRecognizer(tapGesture)
        
        //tap gesture for lblCantFindInstitute
        let tapGestureForlblCantFindInstitute = UITapGestureRecognizer(target: self, action: #selector(showAddInstitutedialog))
        lblCantFindInstitute.addGestureRecognizer(tapGestureForlblCantFindInstitute)
    }
    
    //show rules of use view cobtroller
    @objc func showRulesOfUseViewCobtroller(sender:UITapGestureRecognizer){
        print(CLASS_NAME+" -- showRulesOfUseViewCobtroller() ")
        
        let strBoard = UIStoryboard.init(name: "account", bundle: nil)
        let vc = strBoard.instantiateViewController(withIdentifier: "termsconditionvc") as! TermsConditionViewController
        
        vc.loadedFrom = Optional("Registration")
        
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    //show add institute dialog
    @objc func showAddInstitutedialog(sender:UITapGestureRecognizer){
        print(CLASS_NAME+" -- showAddInstitutedialog() ")
        
        let alertController = UIAlertController(title: "Can't find your institute?", message: "Please write your institute name below and after verification we will add it to the database", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
            let companyNameField = alertController.textFields![0] as UITextField
            
            if companyNameField.text! == "" {
                SnackBarManager.showSnackBar(message: "Enter institue name")
            }else{
                // network call
                self.registerInstitute(instituteName: companyNameField.text!)
            }
            
            // do something with textField
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter institute name "
            textField.keyboardType = .default
            textField.enablesReturnKeyAutomatically = false
        })
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //keyboard -start
    
    override func viewWillAppear(_ animated: Bool) {
        // call method for keyboard notification
        self.setNotificationKeyboard()
    }
    
    // Notification when keyboard show
    func setNotificationKeyboard ()  {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    // get current text field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField;
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    // when show keyboard increase height of scroll view
    @objc func keyboardWasShown(notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height+180, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeTextField
        {
            if (!aRect.contains(activeField.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    // when keyboard hide reduce height of scroll view
    @objc func keyboardWillBeHidden(notification: NSNotification){
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,0.0, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        // Try to find next responder
        if let nextField = textField.superview?.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        
        return false
    }
    
    // END
    
    
    
    //    func dissmisKeyboard() {
    //        let tapper = UITapGestureRecognizer(target: self, action:#selector(releaseKeyBoard))
    //        tapper.cancelsTouchesInView = true
    //        self.view.addGestureRecognizer(tapper)
    //    }
    //
    //    //release keyboard from input field
    //    @objc func releaseKeyBoard(recognizer: UIGestureRecognizer){
    //        self.view.endEditing(true)
    //    }
    //
    //    // Start Editing The Text Field
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //        moveTextField(textField, moveDistance: -250, up: true)
    //    }
    //
    //    // Finish Editing The Text Field
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //        moveTextField(textField, moveDistance: -250, up: false)
    //    }
    //
    //    // Hide the keyboard when the return key pressed
    //     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //        textField.endEditing(true)
    //        self.view.endEditing(true)
    //        return true
    //    }
    //
    //    // Move the text field in a pretty animation!
    //    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
    //        let moveDuration = 0.3
    //        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
    //
    //        UIView.beginAnimations("animateTextField", context: nil)
    //        UIView.setAnimationBeginsFromCurrentState(true)
    //        UIView.setAnimationDuration(moveDuration)
    //        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
    //        UIView.commitAnimations()
    //    }
    //
    
    
    //Keyboard
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    
    
    //MARK: Actions
    //when click on male button
    @IBAction func onClickGenderMaleButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        //changes on btnGenderMale
        btnGenderMale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
        btnGenderMale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
        btnGenderMale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
        
        //changes on btnGenderFemale
        btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
        btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
        btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
        
        //set gender value
        gender = "male"
    }
    
    //when click on female button
    @IBAction func onCLickGenderFemaleButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        //changes on btnGenderFemale
        btnGenderFemale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0)
        btnGenderFemale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GREEN, alpha: 1.0), for: .normal)
        btnGenderFemale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_BLUE, alpha: 1.0).cgColor
        
        //changes on btnGenderMale
        btnGenderMale.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0)
        btnGenderMale.setTitleColor(ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_DEEP_GRAY, alpha: 1.0), for: .normal)
        btnGenderMale.layer.borderColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.COLOR_LIGHT_GRAY, alpha: 1.0).cgColor
        
        //set gender value
        gender = "female"
    }
    
    //when on key press on tfcomapnylist
    @IBAction func onKeyPressInstituteField(_ sender: UITextField, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onKeyPressCompanyField() -- val: \(sender.text!)")
        //get institute list from server
        let text = tfInstitutionName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        //        tfCompanyName.text = ""
        
        //populate company list to dropdown 
        if isInstituteListLoaded {
            populateInstituteListToDropdown(text: text)
        }else{
            SnackBarManager.showSnackBar(message: "Institute not found")
        }
    }
    
    //when click on institute field
    @IBAction func onClickInstituteField(_ sender: UITextField, forEvent event: UIEvent) {
        print(CLASS_NAME+" -- onClickInstituteField() -- val: \(sender.text!)")
        //get institute list from server
        let text = tfInstitutionName.text!
        //        tfCompanyName.text = ""
        
        //populate isntitute list to dropdown
        if isInstituteListLoaded {
            populateInstituteListToDropdown(text: text)
        }else{
            SnackBarManager.showSnackBar(message: "Institute not found")
        }
    }
    
    
    //when click institute search
    @IBAction func onClickInstituteSearchButton(_ sender: UIButton, forEvent event: UIEvent) {
        //get institute list from server
        let text = tfInstitutionName.text!
        tfInstitutionName.text = ""
        
        //populate company list to dropdown
        if isInstituteListLoaded {
            populateInstituteListToDropdown(text: text)
        }else{
            SnackBarManager.showSnackBar(message: "Institute not found")
        }
    }
    
    //when click on create account button
    @IBAction func onClickCreateAccountButton(_ sender: UIBorderButton, forEvent event: UIEvent) {
        
        //hide keyboard
        self.view.endEditing(true )
        
        //get values
        name = tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        email = tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        password = tfPassword.text
        confirmPassword = tfConfirmPassword.text
        institutionName = tfInstitutionName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //validate info and register user
        validateAndRegisterUser()
    }
    
    //on agree rules button
    @IBAction func onClickAgreeRulesButton(_ sender: UIButton, forEvent event: UIEvent) {
        
        isAgreeButtonSelected = !isAgreeButtonSelected
        
        if isAgreeButtonSelected {
            btnAgreeRules.setTitle(".", for: .normal)
        }else{
            btnAgreeRules.setTitle("", for: .normal)
        }
    }
    
    //Populate company list to dropdown
    private func populateInstituteListToDropdown(text:String) -> Void {
        var instituteListForDropdown:[Company] = [Company]()
        var instituteNameListForDropdown:[String] = [String]()
        
        if !text.isEmpty || !text.elementsEqual(""){
            for i in 0 ..< instituteList.count {
                if instituteList[i].name!.lowercased().contains(text.lowercased()){
                    instituteListForDropdown.append(instituteList[i])
                    instituteNameListForDropdown.append(instituteList[i].name!)
                }
            }
        }else{
            for i in 0 ..< instituteList.count {
                instituteListForDropdown.append(instituteList[i])
                instituteNameListForDropdown.append(instituteList[i].name!)
            }
        }
        
        // The list of items to display. Can be changed dynamically
        if instituteNameListForDropdown.count > 0 {
            self.dropDown.dataSource = instituteNameListForDropdown
            // Action triggered on selection
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.tfInstitutionName.text = item
                self.selectedInstituteId = instituteListForDropdown[index].id
                self.dropDown.hide()
            }
            self.dropDown.show()
        }
    }
    
    //MARK: Method -> Register
    //validate and register user
    private func validateAndRegisterUser() -> Void {
        if !Validator.isValidUsername(username: name!) {
            SnackBarManager.showSnackBar(message: "Invalid user name")
        }else if !Validator.isValidEmail(email: email!){
            SnackBarManager.showSnackBar(message: "Invalid email")
        }else if !Validator.isValidPassword(password: password!){
            SnackBarManager.showSnackBar(message: "Invalid password")
        }else if(!Validator.isValidConfirmPassword(password: password!, confirmPassword: confirmPassword!)){
            SnackBarManager.showSnackBar(message: "Password need to be matched")
        }
        
        else if(!Validator.isValidCompanyName(companyId: selectedInstituteId!)){
            SnackBarManager.showSnackBar(message: "Invalid company name")
        }
        //check agree button is selected or not
        else if !isAgreeButtonSelected {
            SnackBarManager.showSnackBar(message: "Accept the rules of use")
        }
        
        else if !ConnectivityDetector.isInternetAvailable(){
            SnackBarManager.showSnackBarForInternetSetting()
            print(CLASS_NAME+" -- validateAndRegisterUser()  -- No internet connection.....")
        }else{
            //register user
            registerUser()
        }
    }
    
    //Register user
    private func registerUser() -> Void { 
        Alamofire.request(
            URL(string: ApiEndpoints.URI_REGISTRATION_STUDENT)!,
            method: .post,
            parameters: ["full_name": name!, "email": email!, "age":"20" ,"passkey": password!, "confirm_passkey": confirmPassword!, "company_id":selectedInstituteId!], encoding: JSONEncoding.default)
            
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    SnackBarManager.showSnackBar(message: "Request Failed")
                    return
                }
                
                
                //go to  login view controller
                let swiftyJson = JSON(response.data!)
                if  swiftyJson["success"].intValue == 1{
                    //                if response.result.isSuccess{
                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    
                    let loginStoryboard = UIStoryboard.init(name: "Login", bundle: nil)
                    let loginViewController = loginStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    
                    //wait few seconds  
                    BackgroundThread.background(delay: 3.0, background: nil, completion: {
                        self.present(loginViewController, animated: true, completion: nil)
                    })
                }else{
                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                }
                
                print(self.CLASS_NAME+" -- registerUser()  -- response -- "+(String(describing: response.result)))
                
        }
    }
    
    //get institute list from server
    private func getInstituteList() -> Void {
        //show progressbar
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_REGISTRATION_COMPANY_LIST)!,
            method: .get,
            parameters: ["platform": "mobile","type":"2", "status":"active","approve": "true"])
            
            .responseJSON { (response) -> Void in
                //hide progrees dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                let swiftyJson = JSON(response.data!)
                if  swiftyJson["success"].intValue == 1{
//                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    
                    //parse json data
                    if(swiftyJson["data"].arrayValue.count > 0){
                        for i in 0 ..< swiftyJson["data"].arrayValue.count {
                            let id = swiftyJson["data"][i]["id"].string
                            let name = swiftyJson["data"][i]["name"].string
                            let details = swiftyJson["data"][i]["details"].string
                            let status = swiftyJson["data"][i]["status"].int
                            self.instituteList.append(Company(id:id, name:name, details:details, status:status))
                            print(self.CLASS_NAME+" -- getInstituteList()  -- company  name -- "+(self.instituteList[i].name!))
                        }
                        
                        //set institute list loaded to true
                        self.isInstituteListLoaded = true
                    }
                }else{
//                    SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                }
                
                print(self.CLASS_NAME+" -- getInstituteList()  -- response -- "+(String(describing: response)))
                
        }
    }
    
    //register institute to server
    private func registerInstitute(instituteName:String) -> Void {
        SVProgressHUD.show()
        
        Alamofire.request(
            URL(string: ApiEndpoints.URI_REGISTRATION_ADD_COMPANY)!,
            method: .post,
            parameters: [ "name": instituteName, "type":2, "approve": "false"])
            .responseJSON { (response) -> Void in
                
                //dismiss progress dialog
                SVProgressHUD.dismiss()
                
                //go to  login view controller
                if response.result.isSuccess{
                    let swiftyJson = JSON(response.data!)
                    if  swiftyJson["success"].intValue == 1{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                        
                    }else{
                        SnackBarManager.showSnackBar(message: swiftyJson["message"].stringValue)
                    }
                    
                }else{
                    print(self.CLASS_NAME+" -- registerInstitute()  -- response -- request failed")
                    SnackBarManager.showSnackBar(message: "Request failed")
                }
                
                print(self.CLASS_NAME+" -- registerInstitute()  -- response -- "+(String(describing: response)))
        }
    }

    
}
