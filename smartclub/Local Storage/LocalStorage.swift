//
//  LocalStorage.swift
//  smartclub
//
//  Created by RASHED on 1/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class LocalStorage{
    private var preference:UserDefaults!
    private var user:User?
    
    init() {
        preference = UserDefaults.standard
        user = User()
    }
    
    //store user data
    func storeUser(user:User) {
        preference.set(user.username, forKey: ConstantCollection.PREFERENCE_USER_NAME)
        preference.set(user.password, forKey: ConstantCollection.PREFERENCE_USER_PASSWORD)
        //preference.set(user, forKey: ConstantCollection.PREFERENCE_USER)
        preference.synchronize()
    }
    
    //retrive user data
    func getUser() -> User {
        user?.setUsername(username: preference.string(forKey: ConstantCollection.PREFERENCE_USER_NAME)!)
        user?.setPassword(password: preference.string(forKey: ConstantCollection.PREFERENCE_USER_PASSWORD)!)
        return user!
    }
    
    //set user logged in
    func setUserLoggedIn(loggedIn:Bool) {
        preference.set(loggedIn, forKey: ConstantCollection.PREFERENCE_USER_LOGGEDIN)
        preference.synchronize()
    }
    
    //check user is loggedin or not
    func isLoggedInUser() -> Bool {
        return preference.bool(forKey: ConstantCollection.PREFERENCE_USER_LOGGEDIN)
    }
    
    //logout user
    func logOut()  {
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_NAME)
        preference.removeObject(forKey: ConstantCollection.PREFERENCE_USER_PASSWORD)
        preference.set(false, forKey: ConstantCollection.PREFERENCE_USER_LOGGEDIN)
        preference.synchronize()
    }
}
