//
//  PopupBuySPViewController.swift
//  smartclub
//
//  Created by Admin on 03/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class WalletViewController: UIViewController{
    
    //MARK: Outlets
    @IBOutlet var ScrollView: UIScrollView!
    @IBOutlet var labelOne: UILabel!
    @IBOutlet var labelTwo: UILabel!
    @IBOutlet var labelThree: UILabel!
    @IBOutlet weak var savedTotalLbl: UILabel!
    @IBOutlet weak var lblTotalSP: UILabel!
    @IBOutlet var walletCenterImage: UIView!
    @IBOutlet weak var commingSoon: UIView!
    
    //MARK: Properties
    fileprivate let CLASS_NAME = "WalletViewController"
    fileprivate let localStorage:LocalStorage = LocalStorage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getWalletLabelData()
        centerImageUI()
        centerImageUITwo()
        
        //ScrollView.contentSize.height = 700
        //ScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 500)
        
        //set sp point
//        lblTotalSP.text = String(describing: RoundNumberFormatter.formatPoints(num: Double(localStorage.getUser().earnedSp!)))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Center Image- Wallet Page- UI
    func centerImageUI() {
        walletCenterImage.layer.cornerRadius = 3
        walletCenterImage.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0).cgColor
        walletCenterImage.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        walletCenterImage.layer.shadowRadius = 1
        walletCenterImage.layer.shadowOpacity = 0.25
    }
    // Wallet image second one
    func centerImageUITwo(){
        commingSoon.layer.cornerRadius = 3
        commingSoon.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0).cgColor
        commingSoon.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        commingSoon.layer.shadowRadius = 1
        commingSoon.layer.shadowOpacity = 0.25
    }
    
    func getWalletLabelData(){
        SVProgressHUD.show()

        Alamofire.request("http://beta.thesmartclubapp.com/api/customers/"+localStorage.getUser().getId()!+"/wallets?platform=mobile")
            .responseJSON { response in

                SVProgressHUD.dismiss()

                if let value = response.result.value {

                    let json = JSON(value)

//                    let left = json["data"]["redeemCount"]["left"].intValue
//                    let main = json["data"]["redeemCount"]["main"].intValue
//                    let right = json["data"]["redeemCount"]["right"].intValue
                    let savedTotal = json["data"]["savedTotal"].stringValue
                    //Printing strings from a JSON Dictionary

//                    self.labelOne.text = String(left)+" DEALS"
//                    self.labelTwo.text = String(main)+" DEALS"
//                    self.labelThree.text = String(right)+" DEALS"
                    
                    self.savedTotalLbl.text = "QAR "+savedTotal

//                    print(self.CLASS_NAME+" -- getWalletLabelData() -- \(right) left : "+String(left)+" -- main : "+String(main)+" -- right : "+String(right))
               }

                print(self.CLASS_NAME+" -- getWalletLabelData() -- \(response)")
        }
    }
}


